const chai = require('chai')
const assert = chai.assert
const voice = require('../src/helpers/voice');


describe('Transforma a saida da API de localidades', function () {
    const result = voice.specialistsToVoice([
        {
            telefoneA: '(11) 91231-5623',
            logradouro: 'AL SANTOS 1000'
        },
        {
            telefoneA: '(11) 92456-6642',
            logradouro: 'R GENERAL CARNEIRO 341'
        },
        {
            telefoneA: '(11) 4126-2312',
            logradouro: 'R RUA BELA CINTRA 420'
        },
    ]);
    it('Deve transformar o endereço', function () {
        assert.strictEqual(result[0].logradouro, 'Alameda Santos, 1000');
        assert.strictEqual(result[1].logradouro, 'Rua General Carneiro, 341');
        assert.strictEqual(result[2].logradouro, 'Rua Bela Cintra, 420');
    });
    const mascaraTel = /(\d{2}<break time="\d+ms"\/>)+\d+/;
    it('Deve transformar o telefone', function () {
        result.forEach(x => assert.match(x.telefoneVoz, mascaraTel));
    });
});
