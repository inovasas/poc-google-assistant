#!/usr/bin/env node

//@ts-check
'use strict';

const readConf = require('./src/helpers/readConfig');
readConf.readEnvFile();
const app = require('./src/index');
const logger = require('./src/helpers/logger');
const http = require('http');

/**
 *
 * @param {string | number | any} val
 */
const normalizePort = val => {
    const port = parseInt(val, 10);

    if (isNaN(port)) {
        return val;
    }

    if (port >= 0) {
        return port;
    }

    return false;
};

/**
 *
 * @param {Object} error
 */
const onError = error => {
    if (error.syscall !== 'listen') {
        throw error;
    }

    const bind = typeof port === 'string' ? 'Pipe ' + port : 'Port ' + port;

    switch (error.code) {
        case 'EACCES':
            logger.error(bind + ' requires elevated privileges');
            process.exit(1);
            break;

        case 'EADDRINUSE':
            logger.error(bind + ' is already in use');
            process.exit(1);
            break;

        default:
            logger.error(error);
            throw error;
    }
};

const onListening = () => {
    let addr = server.address();
    if (typeof addr === 'object') {
        addr = addr.address + ':' + addr.port;
    }
    logger.info('Servidor de pé na ' + addr);
};

const port = normalizePort(process.env.PORT || '3008');

app.set('port', port);

const server = http.createServer(app);
// server.timeout = 900000;
server.listen(port);
server.on('error', onError);
server.on('listening', onListening);

/**
 *
 * @param {Error | number} obj
 */
const onException = obj => {
    if (obj instanceof Error) {
        logger.error('Exceção não tratada porém capturada!', {
            error: obj,
        });
    }
    if (obj instanceof Number) {
        logger.info(`Saindo graciosamente com o número de saida ${obj}`);
    }
};

process.on('uncaughtException', onException);

process.on('exit', onException);

process.on('warning', onException);

process.on('unhandledRejection', (reason, promise) => {
    if (reason.code === 'ERR_HTTP_HEADERS_SENT') {
        logger.error('Tentativa de mandar resposta novamente de request já respondido. ' +
            'Provavelmente request atrasado de timeout que terminou agora.');
    } else {
        logger.error('Rejeição de promessa não tratada!!!', {
            reason, promise, reasonString: reason.toString(),
        });
    }
});


module.exports = {
    normalizePort: normalizePort,
    onException: onException,
};
