// IMPORTS ////////////////////////////////////////////////////////////////////
const gulp = require('gulp');
const clean = require('gulp-clean');
const copy = require('gulp-copy');
const runSequence = require('run-sequence');

// TASKS //////////////////////////////////////////////////////////////////////
function onError(taskName) {
    return () => {
        console.log(`Error running task ${taskName}.`);
        process.exit(1);
    };
}

gulp.on('error', onError);

gulp.task('copy-json', () => {
    return gulp.src([
        './**.js'
    ])
        .pipe(copy('./_src/', {
            prefix: 1
        }));
});

gulp.task('copy-bin', () => {
    return gulp.src([
        './bin/**'
    ])
        .pipe(copy('./_src/', {
            prefix: 1
        }));
});

gulp.task('copy-src', () => {
    return gulp.src([
        './src/**'
    ])
        .pipe(copy('./_src/src/', {
            prefix: 1
        }));
});

gulp.task('copy-modules', () => {
    return gulp.src([
        './node_modules/**'
    ])
        .pipe(copy('./_src/node_modules/', {
            prefix: 1
        }));
});

gulp.task('build', function (callback) {
    runSequence(
        'clean', [
            'copy-json',
            'copy-modules',
            'copy-bin',
            'copy-src'
        ],
        callback);
});

gulp.task('default', function () {

});

gulp.task('clean', function () {
    return gulp
        .src(['./_src/**'], {
            read: false
        }).pipe(clean());
});