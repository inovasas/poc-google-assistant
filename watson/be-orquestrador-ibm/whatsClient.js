'use strict';

const express = require('express');
const bodyParser = require('body-parser');
const request = require('request');
const readline = require('readline');
const uuid = require('uuid');

const senderID = '5511989532684';

const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout,
});

const app = express();
app.use(bodyParser.json({
    limit: '50mb',
}));
app.use(bodyParser.urlencoded({
    parameterLimit: 100000,
    limit: '50mb',
    extended: false,
}));
app.use((req, res) => {
    console.log('Got answer:');
    console.log(req.body);
    res.status(200).send({ status: 'OK' });
});

app.listen(3000, () => {
    console.log('Waiting for whats msgs.');
    rl.question('What do you say?\n> ', answer);
});

async function answer(text) {
    text = (text && text.toString()) || '';
    if (!text) {
        console.log('No text, try again.');
        rl.question('What do you say?\n> ', answer);
        return;
    }
    console.log('Sending whatsapp message: ', text);
    request('http://localhost:3008/api/v1/chat/whatsapp', {
        method: 'POST',
        json: true,
        body: {
            total: 1,
            data: [{
                id: uuid.v4(),
                source: senderID,
                origin: '551130049723',
                userProfile: {
                    name: 'Victor',
                },
                campaignId: 100,
                correlationId: '...',
                campaignAlias: '...',
                flowId: '....',
                extraInfo: '...',
                message: {
                    type: 'TEXT',
                    messageText: text,
                },
                receivedAt: Date.now(),
                receivedDate: new Date(),
            }],
        },
    }, (err) => {
        if (!err) {
            console.log('Sent successfully');
        } else {
            console.error(err);
        }
        rl.question('What do you say?\n> ', answer);
    });
}

