'use strict';
// @ts-check

const status = require('http-status-codes');
const mongoose = require('mongoose');
const _ = require('lodash');

async function getAuthenticationToken(req, res, next) {
    const authHeader = req.headers.authorization;
    try {
        const [type, token] = authHeader.split(' ');
        if (_.lowerCase(type) !== 'apikey') {
            throw new Error('Tipo inválido de token');
        }
        const result = await mongoose.connection.db.collection('authTokens').findOne({
            token,
        });
        if (result) {
            next();
        } else {
            throw 'Token inválido';
        }
    } catch (err) {
        // FIXME: Diferenciar erros de verdade, e.g. erro do mongo ou throw pra pular para cá
        res.status(status.UNAUTHORIZED).json({
            status: 'err',
            message: err.message || 'Autenticação inválida',
        });
    }
}

module.exports = {
    getAuthenticationToken,
};