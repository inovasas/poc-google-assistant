'use strict';
// @ts-check

const status = require('http-status-codes');
const { loadCredentials, filterTokenList } = require('../repositories/watsonData');

async function readTokenMetadata(req, res, next) {
    req.locals = req.locals || {};
    const tokens = req.headers['x-external-token'];
    if (!tokens) {
        return next();
    }
    const token = Array.isArray(tokens) ? tokens[0] : tokens.toString();
    const body = req.body;
    try {
        const abWorkspaceList = await loadCredentials(token);
        const conversationId = body.conversation_id || body.context.conversation_id;
        const wcsData = await filterTokenList(abWorkspaceList, conversationId);
        req.locals.wdsCredentials = wcsData;
        return next();
    } catch (err) {
        res.status(status.INTERNAL_SERVER_ERROR);
        return next({
            status: 'err',
            message: 'Erro ao ler as credenciais associadas a esse external token:\n' +
                token + '\nO erro causado foi:\n' + JSON.stringify(err, undefined, 1),
        });
    }
}

module.exports = {
    readTokenMetadata,
};
