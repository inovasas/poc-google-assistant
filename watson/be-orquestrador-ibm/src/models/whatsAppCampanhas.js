'use strict';

const mongoose = require('mongoose');

const whatsAppCampanhasSchema = new mongoose.Schema({
    name: {
        type: String,
    },
    namespace: {
        type: String,
        required: true,
    },
    elementName: {
        type: String,
        required: true,
    },
    horaMinima: {
        type: String,
        required: true,
    },
    horaMaxima: {
        type: String,
        required: true,
    },
    diasSemana: {
        type: [Number],
        required: true,
    },
    feriado: {
        type: Boolean,
        required: true,
    },
    prioritario: {
        type: Boolean,
        required: true,
    },
    assistantExternalToken: {
        type: String,
        required: true,
    },
    ttl: {
        type: Number, //minutos
        required: true,
    },
});
const WhatsAppCampanhas = mongoose.model('WhatsAppCampanhas', whatsAppCampanhasSchema);

module.exports = WhatsAppCampanhas;
