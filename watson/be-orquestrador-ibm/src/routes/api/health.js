'use strict';

const express = require('express');
const mongoose = require('mongoose');

const router = express.Router();
router.route('/').get((req, res) => {
    res.status(200).json({
        status: 200,
        path: process.cwd() ? process.cwd() : null,
        gid: process.getegid() ? process.getegid() : null,
        uid: process.geteuid() ? process.geteuid() : null,
        memoryUsage: process.memoryUsage(),
        message: 'Alive and kicking',
    });
});
router.route('/livenezz').get((req, res) => {
    return livenezz(res);
});

module.exports = router;

/**
 *
 * @param {object} res
 */
async function livenezz(res) {
    const dbStatus = mongoose.connection.readyState === 1;
    // const watsonStatus = await WA.listWorkspaces()
    //     .then(() => true)
    //     .catch(() => false);

    res.status(dbStatus /* && watsonStatus */ ? 200 : 500).json({
        mongoDB: dbStatus ? 'Up' : 'Down',
        // Watson: watsonStatus ? 'Up' : 'Down'
    });
}