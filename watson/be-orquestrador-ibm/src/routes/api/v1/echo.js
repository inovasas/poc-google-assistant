'use strict';

const logger = require('../../../helpers/logger');
const express = require('express');

const router = express.Router();
router.use('/', (req, res) => {
    logger.info('Request de echo', { req });
    res.send({ status: 'OK' });
});

module.exports = router;
