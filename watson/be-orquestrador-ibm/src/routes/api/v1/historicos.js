//@ts-check
'use strict';

const express = require('express');
const historico = require('../../../controllers/v1/historico');

const router = express.Router();

router.route('/')
    .post(historico.saveHistorico);

module.exports = router;
