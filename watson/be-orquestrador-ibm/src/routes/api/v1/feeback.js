'use strict';
// @ts-check

const express = require('express');
const { saveFeedback } = require('../../../controllers/v1/feedback');

const router = express.Router();

router
    .route('/')
    .post(saveFeedback);

module.exports = router;
