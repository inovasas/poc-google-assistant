//@ts-check
'use strict';

const express = require('express');
const chat = require('../../../controllers/v1/chat');
const VoiceResponse = require('twilio').twiml.VoiceResponse;

const router = express.Router();

router
    .route('/')
    .post(async (req, res, next) => {
        try {
            await chat.conversation(req, res);
        } catch (err) {
            next(err);
        }
    });

router
    .route('/twilio')
    .post(chat.twilio);

router.route('/twilio/error')
    .post((_, res) => {
        const voice = new VoiceResponse();
        voice.say({
            voice: 'alice',
            language: 'pt-BR',
        }, 'Infelizmente, nosso serviço está indisponível no momento. ' +
        'Por favor, tente novamente mais tarde.');
        voice.hangup();
        res.type('text/xml').send(voice.toString());
    });

router
    .route('/google')
    .post(chat.google);

router
    .route('/voice')
    .post(chat.voice);

module.exports = router;
