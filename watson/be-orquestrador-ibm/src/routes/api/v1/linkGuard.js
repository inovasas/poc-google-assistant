//@ts-check
'use strict';

// !FIXME: SECURITY !!! CRLF TOKENS !!!

const express = require('express');
const linkGuard = require('../../../controllers/v1/linkGuard');

const router = express.Router();

router
    .route('/')
    .get(linkGuard.linkGuard);

module.exports = router;