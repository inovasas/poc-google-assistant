//@ts-check
'use strict';

const express = require('express');
const mongoose = require('mongoose');
const httpStatus = require('http-status-codes');

const router = express.Router();

router.route('/cep/:cep').get(async (req, res) => {
    /** @type {string} */
    const cep = req.params.cep;
    const cepObj = await mongoose.connection.db.collection('cepMunicipioIbge')
        .findOne({ cep: parseInt(cep.substr(0, 5), 10) }, {
            projection: { uf: 1, regiao: 1, cep: 1, _id: 0 },
        });
    if (cepObj) {
        cepObj.cep = cep;
        return res.status(httpStatus.OK).json({ status: 'ok', body: cepObj });
    } else {
        return res.status(httpStatus.NOT_FOUND).json({
            message: 'CEP Nao encontrado',
            status: 'error',
        });
    }
});

router
    .route('/veiculos/:marca')
    .get(async (req, res) => {
        const marca = req.params.marca;

        const veiculos = await mongoose.connection.db.collection('veiculos')
            .find(
                { fipe_marca: new RegExp('^' + marca + '$', 'i') },
                { projection: { fipe_name: 1 } },
            ).toArray();

        if (veiculos) {
            const arrVeiculos = veiculos.map(x => x.fipe_name);
            return res.status(httpStatus.OK).json({
                status: 'ok',
                body: arrVeiculos,
            });
        } else {
            return res.status(httpStatus.NOT_FOUND).json({
                message: 'Nenhum veículo encontrado',
                status: 'error',
            });
        }
    });


module.exports = router;
