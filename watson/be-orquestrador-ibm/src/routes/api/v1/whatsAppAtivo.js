'use strict';
// @ts-check

const express = require('express');
const {
    getCampaigns,
    getCampaignById,
    saveCampaign,
    updateCampaign,
    deleteCampaign,
} = require('../../../controllers/v1/chat/whatsapp/campanhasAtivas');

const {
    findConversa,
    searchActives,
} = require('../../../controllers/v1/chat/whatsapp/historicoAtivos');

const { atualizaNovoCaso } = require('../../../repositories/salesforce');

const middleware = require('../../../middlewares/authenticationToken');


const router = express.Router();
router.use(middleware.getAuthenticationToken);

router
    .route('/historico')
    .get(searchActives);

router
    .route('/historico/:case_id')
    .get(findConversa);

router
    .route('/')
    .get(getCampaigns)
    .post(saveCampaign);

router
    .route('/:id')
    .get(getCampaignById)
    .put(updateCampaign)
    .delete(deleteCampaign);

router.route('/atualizarCaso/:case_id')
    .post(atualizaNovoCaso);

module.exports = router;
