'use strict';
// @ts-check

const express = require('express');

const middleware = require('../../../middlewares/authenticationToken');
const {
    listHistorico,
    findHistorico,
    listFeedback,
    getReasons } = require('../../../repositories/oficina');


const router = express.Router();
router.use(middleware.getAuthenticationToken);

router
    .route('/listHistorico')
    .post(listHistorico);

router
    .route('/findHistorico')
    .post(findHistorico);

router
    .route('/listFeedback')
    .post(listFeedback);

router
    .route('/getReasons')
    .post(getReasons);


module.exports = router;
