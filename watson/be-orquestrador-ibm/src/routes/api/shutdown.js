'use strict';

const express = require('express');
const router = express.Router();

router.route('/').get((req, res) => {
    process.kill(0);
    // process.kill(-1);

    return res.status(500).json({
        success: true,
    });
});

module.exports = router;