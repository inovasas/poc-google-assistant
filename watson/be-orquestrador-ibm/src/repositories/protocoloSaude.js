'use strict';

const request = require('request-promise-native');
const rxjs = require('rxjs');
const rxjsOps = require('rxjs/operators');
const auth = require('./sensediaAuth');
const errors = require('../helpers/errors');

const urlSalesforce = process.env.URL_SALESFORCE_CLIENTE ||
    'https://apisulamerica.sensedia.com/cliente/api/v1';
const URL_SALESFORCE_SAUDE = process.env.URL_SALESFORCE_API_SAUDE;

const pathAtendimentoManifestacoesProtocolos = '/atendimentos/manifestacoes/protocolos';

/**
 * @typedef {object} Manifestacao
 * @property {string} ansParam
 * @property {string} carteirinha
 */

/**
 * @param {Manifestacao} manifestacao
 */
async function gerarProtocolo(manifestacao) {
    const accesstoken = await auth.tokenSalesforce(URL_SALESFORCE_SAUDE);
    const protocolo = await novoProtocolo(accesstoken.access_token, manifestacao);
    return protocolo;
}

/**
 * @param {Manifestacao} manifestacao
 */
function novoProtocolo(token, manifestacao) {
    let tries = 0;
    return rxjs.from(request({
        url: urlSalesforce + pathAtendimentoManifestacoesProtocolos,
        headers: {
            access_token: token,
            client_id: auth.AUTH_HEADERS.client_id,
            'content-type': 'application/json',
        },
        method: 'POST',
        body: {
            'Manifestacao': {
                'origem': 'CHAT',
                'status': 'Novo',
                'ANSParam': manifestacao.ansParam,
                'tipoDeRegistro': 'Solicitacao',
                'areaDeNegocio': 'Saúde',
                'assunto': 'CHAT - ' + manifestacao.carteirinha,
            },
        },
        json: true,
        agentOptions: {
            rejectUnauthorized: false,
        },
        resolveWithFullResponse: true,
    })).pipe(
        rxjsOps.first(x => x.statusCode === 200, undefined),
        rxjsOps.map(x => x.body.ManifestacaoRetorno.protocolo),
        rxjsOps.catchError((err, obs) => {
            if (err.statusCode >= 400 && err.statusCode < 500) {
                console.error('Erro ao criar protocolo no salesforce:', err.body);
                throw new errors.ErroOrquestrador('Erro desconhecido ao criar protocolo', 500);
            }
            if (tries++ < 3) {
                return obs;
            } else {
                console.error('Erro desconhecido ao criar protocolo salesforce:', err.message);
                throw new errors.ErroOrquestrador('Erro desconhecido ao criar protocolo', 500);
            }
        }),
    ).toPromise();
}

module.exports = {
    gerarProtocolo,
};
