//@ts-check
'use strict';
const { safeRequest } = require('../helpers/safeRequest');
const logger = require('../helpers/logger');
const { from, empty } = require('rxjs');
const { omit, clone } = require('lodash');
const { concatMap, delay } = require('rxjs/operators');
const { getDetailedHistory } = require('../repositories/chat');

/**
 * @typedef {Object} EnderecoWhatsApp
 * @property {'location'} response_type
 * @property {string} street
 * @property {string?} city
 * @property {string} state
 * @property {string?} zip
 * @property {string?} country
 * @property {string?} country_code
 */

/**
 * @template T
 * @param {T | T[]} elem
 * @returns {T[]}
 */
function toArray(elem) {
    if (Array.isArray(elem)) {
        return elem;
    } else {
        return [elem];
    }
}

/**
 * Recebe saída do watson 
 * 
 * @param {any} wcsOut Saída do watson
 * @param {string | string[]} destinatarios Numeros de telefone
 * @returns {Promise<void>}
 */
function processWatsonOutput(wcsOut, destinatarios) {
    const reqBase = {
        destinations: toArray(destinatarios).map(x => ({ destination: x })),
    };
    return from(wcsOut.generic).pipe(
        concatMap(/** @param {any} saida */ saida => {
            const reqBody = clone(reqBase);
            switch (saida.response_type) {
                case 'text':
                    if (!saida.text) {
                        return empty();
                    }
                    reqBody.message = { messageText: saida.text };
                    break;
                case 'location':
                    reqBody.message = { location: omit(saida, ['response_type']) };
                    break;
                default:
                    logger.warn('Saída do watson não reconhecida', { wcsOut });
                    return empty();
            }
            return callWavy(reqBody).pipe(delay(2800));
        }),
    ).toPromise();
}

function callWavy(body) {
    return safeRequest({
        method: 'POST',
        url: process.env.URL_WAVY + '/send',
        json: true,
        headers: {
            username: process.env.WAVY_USER,
            authenticationToken: process.env.WAVY_TOKEN,
        },
        body,
        strictSSL: true,
    }, 10);
}

/**
 * Envia mensagem
 * 
 * @param {string | string[]} mensagem Texto da mensagem
 * @param {string | string[]} destinatarios Numeros de telefone
 */
function sendWhatsappMsg(mensagem, destinatarios) {
    const nums = toArray(destinatarios).map(x => ({ destination: x }));
    const mensagens = toArray(mensagem).filter(x => x).map(msg => ({
        message: {
            messageText: msg,
        },
        destinations: nums,
    }));
    return from(mensagens).pipe(
        concatMap(msg => callWavy(msg).pipe(delay(1500))),
    ).toPromise();
}

/**
 * 
 * @param {Object} ctx 
 */
async function sendHumanChat(ctx) {
    // const history = await getDetailedHistory(ctx.conversation_id) || [];
    const history = [];

    const messages = [];
    for (const x of history) {
        messages.push({
            senderType: x.typeAgent === 1 ? 'BOT' : 'CUSTOMER',
            message: {
                type: 'TEXT',
                text: x.text,
            },
        });
    }

    const body = {
        chatUserId: ctx.phoneNumber,
        agentHandoff: true,
        agentsGroupId: process.env.WAVY_CHAT_GROUPID,
        messages,
    };

    logger.debug('Transferindo atendimento para humano...', { phoneNumber: ctx.phoneNumber });

    return safeRequest({
        method: 'POST',
        url: 'https://api-messaging.movile.com/v1/channel/whatsapp/conversation',
        json: true,
        headers: {
            username: process.env.WAVY_USER,
            authenticationToken: process.env.WAVY_TOKEN,
        },
        body,
        strictSSL: true,
    }, 10).toPromise();
}

module.exports = { sendWhatsappMsg, processWatsonOutput, sendHumanChat };
