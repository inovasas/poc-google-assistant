//@ts-check
'use strict';

const { safeRequest } = require('../helpers/safeRequest');
const logger = require('../helpers/logger');
const mongoose = require('mongoose');

/**
 * Salva no mongo a resposta do lead do formulário
 *
 * @param {any} form Objeto formulário
 * @param {string} conversationId Conversation ID
 * @param {import('request').Response} response Resposta de http
 */
function gravarLead(form, conversationId, response) {
    var coll = mongoose.connection.collection('leadSalesforce');
    coll.insertOne({
        url: process.env.URL_SALESFORCE,
        form: form,
        conversationId: conversationId,
        responseStatus: response.statusCode,
        responseBody: response.body,
        createdAt: new Date(),
    });
}

/**
 * Salva no mongo a resposta do lead do formulário
 *
 * @param {any} form Objeto formulário
 * @param {string} conversationId Conversation ID
 * @param {import('request').Response} response Resposta de http
 */
function gravarLeadFollowize(form, conversationId, response) {
    var coll = mongoose.connection.collection('leadFollowize');
    coll.insertOne({
        url: process.env.URL_FOLLOWIZE,
        form: form,
        conversationId: conversationId,
        responseStatus: response.statusCode,
        responseBody: response.body,
        createdAt: new Date(),
    });
}

/**
 *
 * @param {Promise<Object>} context
 */
module.exports.sendForm = async (context, conversationId) => {
    logger.debug('context do sendform ****', { context });

    safeRequest({
        url: process.env.URL_SALESFORCE,
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded',
        },
        method: 'POST',
        form: context,
        agentOptions: {
            rejectUnauthorized: false,
        },
    }).subscribe((resp) => {
        logger.debug('resp do envio ', { resp });
        gravarLead(context, conversationId, resp);
    }, (error) =>
        logger.error('Erro ao enviar lead para o salesforce!', { error }),
    );
};

/**
 *
 * @param {Promise<Object>} context
 */
module.exports.sendLeadFollowize = async (context, conversationId) => {
    context = await context;
    safeRequest({
        url: process.env.URL_FOLLOWIZE,
        headers: {
            'Content-Type': 'application/json',
        },
        method: 'POST',
        json: context,
        agentOptions: {
            rejectUnauthorized: false,
        },
    }).subscribe((resp) => {
        gravarLeadFollowize(context, conversationId, resp);
    }, (error) => logger.error('Erro ao enviar lead para followise', { error }));
};
