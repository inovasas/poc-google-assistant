'use strict';

const mongoose = require('mongoose');
const _ = require('lodash');
const boom = require('http-errors');
const moment = require('moment');

/**
 * @param {string} id
 */
module.exports.listById = async (id) => {
    const historico = mongoose.connection.db.collection('historico');
    const head = await historico.findOne({
        $or: [
            { case_id: id },
            { conversation_id: id },
        ],
    });
    if (!head) {
        throw new boom.NotFound('Mensagem não encontrada.');
    }
    const response = {
        case_id: head.case_id,
        conversation_id: head.conversation_id,
        mensagens: '',
    };
    return historico.find({
        conversation_id: head.conversation_id,
    }).sort({ _id: 1 }).project({
        text: true,
        username: true,
        enviado: true,
        createdAt: true,
        recebido: true,
        lido: true,
        typeAgent: true,
    }).toArray().then(x => x.reduce((final, msg) => {
        const datas = _.chain(msg)
            .pick(['enviado', 'recebido', 'lido', 'createdAt'])
            .mapValues(x => moment(x).format('ddd HH:mm:ss'))
            .value();
        if (msg.typeAgent === 1 || msg.typeAgent === 4) {
            const { enviado = '-', recebido = '-', lido = '-' } = datas;
            final.mensagens += `--- SulAmérica (enviado ${enviado}, recebido ${recebido}, ` +
                `lido ${lido}):\n${msg.text}\n`;
        } else {
            final.mensagens += `--- ${msg.username} (recebido: ${datas.createdAt}):\n` +
                `${msg.text}\n`;
        }
        return final;
    }, response));
};

module.exports.listByPhoneAndPeriod = async ({ phoneNumber, startDate, endDate }) => {
    const searchObj = {
        username: { $eq: phoneNumber },
        createdAt: {
            $gte: startDate,
            $lte: endDate,
        },
        case_id: { $exists: true },
    };
    const findObj = _.chain(searchObj)
        .mapValues(x => _.pickBy(x))
        .pickBy(_.negate(_.isEmpty))
        .value();

    return mongoose.connection.db.collection('historico')
        .find(findObj)
        .project({
            conversation_id: 1,
            text: 1,
            enviado: 1,
            recebido: 1,
            lido: 1,
            username: 1,
            case_id: 1,
        })
        .sort({ createdAt: 1 })
        .toArray();
};