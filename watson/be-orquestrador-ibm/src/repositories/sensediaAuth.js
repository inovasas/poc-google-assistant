//@ts-check
'use strict';

const { safeRequest } = require('../helpers/safeRequest');
const { redisGetPromise: redisGet } = require('../helpers/utils');
const logger = require('../helpers/logger');
const redis = require('../helpers/connectRedis');
const _ = require('lodash');

const CLIENT_ID = process.env.SENSEDIA_CLIENTID;
const CLIENT_SECRET = process.env.SENSEDIA_CLIENTSECRET;
const Authorization = Buffer.from(CLIENT_ID + ':' + CLIENT_SECRET).toString('base64');

const CLIENT_ID_PREVIA = process.env.SENSEDIA_CLIENTID_PREVIA;
const CLIENT_SECRET_PREVIA = process.env.CLIENT_SECRET_PREVIA;
const AuthorizationPrevia = Buffer.from(
    CLIENT_ID_PREVIA + ':' + CLIENT_SECRET_PREVIA).toString('base64');

const CLIENT_ID_CUIDADO = process.env.SENSEDIA_CLIENTID_CUIDADO;
const CLIENT_SECRET_CUIDADO = process.env.CLIENT_SECRET_CUIDADO;
const AuthorizationCuidado = Buffer.from(
    CLIENT_ID_CUIDADO + ':' + CLIENT_SECRET_CUIDADO).toString('base64');

const AUTH_HEADERS = Object.freeze({
    'Accept': 'application/json',
    'client_id': CLIENT_ID,
    'Authorization': 'Basic ' + Authorization,
    'Content-Type': 'application/json',
});

const AUTH_HEADERS_AUTO = Object.freeze({
    'Authorization': 'Basic ' + process.env.SENSEDIA_AUTHORIZATION_AUTO + '==',
    'client_id': process.env.SENSEDIA_CLIENTID_AUTO,
    'Accept': 'application/json',
    'Content-Type': 'application/json',
});

const AUTH_HEADERS_BENEFICIARIO = Object.freeze({
    ...AUTH_HEADERS,
    'codigoAcesso': '-1',
    'senha': '-1',
});

// access_token para APIs de prévia de reembolso
const AUTH_HEADERS_BENEFICIARIO_PREVIA = Object.freeze({
    ...AUTH_HEADERS,
    'Authorization': 'Basic ' + AuthorizationPrevia,
    'codigoAcesso': process.env.CD_API_BENEFICIARIO,
    'senha': process.env.SENHA_ACESSO_API_BENEFICIARIO,
    'client_id': CLIENT_ID_PREVIA,
});

// access_token para API de atualização SF (cuidado coordenado)
const AUTH_HEADERS_CUIDADO = Object.freeze({
    'Accept': 'application/json',
    'client_id': CLIENT_ID_CUIDADO,
    'Authorization': 'Basic ' + AuthorizationCuidado,
    'Content-Type': 'application/json',
});

function tokenPagamentos() {
    const urlPgto = process.env.URL_PAGAMENTOS ||
        'https://apisulamerica.sensedia.com/pagamentos/api/v3';
    return getSensediaToken('tokenPagamento', urlPgto + '/oauth/access-token');
}

function tokenVpp() {
    return getSensediaToken('tokenVpp',
        'https://apisulamerica.sensedia.com/oauth/access-token',
        { grant_type: 'client_credentials' });
}

function tokenSalesforce(url) {
    return getSensediaToken('tokenSalesforce',
        url + '/oauth/access-token');
}

function tokenCliente() {
    return getSensediaToken('tokenCliente',
        process.env.URL_SALESFORCE_CLIENTE + '/oauth/access-token');
}

function tokenBeneficiario() {
    return getSensediaToken(
        'tokenBeneficiario',
        process.env.API_SAUDE_BENEFICIARIO + '/oauth/access-token',
        {},
        AUTH_HEADERS_BENEFICIARIO,
    );
}

function tokenPrestador() {
    return getSensediaToken('tokenPrestadorSaude',
        'https://apisulamerica.sensedia.com/saude/prestador/api/v1/oauth/access-token');
}

function tokenApolice() {
    return getSensediaToken('tokenApoliceAuto',
        process.env.API_APOLICE_AUTO + '/oauth/access-token',
        {},
        AUTH_HEADERS_AUTO);
}

function tokenReembolso() {
    return getSensediaToken('tokenReembolso',
        process.env.API_REEMBOLSO + '/oauth/access-token');
}
function tokenPrevia() {
    return getSensediaToken('tokenPrevia',
        process.env.API_SAUDE_BENEFICIARIO_PREVIA + '/oauth/access-token',
        {},
        AUTH_HEADERS_BENEFICIARIO_PREVIA,
    );
}
function tokenCuidado() {
    return getSensediaToken('tokenCuidado',
        process.env.API_CUIDADO_COORDENADO_MANIFESTACAO + '/oauth/access-token',
        {},
        AUTH_HEADERS_CUIDADO,
    );
}

async function getSensediaToken(redisKey, authEndpoint, body, headers = AUTH_HEADERS) {
    const ultimoToken = JSON.parse(await redisGet(redisKey) || 'null');
    // Quando o token expirar, ele vai expirar do redis também e retornar nulo
    if (ultimoToken == null) {
        logger.debug(`Cache miss: ${redisKey} gerando outro token.`);
        const options = {
            url: authEndpoint,
            headers: headers,
            method: 'POST',
            json: true,
        };

        if (!_.isEmpty(body)) {
            options.body = body;
        }

        try {
            // tratar status resp.
            const token = await safeRequest(options).toPromise();
            const tokenStr = JSON.stringify(token);
            logger.debug(`Token ${redisKey} regenerado com sucesso às ` +
                new Date().toString() + ' expira em 60 segundos');
            // 60 por que oauth nao e respeitado pela sensedia
            redis.client.set(redisKey, tokenStr, 'EX', 60);
            return token;
        } catch (error) {
            logger.error('Erro durante a geração de token sensedia!!', { error });
            throw new Error('Erro de autenticação na API de ' + redisKey);
        }
    } else {
        // O token não é refresh no redis pra representar a real expiração do token
        return ultimoToken;
    }
}

module.exports.tokenReembolso = tokenReembolso;
module.exports.tokenPagamentos = tokenPagamentos;
module.exports.tokenPrestador = tokenPrestador;
module.exports.tokenCliente = tokenCliente;
module.exports.tokenSalesforce = tokenSalesforce;
module.exports.tokenApolice = tokenApolice;
module.exports.tokenVpp = tokenVpp;
module.exports.tokenBeneficiario = tokenBeneficiario;
module.exports.tokenPrevia = tokenPrevia;
module.exports.tokenCuidado = tokenCuidado;
module.exports.AUTH_HEADERS = AUTH_HEADERS;
module.exports.AUTH_HEADERS_BENEFICIARIO = AUTH_HEADERS_BENEFICIARIO;
module.exports.AUTH_HEADERS_BENEFICIARIO_PREVIA = AUTH_HEADERS_BENEFICIARIO_PREVIA;
module.exports.AUTH_HEADERS_AUTO = AUTH_HEADERS_AUTO;