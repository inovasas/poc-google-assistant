'use strict';

const mongoose = require('mongoose');
const redis = require('../helpers/connectRedis').client;


/**
 *
 * @param {string} tokenId
 */
module.exports.findTokenInRedis = (tokenId) => {
    return new Promise((resolve, reject) => {
        if (!redis.connected) {
            reject('Redis não conectado!');
            return;
        }

        redis.get(tokenId, function (err, reply) {
            if (err) {
                reject(err);
            } else {
                resolve(reply);
            }
        });
    });
};

/**
 *
 * @param {string} conversationId
 */
module.exports.findConversationIdInRedis = (conversationId) => {
    return new Promise((resolve, reject) => {
        redis.get(conversationId, function (err, reply) {
            if (err) {
                reject(err);
            } else {
                resolve(reply);
            }
        });
    });
};

/**
 *
 * @param {string} tokenId
 */
module.exports.findTokenInDb = async (tokenId) => {
    const collection = mongoose.connection.db.collection('workspace');
    const results = await collection.find(
        {
            token: tokenId,
        },
        {
            projection: {
                workspace_id: 1,
                username: 1,
                password: 1,
                chatbase_key: 1,
                save_history: 1,
                discovery_collection_id: 1,
                discovery_environment_id: 1,
                discovery_threshold: 1,
            },
        }).toArray();

    if (results.length > 0) {
        return results;
    } else {
        throw 'Nenhum workspace encontrado';
    }
};

/**
 *
 * @param {string} conversationId
 */
module.exports.findConversationIdInDb = (conversationId) => {
    return new Promise((resolve, reject) => {
        mongoose.connection.db.collection('historico', function (err, collection) {
            collection.find({
                conversation_id: conversationId,
            }).toArray(function (err, results) {
                if (err) {
                    reject(err);
                } else {
                    const historyObj = [];

                    results.forEach(e => {
                        historyObj.push({
                            workspace_id: e.workspace_id,
                            conversation_id: e.conversation_id,
                        });
                    });

                    resolve(historyObj);
                }
            });
        });
    });
};

/**
 *
 * @param {Object} req
 * @param {String} workspaceId
 * @param {String} oid
 */
module.exports.logRequestMessage = (req, workspaceId, oid) => {
    if (!req.context.conversation_id) {
        return;
    }

    mongoose.connection.db.collection('historico', function (err, collection) {
        try {
            collection.insertOne({
                oid: oid,
                workspace_id: workspaceId,
                conversation_id: req.context.conversation_id,
                text: req.input.text.trim(),
                username: req.context.usuario,
                filtroObj: req.context.filtroObj,
                typeAgent: 2,
                createdAt: new Date(),
                updatedAt: new Date(),
            });
        } catch (err) {
            console.log(err);
        }
    });
};

/**
 *
 * @param {Object} resp
 * @param {String} workspaceId
 * @param {String} oid
 */
module.exports.logResponseMessage = async (resp, workspaceId, oid) => {
    let outputText = '';

    if (resp.output.text instanceof Array) {
        resp.output.text.forEach(e => {
            outputText += (e + ' ');
        });
    } else {
        outputText = resp.output.text;
    }

    const collection = mongoose.connection.db.collection('historico');
    try {
        return collection.insertOne({
            oid: oid,
            workspace_id: workspaceId,
            conversation_id: resp.context.conversation_id,
            text: outputText.trim(),
            intents: resp.intents,
            entities: resp.entities,
            username: resp.context.usuario,
            filtroObj: resp.context.filtroObj,
            typeAgent: 1,
            createdAt: new Date(),
            updatedAt: new Date(),
        });
    } catch (err) {
        console.log(err);
    }

};

/**
 *
 * @param {Object} resp
 * @param {String} workspaceId
 * @param {String} oid
 */
module.exports.saveFeedback = (resp, workspaceId, oid) => {
    mongoose.connection.db.collection('feedback', function (err, collection) {
        try {
            collection.insertOne({
                oid: oid,
                workspace_id: workspaceId,
                conversation_id: resp.context.conversation_id,
                success: resp.output.feedback,
                reason: resp.output.justificativa_feedback,
                intents: resp.intents,
                username: resp.context.usuario,
                filtroObj: resp.context.filtroObj,
                reviewed: false,
                createdAt: new Date(),
                updatedAt: new Date(),
            });
        } catch (err) {
            console.log(err);
        }
    });
};

/**
 *
 * @param {String} sugestao
 */
module.exports.findSuggestionInDb = (sugestao) => {
    return new Promise((resolve, reject) => {
        mongoose.connection.db.collection('colecao', function (err, collection) {
            collection.find({
                nome: sugestao,
            }).toArray(function (err, results) {
                if (err) {
                    reject(err);
                } else {
                    resolve(results);
                }
            });
        });
    });
};

/**
 *
 * @param {string} callSid
 */
module.exports.findCallSidInRedis = (callSid) => {
    return new Promise((resolve, reject) => {
        redis.get(callSid, function (err, reply) {
            if (err) {
                reject(err);
            } else {
                resolve(reply);
            }
        });
    });
};

/**
 *
 * @param {String} conversation_id
 */
module.exports.getAllHistory = async (conversation_id) => {
    let text = '';

    const messages = await mongoose.connection.db.collection('historico').aggregate([
        { $match: { conversation_id: conversation_id } },
        { $sort: { _id: 1 } },
        { $project: { text: '$text' } },
    ]).toArray();

    messages.forEach(msg => {
        text += msg.text + '\n';
    });

    return text;
};

/**
 *
 * @param {String} conversation_id
 */
module.exports.getDetailedHistory = async (conversation_id) => {
    const messages = await mongoose.connection.db.collection('historico').aggregate([
        { $match: { conversation_id: conversation_id } },
        { $sort: { _id: 1 } },
        { $project: { text: '$text', typeAgent: '$typeAgent' } },
    ]).toArray();

    return messages;
};