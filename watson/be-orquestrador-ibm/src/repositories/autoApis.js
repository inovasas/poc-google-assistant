'use strict';

// const request = require('request');
const { safeRequest } = require('../helpers/safeRequest');
const auth = require('./sensediaAuth');
const { redisGetPromise: redisGet, capitalize } = require('../helpers/utils');
const redis = require('../helpers/connectRedis');
const errors = require('../helpers/errors');
const _ = require('lodash');
const moment = require('moment');
const { map } = require('rxjs/operators');

const URL_PGTO = 'https://apisulamerica.sensedia.com/pagamentos/api/v1';

/**
 * Função especializada para construir um objeto representando uma apolice de auto
 * a partir de um objeto generico MDM.
 *
 * @param {any} produtoMdm Payload MDM do produto de auto
 */
async function buildProdutoAuto(produtoMdm) {
    const CHAVE_REDIS = 'produtosAuto_' + produtoMdm.cpfId;
    /** @type{any[]} */
    let listaAuto = JSON.parse(await redisGet(CHAVE_REDIS) || '[]');
    if (!Array.isArray(listaAuto) || listaAuto.length === 0) {
        const token = await auth.tokenApolice();
        listaAuto = JSON.parse(await getApoliceList(produtoMdm.cpfId, token.access_token));
        redis.client.set(CHAVE_REDIS, JSON.stringify(listaAuto), 'EX', token.expires_in - 3);
    }
    const apolice = listaAuto.find((x) =>
        _.get(x, ['apolice', 'numeroApolice']).toString() === produtoMdm.agrmntNum4);
    if (_.isNil(apolice)) {
        throw new Error('Apolice de auto retorna no MDM e nao na API de auto!!!');
    }
    const { veiculo, segurado, docVigente } = apolice;
    const placa = veiculo.placa;

    // máscara para exibir somente últimos 2 dígitos da placa
    const placaMask = placa.replace(placa.substring(0, 3), '```***```')
        .replace(placa.substring(placa.length - 4, placa.length - 2), '```**```');

    if (!docVigente) {
        throw new errors.ProdutoInativo();
    }
    return {
        cpf: segurado.cpfCnpj,
        status: produtoMdm.idcStatus === 'A' ? 'Ativo' : 'Inativo',
        produto: 'AUTO',
        nome: capitalize(segurado.nome),
        descricao: `Auto (placa ${placaMask})`
            .replace(/\s\s+/g, ' '),
        apolice,
        planoBU: 'Auto',
    };
}

async function getBoleto(apolice, sucursal) {

    let access_token = '';

    const resp = await auth.tokenPagamentos();
    access_token = resp.access_token;

    let result = await getParcelas(apolice, sucursal, access_token);
    result = JSON.parse(result);

    const proximaParcela = result.filter(parcela => parcela.status !== 'Pago')
        .sort((a, b) => a.numeroParcela - b.numeroParcela).shift();
    let boleto = {};
    try {
        boleto = await getBoletoIPTE(apolice, sucursal, proximaParcela,
            access_token);

        proximaParcela.ipte = boleto.ipte;
        proximaParcela.dataVencimento = boleto.dataVencimento ?
            boleto.dataVencimento : proximaParcela.dataVencimento;
    } catch (err) {
        // logger.error('Não foi possível gerar boleto de auto.', { error: err });

        return proximaParcela;
    }

    return proximaParcela;
}

function getParcelas(apolice, sucursal, token) {
    return safeRequest({
        url: URL_PGTO + '/parcelas/auto',
        qs: {
            origem: 'CH',
            sistemaOrigem: 'APA',
            numeroApolice: apolice,
            codigoSucursal: sucursal,
        },
        headers: {
            access_token: token,
            client_id: auth.AUTH_HEADERS.client_id,
        },
    }).toPromise();
}

function getBoletoIPTE(apolice, sucursal, parcela, token) {
    const fmtData = 'DDMMYYYY';
    const qs = {
        tipoRetorno: '3',
        origem: 'CH',
        numeroApolice: apolice,
        numeroParcela: parcela.numeroParcela,
        codigoSucursal: sucursal,
        vencido: false,
    };
    // Formata a data de vencto da API
    const vencto = moment(parcela.dataVencimento, fmtData);
    // Mais velho que 60 dias não pode
    if (vencto.isBefore(moment().subtract(60, 'days'))) {
        throw new Error('Boleto muito antigo!');
    }
    // Se vencido...
    if (!vencto.isValid() || vencto.isBefore()) {
        // Pede pra API vencimento amanhã
        qs.dataVencimento = moment().add(1, 'day').format(fmtData);
        qs.vencido = true;
    }
    return safeRequest({
        url: URL_PGTO + '/boletos/auto/segundaVia',
        qs,
        headers: {
            access_token: token,
            client_id: auth.AUTH_HEADERS.client_id,
            'Content-Type': 'application/json',
            'Accept': 'application/json',
        },
        json: true,
    }).pipe(map(x => {
        x.dataVencimento = qs.dataVencimento;
        return x;
    })).toPromise();
}

async function getApolices(cpf) {

    let access_token = '';
    const resp = await auth.tokenApolice();
    access_token = resp.access_token;

    let result = await getApoliceList(cpf, access_token);
    result = JSON.parse(result);

    return result;
}

function getApoliceList(cpf, token) {
    return safeRequest({
        url: process.env.API_APOLICE_AUTO + '/apolices/auto',
        qs: {
            cpfCnpj: cpf,
            origem: 'CH',
        },
        headers: {
            access_token: token,
            client_id: auth.AUTH_HEADERS_AUTO.client_id,
        },
    }).toPromise();
}

module.exports = {
    getBoleto,
    getApolices,
    buildProdutoAuto,
};
