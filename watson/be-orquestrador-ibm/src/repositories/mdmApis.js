'use strict';

const auth = require('./sensediaAuth');
const auto = require('./autoApis');
const saude = require('./saudeApis');
const logger = require('../helpers/logger');
const { safeRequest } = require('../helpers/safeRequest');
const ctx = require('../helpers/validateContext');
const errors = require('../helpers/errors');
const util = require('../helpers/utils');
const _ = require('lodash');

const ANO_MILLIS = 12 * 30 * 24 * 60 * 60 * 1000;

ctx.posWatsonReducers.consulta_mdm = async function (watsonData, wcsResp) {
    const ctx = wcsResp.context;
    const produtos = await buildProdutos(ctx.documento, ctx.filtro_produto);
    ctx.nome = util.capitalize(produtos.map(x => x.nome).find(x => x));
    ctx.produtos_mdm = produtos;
    ctx.descricoes_produtos = _(produtos)
        .filter(k => _.negate(_.isNil)(k.descricao))
        .map((x, i) => ` *${i + 1}*) ${x.descricao}`)
        .value();
};

async function stubProdutoBuilder(produto) {
    return {
        descricao: util.capitalize(produto.brdLobDesc),
        nome: util.capitalize(produto.fullNm),
        produto: produto.brdLobDesc,
    };
}

async function buildProdutos(cpf, produto, produtosMdm) {
    if (_.isEmpty(produtosMdm)) {
        produtosMdm = await getProdutos(cpf, produto);
    }

    const part = _.partition(produtosMdm, function (produto) {
        return produto.brdLobCd === 'AUT' || produto.brdLobCd === 'SAU';
    });

    produtosMdm = part[0].concat(_.uniqBy(part[1], 'brdLobCd'));

    let produtos = [];
    for (const produto of produtosMdm) {
        
        /** @type {(produto: any) => Promise<any>} */
        let productBuilder;
        
         if (produto.brdLobCd === 'SAU') {

            productBuilder = saude.buildProdutoSaude;
            
        } 
        else {
            // TODO: Mais produtos!
            productBuilder = stubProdutoBuilder;
        }
        let produtoPronto;
        try {
            produtoPronto = await productBuilder(produto);
        } catch (error) {
            if (error instanceof errors.ProdutoInativo) {
                continue;
            } else {
                const msg = 'Erro ao construir o produto de ' + produto.brdLobDesc +
                    ' com o CPF ' + produto.cpfId;
                logger.error(msg, { error, produto });
                throw new Error(msg);
            }
        }
        produtos.push(produtoPronto);
    }
    produtos = produtos.filter(x => x.produto === 'SAUDE');
    produtos = produtos.slice(0,1);
    return produtos;
}

async function getProdutos(cpf, produto) {

    let access_token = '';
    const resp = await auth.tokenCliente();
    access_token = resp.access_token;
    try {
        const result = JSON.parse(await getContratos(cpf, access_token) || 'null');

        if (result == null) {
            return [];
        }
        const ativos = result.filter(contrato => {
            const hoje = Date.now();
            let vencimento = Date.parse(contrato.agrmntEndDt);
            if (contrato.brdLobCd === 'SAU') {
                vencimento -= ANO_MILLIS;
            }
            return contrato.idcStatus === 'A' || (
                // Se tiver produto, tem que ser igual ao especificado
                (!produto || contrato.brdLobDesc === produto) &&
                // Se tiver vencimento, tem que ser posterior a hoje
                (!contrato.agrmntEndDt || vencimento > hoje));
        });
        return ativos;
    } catch (error) {
        if (error.statusCode === 404) {
            return [];
        }
        logger.error('Erro ao listar os contratos do segurado!', { error, errorObj: error });
        throw error;
    }
}

function getContratos(cpf, token) {
    return safeRequest({
        uri: process.env.API_CLIENTES + cpf +
            '/contratos',
        headers: {
            access_token: token,
            client_id: auth.AUTH_HEADERS.client_id,
        },
        timeout: 5000,
    }).toPromise();
}


module.exports = {
    getProdutos, buildProdutos,
};
