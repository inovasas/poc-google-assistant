//@ts-check
'use strict';

const request = require('request-promise-native');
const _ = require('lodash');
const auth = require('./sensediaAuth');
const errors = require('../helpers/errors');
const logger = require('../helpers/logger');

/**
 *
 * @param {String} especialidade
 * @param {number} lat
 * @param {number} long
 * @param {String} produto
 * @param {String} plano
 * @returns {Promise<any[]>} lista de especialistas
 */
async function getSpecialists(especialidade, lat, long, produto, plano) {
    const access_token = (await auth.tokenPrestador()).access_token;

    const result = await getSpecialistsAddresses(especialidade, lat, long, access_token,
        produto, plano);
    const filteredList = _.chain(result)
        .uniqBy(x => x.endereco.endereco)
        .take(30)
        .map(prox => _.pick(prox, ['telefones', 'nomeFantasia', 'endereco',
            'posicaoGeografica']))
        .value();
    return filteredList;
}

/**
 * Função que consulta a latitude e longitude de dado endereço usando o serviço de
 * localização do google.
 * 
 * @param {string} endereco
 * @returns {Promise<any>} Objeto com latitude e longitude
 */
async function getLatLong(endereco) {
    const httpResponse = await request({
        uri: 'https://maps.googleapis.com/maps/api/geocode/json',
        qs: {
            address: endereco,
            language: 'pt-BR',
            region: 'br',
            components: 'country:BR',
            key: process.env.GEOCODING_KEY,
        },
        json: true,
        resolveWithFullResponse: true,
    });
    if (httpResponse.statusCode === 200) {
        return httpResponse.body;
    }
    logger.error('Google geocoding rejeitou a chamada.', {
        resposta: httpResponse.body,
    });
    throw new errors.ErroOrquestrador('Erro na chamada de geocoding do google!', 500);
}

/**
 *
 * @param {String} specialty
 * @param {number} lat
 * @param {number} long
 * @param {String} access_token
 * @param {String} produto
 * @param {String} plano
 */
async function getSpecialistsAddresses(specialty, lat, long, access_token, produto, plano) {
    let especialidade;
    let categoria;
    const codigoEspecialidade = specialty.substring(3, 7);

    switch (specialty.substring(8, 12)) {
        case 'pron':
            categoria = 1;
            especialidade = '';
            break;
        case 'hosp':
            categoria = 2;
            especialidade = '';
            break;
        default:
            categoria = 3;
            especialidade = codigoEspecialidade;
            break;
    }
    const httpResponse = await request({
        uri: 'https://apisulamerica.sensedia.com/saude/prestador/api/v1/prestadores',
        qs: {
            canal: 2,
            categoria: categoria,
            especialidade: especialidade,
            latitude: lat,
            longitude: long,
            produto: produto,
            plano: plano,
        },
        headers: {
            access_token: access_token,
            client_id: auth.AUTH_HEADERS.client_id,
            'app-version': 1,
        },
        resolveWithFullResponse: true,
        json: true,
    });

    if (httpResponse.statusCode === 200) {
        return httpResponse.body;
    }
}

module.exports = {
    getLatLong,
    getSpecialists,
};