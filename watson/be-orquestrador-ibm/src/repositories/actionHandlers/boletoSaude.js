'use strict';

const saudeApis = require('../saudeApis');

async function boletoSaudeWhatsapp(wcsReq, wcsMsg) {
    const carteirinha = wcsMsg.context.produto_atual
        .carteirinha['codigo-beneficiario'];
    const parcelaAberto = await saudeApis.getBoletoIndividual(carteirinha);
    const produtoAtual = wcsMsg.context.produto_atual;

    if (parcelaAberto) {
        if (parcelaAberto.ipte) {
            wcsMsg.output.generic[0].text = wcsMsg.output.generic[0].text
                .replace('%%vencimento%%', parcelaAberto.dataVencimento)
                .replace('%%numero_parcela%%', parcelaAberto.numeroParcela)
                .replace('%%valor%%', 'R$ ' + parcelaAberto.valorEmitido);
            wcsMsg.output.generic.splice(1, 0, {
                response_type: 'text',
                text: parcelaAberto.ipte,
            });
        } else {
            wcsMsg.output.generic[0].text = wcsMsg.output.msg_erro_ipte
                .replace('%%numero_parcela%%', parcelaAberto.numeroParcela)
                .replace('%%vencimento%%', parcelaAberto.dataVencimento)
                .replace('%%tipoPagamento%%', parcelaAberto.datatipoPagamento);
        }
    } else if (produtoAtual.tipoProduto === 'Adesão') {
        switch (produtoAtual.empresaAdesao) {
            case 'Qualicorp':
                wcsMsg.output.generic[0].text = wcsMsg.output.msg_adesao_qualicorp;
                break;
            case 'Extramed':
                wcsMsg.output.generic[0].text = wcsMsg.output.msg_adesao_extramed;
                break;
            case 'EV':
                wcsMsg.output.generic[0].text = wcsMsg.output.msg_adesao_ev;
                break;
            case 'Comando da Aeronáutica':
                wcsMsg.output.generic[0].text = wcsMsg.output.msg_adesao_aeronautica;
                break;
            default:
                wcsMsg.output.generic[0].text = wcsMsg.output.msg_adesao;
                break;
        }
    } else if (produtoAtual.tipoProduto === 'Empresarial') {
        wcsMsg.output.generic[0].text = wcsMsg.output.msg_empresarial;
    } else {
        wcsMsg.output.generic[0].text = wcsMsg.output.msg_erro_sem_parcelas_aberto;
    }
}

module.exports[2001] = boletoSaudeWhatsapp;
