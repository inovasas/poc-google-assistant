'use strict';
const saudeApis = require('../saudeApis');
const moment = require('moment');
var logger = require('../../helpers/logger');

async function previaParametros(wcReq, wcRes) {

    const { codigoTipoPrevia } = wcRes.context;
    const codigoTuss = wcRes.context.codigo_tuss || '';
    const valorInformadoProfissional = wcRes.context.valorPrevia.toFixed(2);
    const uf = wcRes.context.ufPrevia;
    const codigoBeneficiario = wcRes.context.produto_atual.carteirinha['codigo-beneficiario'];

    const dateFormat = 'DD/MM/YYYY';
    const dataRecebida = moment(wcReq.context.dataPrevia, dateFormat).toDate();
    const dataAtendimento = moment(dataRecebida).format(dateFormat);

    try {
        logger.debug('inicio busca de prévia');

        let codigoServico = await saudeApis.
            getCodigoServico(codigoTuss, codigoTipoPrevia);
        logger.debug('result codigoServico', { codigoServico });

        codigoServico = codigoServico[0].codigo;

        const codigoParametro = await saudeApis.
            getPreviaParametros(codigoServico);
        logger.debug('result codigoParametro', { codigoParametro });

        const codigoParametroServico = codigoParametro.servicos[0].codigoParametroServico;
        const classificacaoServico = codigoParametro.servicos[0].classificacaoServico;
        const codigoParametroPrevia = codigoParametro.codigoParametroPrevia;
        const codigoProfissional = codigoParametro.servicos[0]
            .profissionais[0].codigoProfissional;

        const body = {
            uf,
            servicos: [
                {
                    codigoParametroServico,
                    classificacaoServico,
                    profissionais: [
                        {
                            codigoProfissional,
                            valorInformadoProfissional,
                        },
                    ],
                },
            ],
            dataAtendimento,
            codigoParametroPrevia,
        };

        const valorPrevia = await saudeApis.
            getPreviaReembolso(codigoBeneficiario, body);

        logger.debug('valor de prévia de reembolso calculado', { valorPrevia });

        const valorCalculado = valorPrevia.calculoServicos[0].profissionais[0].valorCalculado;

        wcRes.context.valorReembolsoConsulta = valorCalculado;

    } catch (e) {
        logger.error('Erro ao buscar prévia de reembolso', { e });
        wcRes.context.erroPrevia = true;
        throw e;
    }
}

module.exports = {
    6008: previaParametros,
};