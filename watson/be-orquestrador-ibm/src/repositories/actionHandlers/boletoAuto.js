'use strict';

const autoApis = require('../autoApis');

async function boletoAutoWhatsApp(wcsReq, wcsMsg) {
    let produtoAuto;
    try {
        produtoAuto = wcsMsg.context.produto_atual.apolice;
        wcsMsg.context.veiculo = produtoAuto.veiculo.descricao;
        wcsMsg.context.placa = produtoAuto.veiculo.placa;
        wcsMsg.context.nome = produtoAuto.segurado.nome;
    } catch (err) {
        wcsMsg.output.generic = [{
            text: wcsMsg.output.msg_erro_tecnico,
            response_type: 'text',
        }];
        return;
    }

    let parcelaAberto;
    try {
        parcelaAberto = await autoApis.getBoleto(produtoAuto.apolice.numeroApolice,
            produtoAuto.apolice.codSucursal);
    } catch (err) {
        parcelaAberto = null;
    }

    if (parcelaAberto) {
        if (parcelaAberto.numeroParcela === '01' &&
            parcelaAberto.tipoPagamento === 'Débito em Conta') {
            wcsMsg.output.generic[0].text = wcsMsg.output.msg_primeira_parcela_dcc;
        } else if (parcelaAberto.tipoPagamento === 'Débito em Conta') {
            wcsMsg.output.generic[0].text = wcsMsg.output.msg_pagamento_dcc;
        } else if (parcelaAberto.status === 'Pendente') {
            // parcela pendente (?)
            wcsMsg.output.generic[0].text = wcsMsg.output.msg_pendente
                .replace('%%numero_parcela%%', parcelaAberto.numeroParcela)
                .replace('%%valor%%', 'R$ ' + parcelaAberto.valor);
        } else if (parcelaAberto.status === 'Pago por Indenização Integral') {
            wcsMsg.output.generic[0].text = wcsMsg.output.msg_pago_indenizacao_integral
                .replace('%%numero_parcela%%', parcelaAberto.numeroParcela)
                .replace('%%dataVencimento%%', parcelaAberto.dataVencimento);
        } else if (parcelaAberto.status === 'Pago para Reemissão') {
            wcsMsg.output.generic[0].text = wcsMsg.output.msg_pago_reemissao
                .replace('%%numero_parcela%%', parcelaAberto.numeroParcela)
                .replace('%%dataVencimento%%', parcelaAberto.dataVencimento);
        } else if (parcelaAberto.status === 'Cancelado por Falta de Pagamen') {
            wcsMsg.output.generic[0].text = wcsMsg.output.msg_cancelado_falta_pagamento;
        } else if (parcelaAberto.status === 'Cancelamento a Pedido') {
            wcsMsg.output.generic[0].text = wcsMsg.output.msg_cancelado_pedido;
        } else if (parcelaAberto.ipte) {
            wcsMsg.output.generic[0].text = wcsMsg.output.generic[0].text
                .replace('%%vencimento%%', parcelaAberto.dataVencimento)
                .replace('%%numero_parcela%%', parcelaAberto.numeroParcela)
                .replace('%%valor%%', 'R$ ' + parcelaAberto.valor);
            wcsMsg.output.generic.splice(1, 0, {
                response_type: 'text',
                text: parcelaAberto.ipte,
            });
        } else {
            wcsMsg.output.generic[0].text = wcsMsg.output.msg_erro_ipte
                .replace('%%numero_parcela%%', parcelaAberto.numeroParcela)
                .replace('%%vencimento%%', parcelaAberto.dataVencimento)
                .replace('%%tipoPagamento%%', parcelaAberto.datatipoPagamento);
        }
    } else {
        wcsMsg.output.generic[0].text = wcsMsg.output.msg_erro_sem_parcelas_aberto;
    }
}

module.exports[1009] = boletoAutoWhatsApp;
