'use strict';

const logger = require('../../helpers/logger');
const fs = require('fs');
const path = require('path');
const _ = require('lodash');

const ACTIONS = {};

fs.readdir(__dirname, { withFileTypes: true }, (error, files) => {
    if (error) {
        logger.error('Erro ao carregar as actions.', { error });
        return;
    }
    _.assign(ACTIONS, ..._(files).filter(x =>
        x.isFile() && x.name !== path.basename(__filename) && path.extname(x.name) === '.js',
    ).map(x => './' + path.basename(x.name, '.js')).map(x => require(x)).value());
});

async function action(wcsReq, wcsRes, number) {
    const handler = ACTIONS[number];
    if (!handler) {
        logger.error('Action ' + number + ' não cadastrada', { wcsRes });
        return;
    }
    await handler(wcsReq, wcsRes);
}

module.exports = action;
