'use strict';

const prestadores = require('../prestadores');
const { sprintf } = require('sprintf-js');
const { capitalize } = require('../../helpers/utils');
const _ = require('lodash');

/** @typedef {import('../wavyWhatsapp').EnderecoWhatsApp} EnderecoWhatsApp */

async function redeReferenciada(wcsReq, wcsRes) {
    const produtoAtual = wcsRes.context.produto_atual;
    const produto = produtoAtual.carteirinha.produto['codigo-produto'];
    const plano = produtoAtual.codigoPlano;

    const { input: enderecoBusca } = wcsReq;
    const {
        output: { generic },
        context: { especialidade, templateRedeRef, listaPrestadoresVazia },
    } = wcsRes;
    let {
        context: {
            latitude: lat,
            longitude: lng,
            quantidadeRedeRef: qntRedeRef,
            lista_prestadores: listaPrestadores,
        },
    } = wcsRes;

    qntRedeRef = Number.parseInt(qntRedeRef, 10);

    if (!lat || !lng) {
        const latLong = await prestadores.getLatLong(enderecoBusca);

        if (latLong.results.length < 1) {
            generic.splice(0, 1, { response_type: 'text', text: listaPrestadoresVazia });
            delete wcsRes.context.lista_prestadores;
            return;
        }
        lat = latLong.results[0].geometry.location.lat;
        lng = latLong.results[0].geometry.location.lng;
    }

    if (listaPrestadores == null) {
        try {
            listaPrestadores = _.take(await prestadores.getSpecialists(
                especialidade, lat, lng, produto, plano), qntRedeRef * 3);
        } catch (err) {
            generic.splice(0, 1, { response_type: 'text', text: listaPrestadoresVazia });
            delete wcsRes.context.lista_prestadores;
            return;
        }
    } else if (listaPrestadores.length < 1) {
        generic.splice(0, 1, { response_type: 'text', text: listaPrestadoresVazia });
        delete wcsRes.context.lista_prestadores;
        return;
    }

    const proximosPrestadores = listaPrestadores.splice(0, qntRedeRef || 3);
    const finalizacao = generic.pop();
    wcsRes.context.resultados = proximosPrestadores;
    for (const prestador of proximosPrestadores) {
        const { endereco, posicaoGeografica } = prestador;
        generic.push({
            response_type: 'text',
            text: sprintf(templateRedeRef, prestador),
        });
        /** @type {EnderecoWhatsApp} */
        generic.push({
            response_type: 'location',
            geoPoint: posicaoGeografica.latitude + ',' + posicaoGeografica.longitude,
            name: capitalize(prestador.nomeFantasia),
            address: endereco.endereco + ', ' + endereco.numeroEndereco,
        });
    }
    generic.push(finalizacao);
    wcsRes.context.lista_prestadores = listaPrestadores;
}

module.exports = {
    6001: redeReferenciada,
};
