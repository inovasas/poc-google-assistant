'use strict';
const saudeApis = require('../saudeApis');
var moment = require('moment');
var logger = require('../../helpers/logger');

async function listaReembolso(wcsReq, wcsRes) {

    const codBeneficiario = wcsRes.context.produto_atual
        .carteirinha['codigo-beneficiario'];
    const dataFinal = moment().format('YYYYMMDD');
    const dataInicial = moment().subtract(6, 'months').format('YYYYMMDD');
    let lista_reembolso = [];

    const inicioListaReembolso = wcsRes.context.inicioListaReembolso;
    const fimListaReembolso = wcsRes.context.fimListaReembolso;
    let informacoesReembolso = [];

    if (!('informacoesReembolso' in wcsRes.context)) {
        try {
            informacoesReembolso = await saudeApis.
                checkStatusReembolso(dataInicial, dataFinal, codBeneficiario);

            informacoesReembolso = informacoesReembolso
                .filter(x => x.codBeneficiario === codBeneficiario);

            informacoesReembolso.forEach(e => {

                e.datInclusao = e.datInclusao.substring(0, 10);

                if (!Number.isInteger(e.vlrApresentado)) {
                    e.vlrApresentado = e.vlrApresentado.toString().replace('.', ',');
                } else {
                    e.vlrApresentado = e.vlrApresentado.toFixed(2);
                }
            });

            wcsRes.context.informacoesReembolso = informacoesReembolso;

        } catch (err) {
            if (err.statusCode === 404) {
                logger.error('Status de reembolso não encontrado: ', { e: err });
                wcsRes.context.statusBusca = false;
                return;
            } else {
                logger.error('Erro na busca de status de reembolso: ', err);
                throw err;
            }
        }
    }

    // splice na lista completa de resultados para deixar no contexto
    // apenas os N primeiros
    lista_reembolso = wcsRes.context.informacoesReembolso
        .splice(inicioListaReembolso, fimListaReembolso);
    wcsRes.context.lista_status_reembolso = lista_reembolso;
}

module.exports = {
    5001: listaReembolso,
};