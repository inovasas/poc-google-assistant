'use strict';

const redis = require('../helpers/connectRedis').client;
const utils = require('../helpers/utils');
const errors = require('../helpers/errors');
const auth = require('./sensediaAuth');
const request = require('request-promise-native');
const _ = require('lodash');
const mongoose = require('mongoose');
const logger = require('../helpers/logger');
const moment = require('moment');

const URL_SALESFORCE_SAUDE = process.env.URL_SALESFORCE_API_SAUDE;
const URL_SALESFORCE_AUTO = process.env.URL_SALESFORCE_API_AUTO;
const SENSEDIA_CLIENT_ID = process.env.SENSEDIA_CLIENTID;
const LAYOUTS_RESOURCE_PATH = '/manifestacoes/descricao/layouts';
const SUBTIPOS_RESOURCE_PATH = '/manifestacoes/subtipos';
const SEGURADOS_RESOURCE_PATH = '/segurados';
const MANIFESTACOES_RESOURCE_PATH = '/manifestacoes';
const CPF_REGEX = /^(\d{3})\.?(\d{3})\.?(\d{3})-?(\d{2})$/;
const TEL_REGEX = /^([1-9]{2})? ?\(?0?([1-9]{2})\)? ?(9?[2-9]\d{3})(( - )|[ -])?(\d{4})$/;
const HOUR_FORMAT = 'HH:mm:ss';


/**
 * @param {string} recordTypeName
 * @param {string} subTipoPesquisaC - The employee's department.
 * 
 * @param {string} areaNegocio
 * @param {string} userId 
 *
 * @param {object} manifestacao Corpo da manifestação
 * @param {string} manifestacao.nomeDaPessoaC
 * @param {string} manifestacao.categoriaC
 * @param {string} manifestacao.type
 * @param {string} manifestacao.tipoC
 * @param {string} manifestacao.subject
 * @param {string} manifestacao.description
 * @param {string} manifestacao.status
 * @param {string} manifestacao.motivoStatus
 * @param {string} manifestacao.origin
 * @param {string} manifestacao.formaDeContatoC
 * @param {string} manifestacao.IdSessionC
 * @param {string} manifestacao.phoneNumber
 *
 */
async function gerarManifestacao(recordTypeName, subTipoPesquisaC, areaNegocio, userId,
    manifestacao) {
    const baseUrl = await setBaseUrl(areaNegocio);

    const accesstoken = (await auth.tokenSalesforce(baseUrl)).access_token;

    const recordTypeId = await getRecordTypeId(accesstoken, baseUrl, recordTypeName);

    const identificadorSubtipo = await getIdentificadorSubTipo(accesstoken, baseUrl,
        manifestacao, subTipoPesquisaC, areaNegocio);

    const codigosegurado = await buscarAccountSegurado(accesstoken, baseUrl, userId);

    const envioManifestacao = await enviarManifestacao(accesstoken, baseUrl, manifestacao,
        identificadorSubtipo, codigosegurado, recordTypeId, areaNegocio);

    return envioManifestacao;
}

async function setBaseUrl(areaNegocio) {
    if (areaNegocio.startsWith('Saúde')) {
        return URL_SALESFORCE_SAUDE;
    } else {
        return URL_SALESFORCE_AUTO;
    }
}

/**
 * @param {string} token
 * @param {string} recordTypeName
 *
 */
async function getRecordTypeId(token, baseUrl, recordTypeName) {
    let recordTypeId = await utils.redisGetPromise(recordTypeName);

    if (_.isNil(recordTypeId)) {
        recordTypeId = await buscarRecordTypeIdSolicitacao(token, baseUrl, recordTypeName);
        redis.set(recordTypeName, recordTypeId, 'EX', 3000);
    }

    return recordTypeId;
}

/**
 * @param {string} token 
 * @param {string} recordTypeName
 *
 */
async function buscarRecordTypeIdSolicitacao(token, baseUrl, recordTypeName) {
    const httpResponse = await request({
        url: baseUrl + LAYOUTS_RESOURCE_PATH,
        headers: {
            access_token: token,
            client_id: SENSEDIA_CLIENT_ID,
        },
        method: 'GET',
        json: true,
        resolveWithFullResponse: true,
    });
    const { body } = httpResponse;
    if (httpResponse.statusCode === 200 && !_.isEmpty(body)) {
        const recordTypeId = body.recordTypeMappings.filter((recordType) =>
            recordType.name === recordTypeName,
        )[0].recordTypeId;
        return recordTypeId;
    } else {
        throw new Error('Erro ao buscar type id solicitação!');
    }
}

/**
 * Assign the project to an employee.
 *
 * @param {object} manifestacao
 * @param {string} manifestacao.categoriaC - The employee's department.
 * @param {string} manifestacao.tipoC - The employee's department.
 * @param {string} subTipoPesquisaC - The employee's department.
 *
 * @param {string} areaNegocio - areaNegocio.
 *
 */
async function getIdentificadorSubTipo(accesstoken, baseUrl, manifestacao, subTipoPesquisaC,
    areaNegocio) {

    const subtipoKey = manifestacao.categoriaC + manifestacao.tipoC + subTipoPesquisaC +
        areaNegocio;

    let identificadorSubtipo = await utils.redisGetPromise(subtipoKey);

    if (!identificadorSubtipo || identificadorSubtipo === 'undefined') {
        identificadorSubtipo = await buscarSubTipoByMapeamento(accesstoken, baseUrl,
            manifestacao.categoriaC, manifestacao.tipoC, subTipoPesquisaC, areaNegocio);
        redis.set(subtipoKey, identificadorSubtipo, 'EX', 3000);
    }

    return identificadorSubtipo;
}

/**
 * Assign the project to an employee.
 *
 * @param {string} token
 * @param {string} categoriaC
 * @param {string} baseUrl
 * @param {string} tipoC
 * @param {string} subTipoPesquisaC
 * 
 * @param {string} areaNegocio
 *
 */
async function buscarSubTipoByMapeamento(token, baseUrl, categoriaC, tipoC, subTipoPesquisaC,
    areaNegocio) {
    const httpResponse = await request({
        url: baseUrl + SUBTIPOS_RESOURCE_PATH,
        qs: {
            categoria: categoriaC,
            tipo: tipoC,
            nome: subTipoPesquisaC,
            areaNegocio: areaNegocio,
        },
        headers: {
            access_token: token,
            client_id: SENSEDIA_CLIENT_ID,
        },
        json: true,
        method: 'GET',
        resolveWithFullResponse: true,
    });
    if (httpResponse.statusCode !== 200) {
        throw new errors.ErroOrquestrador('Erro ao consultar o subtipo no salesforce', 500);
    }
    return _.get(httpResponse, ['body', '0', 'identificador'], '');
}

/**
 * 
 * @param {String} token 
 * @param {String} baseUrl
 * @param {String} userId
 */
async function buscarAccountSegurado(token, baseUrl, userId) {
    if (userId === null) {
        return null;
    }

    if (baseUrl === URL_SALESFORCE_SAUDE) {
        return await buscarCodigoSegurado(token, URL_SALESFORCE_SAUDE,
            userId.substring(3), 'codigoSegurado');
    } else if (baseUrl === URL_SALESFORCE_AUTO) {
        const cpf = await formatarCPF(userId);
        return await buscarCodigoSegurado(token, URL_SALESFORCE_AUTO,
            cpf, 'Id');
    }
}

/**
 * @param {String} token
 * @param {String} baseUrl
 * @param {String} userId
 * @param {String} responseField
 */
async function buscarCodigoSegurado(token, baseUrl, userId, responseField) {
    /** @type {import('request').Response} */
    const httpResponse = await request({
        url: baseUrl + SEGURADOS_RESOURCE_PATH,
        qs: {
            codigoCarteira: userId,
        },
        headers: {
            access_token: token,
            client_id: SENSEDIA_CLIENT_ID,
        },
        method: 'GET',
        resolveWithFullResponse: true,
        json: true,
    });
    if (httpResponse.statusCode !== 200) {
        throw new errors.ErroOrquestrador('Erro ao pegar account do segurado', 500);
    }
    return _.get(httpResponse.body, ['0', responseField], null);
}

/** 
 * 
 * @param {object} manifestacao - manifestacao
 * 
 * @param {string} manifestacao.nomeDaPessoaC - nome
 * @param {string} manifestacao.categoriaC - categoriaC
 * @param {string} manifestacao.type - Type
 * @param {string} manifestacao.tipoC - TIPO__c
 * @param {string} manifestacao.subject - Subject
 * @param {string} manifestacao.description - Description
 * @param {string} manifestacao.status - Status
 * @param {string} manifestacao.motivoStatus - MOTIVO_DO_STATUS__c
 * @param {string} manifestacao.origin - Origin
 * @param {string} manifestacao.formaDeContatoC - FORMA_DE_CONTATO__c
 * @param {string} manifestacao.IdSessionC - Id_Session__c
 * @param {string} manifestacao.phoneNumber - TELEFONE_PESSOA__c
 * 
 * @param {string} recordTypeId - RecordTypeId
 * @param {string} areaNegocio - RecordTypeId
 * 
*/
async function enviarManifestacao(token, baseUrl, manifestacao, identificadorSubtipo,
    codigosegurado, recordTypeId, areaNegocio) {
    let httpResponse;

    const bodyObj = {
        'NOME_DA_PESSOA__c': manifestacao.nomeDaPessoaC,
        'AccountId': codigosegurado,
        'RecordTypeId': recordTypeId,
        'CATEGORIA__c': manifestacao.categoriaC,
        'Type': manifestacao.type,
        'TIPO__c': manifestacao.tipoC,
        'SUBTIPO_PESQUISA__c': identificadorSubtipo,
        'Subject': manifestacao.subject,
        'Description': manifestacao.description,
        'Status': manifestacao.status,
        'MOTIVO_DO_STATUS__c': manifestacao.motivoStatus,
        'Origin': manifestacao.origin,
        'FORMA_DE_CONTATO__c': manifestacao.formaDeContatoC,
        'TELEFONE_PESSOA__c': (await formatarTel(manifestacao.phoneNumber)),
        'Id_Session__c': manifestacao.IdSessionC,
    };

    if (areaNegocio !== '') {
        bodyObj.NME_PESSOA_CONTATO_EMAIL__c = 'whatsapp@sulamerica.com.br';
    }

    try {
        httpResponse = await request({
            url: baseUrl + MANIFESTACOES_RESOURCE_PATH,
            headers: {
                access_token: token,
                client_id: SENSEDIA_CLIENT_ID,
                'content-type': 'application/json',
            },
            method: 'POST',
            body: bodyObj,
            json: true,
            agentOptions: {
                rejectUnauthorized: false,
            },
            resolveWithFullResponse: true,
        });
        if (httpResponse.statusCode !== 201) {
            throw new errors.ErroOrquestrador(
                'Erro durante a criação de manifestação do SalesForce!!! Status: ' +
                httpResponse.statusCode + ' corpo da resposta:\n' + httpResponse.body, 500);
        }
        return httpResponse.body;
    } catch (err) {
        if (err instanceof errors.ErroOrquestrador) {
            throw err;
        }
        const MSG_ERR = 'Erro desconhecido durante a criação de manifestação do SalesForce!!!';
        logger.error(MSG_ERR, { err });
        throw new errors.ErroOrquestrador(MSG_ERR, 500);
    }
}

async function formatarCPF(cpf) {
    return cpf.replace(CPF_REGEX, '$1.$2.$3-$4');
}

async function formatarTel(tel) {
    return tel.replace(TEL_REGEX, '($2) $3-$6');
}

/**
 * 
 * @param {String} conversation_id 
 * @param {String} statusCasoWhatsapp 
 */
async function atualizaCaso(conversation_id, statusCasoWhatsapp) {
    try {
        logger.debug('Início de atualização de caso', { conversation_id, statusCasoWhatsapp });

        const caseId = await mongoose.connection.db.collection('historico').find(
            { conversation_id, case_id: { $exists: true } },
        ).toArray();

        const horarios = await mongoose.connection.db.collection('historico').find(
            { conversation_id },
            { projection: { createdAt: 1 } },
        ).toArray();

        const inicioAtendimento = moment(horarios[0].createdAt).utcOffset('-0300')
            .format(HOUR_FORMAT) || '';
        const fimAtendimento = moment(horarios[horarios.length - 1].createdAt)
            .utcOffset('-0300').format(HOUR_FORMAT) || '';

        const body = {
            request: [
                {
                    caseId: caseId[0].case_id,
                    statusCasoWhatsapp,
                    inicioAtendimento,
                    fimAtendimento,
                },
            ],
        };

        logger.debug('body SF: ', { body });

        enviaCasoSF(body);
    } catch (err) {
        logger.error('Erro ao buscar dados para atualização de case SF',
            { statusCasoWhatsapp, err });
    }
}

function atualizaNovoCaso(req, res) {
    const caseId = req.params.case_id;
    const horarioAtendimento = moment().utcOffset('-0300').format(HOUR_FORMAT) || '';

    const body = {
        request: [
            {
                caseId,
                statusCasoWhatsapp: 'Disparada a Mensagem',
                inicioAtendimento: horarioAtendimento,
                fimAtendimento: horarioAtendimento,
            },
        ],
    };

    logger.debug('atualização de novo caso', { body });
    enviaCasoSF(body);

    res.status(200).json({
        status: 'ok',
        message: 'Caso enviado ao SalesForce',
    });
}

/**
 * 
 * @param {Object} body 
 */
async function enviaCasoSF(body) {
    try {
        const access_token = (await auth.tokenCuidado()).access_token;

        const result = await request({
            url: process.env.API_CUIDADO_COORDENADO_MANIFESTACAO + '/manifestacoes',
            headers: {
                access_token,
                client_id: process.env.SENSEDIA_CLIENTID_CUIDADO,
            },
            method: 'PATCH',
            body,
            json: true,
        });

        logger.debug('Result Atualização SF: ', { result });
    } catch (err) {
        logger.error('Erro ao tentar atualizar caso no SalesForce', { body, err });
    }
}

module.exports = {
    gerarManifestacao,
    atualizaCaso,
    atualizaNovoCaso,
};