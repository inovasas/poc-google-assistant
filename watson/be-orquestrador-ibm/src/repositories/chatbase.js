//@ts-check
'use strict';

const mongoose = require('mongoose');
const redisGet = require('../helpers/utils').redisGetPromise;
const logger = require('../helpers/logger');
const redis = require('../helpers/connectRedis').client;
const { safeRequest } = require('../helpers/safeRequest');
const { concat, EMPTY } = require('rxjs');
const { retry, catchError, delay } = require('rxjs/operators');
const _ = require('lodash');

const CHATBASE_ENDPOINT = 'https://chatbase-area120.appspot.com/api/messages';

/**
 * O valor da APIKEY do chatbase
 * 
 * @param {string} conversationId
 * @returns {Promise<string>}
 */
async function readChatbaseKey(conversationId) {
    if (!conversationId) {
        return '';
    }

    const redisKey = 'chatbase' + conversationId;
    let chatbaseKey = await redisGet(redisKey);
    if (!chatbaseKey) {
        const historico = mongoose.connection.db.collection('historico');
        const msg = await historico.aggregate([
            { $match: { conversation_id: conversationId } },
            { $sort: { createdAt: -1 } },
            { $limit: 1 },
            {
                $lookup: {
                    from: 'workspace',
                    localField: 'workspace_id',
                    foreignField: 'workspace_id',
                    as: 'dados_workspace',
                },
            },
        ]).toArray();
        chatbaseKey = msg[0].dados_workspace[0].chatbase_key;
        redis.set(redisKey, chatbaseKey, 'EX', 60 * 30);
    }
    return chatbaseKey;
}

/**
 * 
 * @param {any} wcsReq Request Watson
 * @param {any} wcsRes Request response
 * @param {any} wcsCredentials credenciais
 * @returns {Promise<any>} Resultado envio
 */
function logToChatbase(wcsReq, wcsRes, wcsCredentials) {
    if (!wcsCredentials.chatbase_key) {
        return Promise.resolve();
    }
    const ctx = wcsRes.context;
    const intent = wcsRes.intents[0] || {};
    const baseMsg = {
        api_key: wcsCredentials.chatbase_key,
        user_id: ctx.documento || ctx.username || ctx.nome || ctx.conversation_id,
        platform: ctx.canal || 'web',
        time_stamp: Date.now(),
        intent: intent.intent,
    };
    const uid = baseMsg.user_id.replace(/[^0-z]/g, '');
    // chatbase só aceita caracteres alfanúmericos
    if (uid !== baseMsg.user_id) {
        logger.debug('User ID chatbase inválido!!!', { user_id: baseMsg.user_id, ctx });
        baseMsg.user_id = uid;
    }
    /** @type {import('rxjs').Observable<any>} */
    const userLogged = wcsReq.input ? safeRequest({
        url: CHATBASE_ENDPOINT,
        timeout: 10000, // Fail silently and fast.
        method: 'POST',
        body: {
            messages: [
                {
                    ...baseMsg,
                    type: 'user',
                    not_handled: (intent.confidence == null ||
                        intent.confidence < 0.6),
                    message: wcsReq.input,
                },
            ],
        },
        json: true,
    }).pipe(delay(1500)) : EMPTY;

    const messages = _.filter(wcsRes.output.text);
    const botLogged = messages.length > 0 ? safeRequest({
        url: CHATBASE_ENDPOINT,
        timeout: 10000, // Fail silently and fast.
        method: 'POST',
        body: {
            messages: messages.map(msg => ({
                ...baseMsg,
                type: 'agent',
                message: msg,
            })),
        },
        json: true,
    }).pipe(delay(1500)) : EMPTY;

    return concat(userLogged, botLogged).pipe(
        retry(2),
        catchError(() => null), // Fail silently and fast.
    ).toPromise();
}

module.exports = {
    readChatbaseKey, logToChatbase,
};
