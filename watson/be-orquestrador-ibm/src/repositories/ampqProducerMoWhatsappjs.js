//@ts-check
'use strict';
const connection = require('./amqp');

const ROUTING_KEY = 'mo-whatsapp';
const EXCHANGE_NAME = 'whatsapp';

// Create a channel wrapper
const channelWrapper = connection.createChannel({
    json: true,
    name: 'DEFAULT_PRODUCER',
    async setup(channel) {
        await channel.assertExchange(EXCHANGE_NAME, 'direct', { durable: true });
    },
});

function publish(data) {
    try {
        channelWrapper.publish(EXCHANGE_NAME, ROUTING_KEY, data);
    } catch (e) {
        console.log('Message was rejected:', e);
    }
}

module.exports.publish = publish;
