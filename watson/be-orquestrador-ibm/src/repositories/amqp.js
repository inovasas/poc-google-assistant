'use strict';

const amqp = require('amqp-connection-manager');
const logger = require('../helpers/logger');
const _ = require('lodash');

const vcapServices = JSON.parse(process.env.VCAP_SERVICES)['messages-for-rabbitmq'];
const credencialAmpq = vcapServices[0].credentials.connection.amqps;
const credencialBase64 = _.get(credencialAmpq, 'certificate.certificate_base64');
const connectionString = credencialAmpq.composed[0];

const op = {
    connectionOptions: {
        ca: _.filter([credencialBase64 && Buffer.from(credencialBase64, 'base64')]),
    },
    heartbeatIntervalInSeconds: 45,
};

// Create a connetion manager
const connection = amqp.connect(connectionString, op);
connection.on('connect', () => logger.debug('RabbitMQ connectado'));
connection.on('disconnect', err => logger.error('Disconnected do rabbit.', err));

module.exports = connection;
