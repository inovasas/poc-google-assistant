//@ts-check
'use strict';

const worker = require('../controllers/v1/chat/whatsapp/mensagensThread');
const connection = require('./amqp');

const QUEUE_NAME = 'mo-whatsapp';
const EXCHANGE_NAME = 'whatsapp';
const ROUTING_KEY = QUEUE_NAME;


// Handle an incomming message.
const onMessage = originalData => {
    const { content } = originalData;
    const data = JSON.parse(content.toString());
    try {
        worker(data);
        channelWrapper.ack(originalData);
    } catch (err) {
        channelWrapper.nack(originalData, false, false);
    }
};

// Set up a channel listening for messages in the queue.
const channelWrapper = connection.createChannel({
    // `channel` here is a regular amqplib `ConfirmChannel`.
    setup: channel => Promise.all([
        channel.assertQueue(QUEUE_NAME),
        channel.assertExchange(EXCHANGE_NAME, 'direct'),
        channel.prefetch(1),
        channel.bindQueue(QUEUE_NAME, EXCHANGE_NAME, ROUTING_KEY),
        channel.consume(QUEUE_NAME, onMessage),
    ]),
});

module.exports.channelWrapper = channelWrapper;
