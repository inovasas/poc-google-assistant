'use strict';

const mongoose = require('mongoose');
const logger = require('../helpers/logger');

module.exports.listHistorico = async (req, res) => {
    try {
        const date = req.body.objSearch.createdAt;
        if (date) {
            req.body.objSearch.createdAt.$gte = new Date(date.$gte);
            req.body.objSearch.createdAt.$lte = new Date(date.$lte);
        }

        const messages = await mongoose.connection.db.collection('historico').aggregate(
            [{
                $match: req.body.objSearch,
            },
            {
                $group: {
                    _id: {
                        workspace_id: '$workspace_id',
                        conversation_id: '$conversation_id',
                        username: '$username',
                        createdAt: {
                            $dateToString: {
                                format: '%Y-%m-%dT%H:%M:%S.000Z',
                                date: '$createdAt',
                            },
                        },
                    },
                },
            },
            {
                $sort: {
                    '_id.conversation_id': 1,
                    '_id.createdAt': 1,
                },
            },
            {
                $group: {
                    _id: {
                        workspace_id: '$_id.workspace_id',
                        conversation_id: '$_id.conversation_id',
                        username: '$_id.username',
                    },
                    createdAt: {
                        $first: '$_id.createdAt',
                    },
                },
            }]).toArray();

        res.status(200).json(messages);

    } catch (err) {
        res.status(500).json({
            status: 'error',
            message: 'Erro ao buscar lista de histórico',
        });

        logger.error('Erro ao buscar lista de histórico', { err });
    }

};

module.exports.findHistorico = async (req, res) => {
    try {
        const messages = await mongoose.connection.db.collection('historico').aggregate(
            [{
                $match: req.body.objSearch,
            }, {
                $group: {
                    _id: {
                        _id: '$_id',
                        username: '$username',
                        text: '$text',
                        intencoes: '$intents',
                        entidades: '$entities',
                        typeAgent: '$typeAgent',
                        createdAt: {
                            $dateToString: {
                                format: '%Y-%m-%dT%H:%M:%S.%L',
                                date: '$createdAt',
                            },
                        },
                    },
                },
            }, {
                $sort: {
                    '_id._id': 1,
                },
            }]).toArray();

        res.status(200).json(messages);

    } catch (err) {
        res.status(500).json({
            status: 'error',
            message: 'Erro ao buscar mensagens de histórico',
        });

        logger.error('Erro ao buscar mensagens de histórico', { err });
    }

};

module.exports.listFeedback = async (req, res) => {
    try {
        const date = req.body.obj.createdAt;
        if (date) {
            req.body.obj.createdAt.$gte = new Date(date.$gte);
            req.body.obj.createdAt.$lte = new Date(date.$lte);
        }

        const messages = await mongoose.connection.db.collection('feedback').aggregate(
            [{
                $match: req.body.obj,
            },
            { $sort: { _id: 1 } },
            {
                $group: {
                    _id: '$conversation_id',
                    id: { $last: '$_id' },
                    conversation_id: { $last: '$conversation_id' },
                    success: { $last: '$success' },
                    createdAt: { $last: '$createdAt' },
                    intents: { $last: '$intents' },
                    oid: { $last: '$oid' },
                    reason: { $last: '$reason' },
                    reviewed: { $last: '$reviewed' },
                    updatedAt: { $last: '$updatedAt' },
                    username: { $last: '$username' },
                    userReviewed: { $last: '$userReviewed' },
                    workspace_id: { $last: '$workspace_id' },
                }
            }]).toArray();

        res.status(200).json(messages);

    } catch (err) {
        res.status(500).json({
            status: 'error',
            message: 'Erro ao buscar lista de feedback',
        });

        logger.error('Erro ao buscar lista de feedback', { err });
    }

};

module.exports.getReasons = async (req, res) => {
    try {
        const messages = await mongoose.connection.db.collection('feedback').aggregate(
            [{
                $match: {
                    oid: req.body.oid,
                    $and: [
                        { reason: { $exists: true } },
                        { reason: { $ne: '' } },
                        { reason: { $ne: null } },
                    ],
                },
            },
            { $project: { reason: '$reason' } },
            { $group: { _id: { reason: '$reason' } } },
            { $sort: { count: 1 } }]).toArray();

        res.status(200).json(messages);

    } catch (err) {
        res.status(500).json({
            status: 'error',
            message: 'Erro ao buscar justificativas de feedback',
        });

        logger.error('Erro ao buscar justificativas de feedback', { err });
    }

};