'use strict';
const { safeRequest } = require('../helpers/safeRequest');
const { client: redis, get } = require('../helpers/connectRedis');
const _ = require('lodash');
const { from } = require('rxjs');
const { concatMap, retryWhen, map } = require('rxjs/operators');
const logger = require('../helpers/logger');

const { SF_REFRESH_TOKEN, SF_BASE_URL, SF_CLIENT_ID, SF_CLIENT_SECRET } = process.env;
const SF_REDIS_AUTH_KEY = 'tokens:salesforce_health';

async function getAuthorization() {
    const cached = await get(SF_REDIS_AUTH_KEY);
    if (!_.isEmpty(cached)) {
        return cached;
    }
    logger.debug('SF Authentication token cache miss.');
    const authResp = await safeRequest({
        url: SF_BASE_URL + '/services/oauth2/token',
        qs: {
            grant_type: 'refresh_token',
            client_id: SF_CLIENT_ID,
            client_secret: SF_CLIENT_SECRET,
            refresh_token: SF_REFRESH_TOKEN,
        },
        method: 'POST',
        json: true,
    }).toPromise();
    const { access_token } = authResp;
    // Não tem await de propósito. Outro request vai passar pelo if acima de qualquer
    // forma.
    redis.set(SF_REDIS_AUTH_KEY, access_token, 'EX', 60 * 10);
    return access_token;
}

function authorizedRequest(opts) {
    return from(getAuthorization()).pipe(
        concatMap(token => safeRequest({
            headers: Object.assign(opts.headers || {}, {
                Authorization: `Bearer ${token}`,
            }),
            ...opts,
        }, 1)),
        retryWhen((mirror) => mirror.pipe(map((err, i) => {
            const { statusCode } = err;
            if ((statusCode >= 400 && statusCode <= 504) && i < 10) {
                logger.debug('Retry de authorization token', { err, opts });
                redis.del(SF_REDIS_AUTH_KEY);
                return err;
            } else {
                throw err;
            }
        }))),
    );
}

async function getServicosMedicos(context) {
    const body = {
        request: {
            especialidade: context.Especialidade || 'Pediatria',
            estado: context.UF || 'SP',
            cidade: context.Cidade || 'SAO PAULO',
        },
    };

    logger.debug('Buscando serviços médicos...', { body });

    let result;
    try {
        result = await authorizedRequest({
            url: process.env.SERVICOS_MEDICOS_URI,
            headers: {
                'Content-Type': 'application/json',
            },
            method: 'POST',
            body: body,
            json: true,
        }).toPromise();
    } catch (err) {
        logger.error('Erro ao buscar serviços médicos', { err });
        return null;
    }

    return result.data.objeto;
}

module.exports = {
    authorizedRequest,
    getServicosMedicos,
};
