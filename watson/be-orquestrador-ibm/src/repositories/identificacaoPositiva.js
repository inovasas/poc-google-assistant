'use strict';
const Fuse = require('fuse.js')
const saude = require('./saudeApis');
const ctx = require('../helpers/validateContext');
const { client: redis, get } = require('../helpers/connectRedis');
const logger = require('../helpers/logger');
const {
    CHAVE24HORAS, CAMPOS_CONTEXTO, SECS24HORAS,
} = require('../controllers/v1/chat/whatsapp/filas');
const _ = require('lodash');

const CHAVES_RESP = ['resposta1', 'resposta2', 'resposta3'];


ctx.posWatsonReducers.autenticacao_mdm = async function (watsonData, wcsResp) {
    const ctx = wcsResp.context;
    const { produto_atual: produto } = wcsResp.context;

    redis.del(CHAVE24HORAS + ctx.phoneNumber);

    for (const chave of CHAVES_RESP) {
        // Limpa tentativas anteriores
        delete wcsResp.context[chave];
    }
    if (produto.produto === 'SAUDE') {
        // FIXME: Saude ainda permite brute force horizontal!!!
        try {
            const perguntas = await saude.getPerguntas(
                produto.carteirinha['codigo-beneficiario']);

            perguntas.forEach(p => {
                p.opcoesHTML = p.opcoes.map((opc, i) => ` *${i + 1}*) ${opc}`).join('\n');
                p.pergunta = '\n' + p.pergunta + '\n';
            });

            ctx.perguntas_identificacao = perguntas;
        } catch (err) {
            const errResp = err.response;
            if (errResp.statusCode === 422) {
                ctx.erro_login = errResp.body.details;
            } else if (errResp.statusCode === 401) {
                logger.error('Tempo de exipiração errado sensedia.', {
                    error: err,
                    errResp,
                });
                ctx.erro_login = 'Desculpe, ocorreu um erro em nosso sistema. ' +
                    'Por favor, tente novamente mais tarde';
                // Referencia do arquivo sensediaAuth
                redis.del('tokenBeneficiario');
            } else {
                throw err;
            }
        }
        return;
    } else if (produto.produto === 'AUTO') {
        const chaveRedis = 'tentativasAuth_' + ctx.documento;
        let tentativas = Number.parseInt(await get(chaveRedis), 10) || 0;
        if (tentativas >= 3) {
            ctx.erro_login = 'Tentativas de autenticação de auto excedidas.';
            return;
        }

        ctx.perguntas_identificacao = [{
            pergunta: 'Qual o número de sua apólice?',
            opcoesHTML: '',
        }, {
            pergunta: 'Por favor, me informe a placa de um dos ' +
                'veículos presentes nessa apólice',
            opcoesHTML: '',
        }];
        redis.set(chaveRedis, (++tentativas).toString(10), 'EX', SECS24HORAS);
        return;
    }
};

ctx.posWatsonReducers.validar_autenticacao = async (wcsReq, wcsRes) => {
    const ctx = wcsRes.context;

    if (ctx.produto_atual.produto === 'SAUDE') {
        const { perguntas_identificacao: perguntas } = wcsRes.context;
        const respostas = CHAVES_RESP.map((idResp, i) => {
            const numAlternativa = Number.parseInt(wcsRes.context[idResp], 10) - 1;
            return {
                codigo: perguntas[i].codigo,
                resposta: perguntas[i].opcoes[numAlternativa],
            };
        });
        const numCarteirinha = wcsRes.context.produto_atual.carteirinha['codigo-beneficiario'];
        try {
            const respostasOK = await saude.checkRespostas(numCarteirinha, respostas);
            wcsRes.context.autenticado = respostasOK;
        } catch (error) {
            if (error.statusCode === 401) {
                logger.error('Timeout errado da API sensedia?',
                    { bodyResp: error.body, error });
            }
            wcsRes.context.autenticado = false;
        }
    } else if (ctx.produto_atual.produto === 'AUTO') {
        const prd = ctx.produto_atual;
        ctx.resposta2 = ctx.resposta2.toUpperCase().replace(/[ -]/g, '');
        const placa = prd.apolice.veiculo.placa.toUpperCase().replace(/[ -]/g, '');
        const respostaOK = prd.apolice.apolice.numeroApolice === ctx.resposta1 &&
            placa === ctx.resposta2;
        wcsRes.context.autenticado = respostaOK;

        if (respostaOK) {
            redis.del('tentativasAuth_' + ctx.documento);
        }
    }

    if (wcsRes.context.autenticado) {
        const userSession = _.pick(wcsRes.context, CAMPOS_CONTEXTO);
        userSession.autenticado = true;

        const ctx24hrs = JSON.stringify(userSession);
        redis.set(CHAVE24HORAS + ctx.phoneNumber, ctx24hrs);
    }


};
