'use strict';


const { map } = require('rxjs/operators');
const auth = require('./sensediaAuth');
const protocolo = require('./protocoloSaude');
const utils = require('../helpers/utils');
const { safeRequest } = require('../helpers/safeRequest');
const ctx = require('../helpers/validateContext');
const moment = require('moment');
const logger = require('../helpers/logger');
const API_SAUDE_BENEFICIARIO_PREVIA = process.env.API_SAUDE_BENEFICIARIO_PREVIA;
const URL_PGTO = process.env.URL_PAGAMENTOS ||
    'https://apisulamerica.sensedia.com/pagamentos/api/v3';
const API_SAUDE_BENEFICIARIO = process.env.API_SAUDE_BENEFICIARIO ||
    'https://apisulamerica.sensedia.com/homolog/saude/beneficiario/api/v2';

const API_REEMBOLSO = process.env.API_REEMBOLSO ||
    'https://apisulamerica.sensedia.com/reembolsos/api/v1';

ctx.posWatsonReducers.gerar_protocolo_saude = async (wcsData, wcsResp) => {
    try {
        const { carteirinha } = wcsResp.context.produto_atual;
        const numProtocolo = await protocolo.gerarProtocolo({
            ansParam: carteirinha['codigo-registro-ans'],
            carteirinha: carteirinha['codigo-beneficiario'],
        });
        wcsResp.output.generic.forEach(x => {
            if (x.response_type !== 'text') {
                return;
            }
            x.text = x.text.replace('%%num_protocolo%%', numProtocolo);
        });
        wcsResp.context.num_protocolo_saude = numProtocolo;
    } catch (err) {
        wcsResp.output.generic.forEach(x => {
            if (x.response_type !== 'text') {
                return;
            }
            x.text = x.text.replace('%%num_protocolo%%', '(Erro ao gerar protocolo)');
        });
        wcsResp.context.num_protocolo_saude = null;
    }
};

/**
 * Função especializada para construir um objeto representando uma carteirinha de saúde
 * a partir de um objeto generico MDM.
 *
 * @param {any} produtoMdm Payload MDM do produto de saúde
 */
async function buildProdutoSaude(produtoMdm) {
    const beneficiario = await apiVPP(produtoMdm.valChaveOrigem);
    const nome = utils.capitalize(beneficiario.nome).split(' ');
    let desc = 'Saúde';
    let tipoProduto = 'Individual';
    let empresaAdesao = '';

    const individual = await isIndividual(beneficiario);
    const adesao = await isAdesao(beneficiario);

    if (individual) {
        desc += ' individual.';
    } else if (adesao) {
        tipoProduto = 'Adesão';
        empresaAdesao = adesao;
        desc += ` - _${utils.capitalize(beneficiario.empresa['nome-empresa'])}_`;
    } else {
        tipoProduto = 'Empresarial';
        desc += ` - _${utils.capitalize(beneficiario.empresa['nome-empresa'])}_`;
    }

    const returnObj = {
        cpf: produtoMdm.cpfId,
        status: produtoMdm.idcStatus === 'A' ? 'Ativo' : 'Inativo',
        produto: 'SAUDE',
        nome: nome.join(' '),
        descricao: desc,
        carteirinha: beneficiario,
        tipoProduto: tipoProduto,
        empresaAdesao: empresaAdesao,
        planoBU: 'Saúde ' + tipoProduto,
        codigoPlano: produtoMdm.codigoPlano,
    };

    logger.debug('infos usuario saude: ', { returnObj });


    return returnObj;
}

/**
 * 
 * @param {Object} beneficiario
 */
async function isIndividual(beneficiario) {
    const codigosIndividuais = ['132', '133', '152', '153', '182', '183', '192', '193',
        '322', '325', '342', '345', '622', '625', '642', '645', '839', '840', '842', '940',
        '950', '951', '952', '953', '960', '961', '968', '969', '970', '971', '972', '978',
        '982', '983', '984', '985', '101', '102', '103', '301', '302', '303', '312', '313',
        '314', '318', '401', '612', '613', '830', '831', '833', '834', '836'];

    return beneficiario.produto['descricao-produto'].startsWith('IND') ||
        beneficiario.produto['codigo-produto'].includes(codigosIndividuais);
}

/**
 *
 * @param {Object} beneficiario
 */
async function isAdesao(beneficiario) {
    const nomeEmpresa = beneficiario.empresa['nome-empresa'];
    const adesaoQualicorp = ['CAASP', 'APM', 'APCD', 'UBES', 'UNE', 'SINDICOM'];
    const adesaoExtramed = ['EXTRA SAUDE', 'EXTRA CLUB'];

    if (nomeEmpresa.includes(adesaoQualicorp) ||
        nomeEmpresa.startsWith('QUALICORP') ||
        nomeEmpresa.startsWith('ACCESS CLUB') ||
        nomeEmpresa.startsWith('CAIXA ASSISTENCIA ADVOGADOS')) {
        return 'Qualicorp';
    } else if (nomeEmpresa.includes(adesaoExtramed) ||
        nomeEmpresa.startsWith('Extramed')) {
        return 'Extramed';
    } else if (nomeEmpresa.startsWith('EV ADM') ||
        nomeEmpresa.startsWith('EV Cartaz')) {
        return 'EV';
    } else if (nomeEmpresa.startsWith('COMANDO DA AERONAUTICA')) {
        return 'Comando da Aeronáutica';
    }

    return beneficiario.produto['descricao-produto'].includes('ADESAO');
}

/**
 * 
 * @param {string} codigoBeneficiario numero de indentificacao do segurado
 * 
 * @returns {Promise<any>} segurado
 */
async function apiVPP(codigoBeneficiario) {
    const token = await auth.tokenPagamentos();
    return safeRequest({
        url: 'https://apisulamerica.sensedia.com/vpp/saude/v1/beneficiario' +
            '/consultar-por-codigo/codigo-beneficiario/' + codigoBeneficiario,
        method: 'GET',
        json: true,
        headers: {
            access_token: token.access_token,
            client_id: auth.AUTH_HEADERS.client_id,
        },
    }).pipe(
        map(x => x.response.beneficiario),
    ).toPromise();
}

async function getBoletoIndividual(codigoBeneficiario) {
    try {
        const access_token = (await auth.tokenPagamentos()).access_token;
        const result = await getParcelas(codigoBeneficiario, access_token);

        const proximaParcela = result.filter(parcela => parcela.situacao !== 'PAGO')
            .sort((a, b) => a.numeroParcela - b.numeroParcela).shift();

        const boleto = await getBoletoIndividualIPTE(codigoBeneficiario,
            proximaParcela, access_token);
        proximaParcela.ipte = boleto.ipte;
        proximaParcela.dataVencimento = boleto.dataVencimento ?
            boleto.dataVencimento : proximaParcela.dataVencimento;

        return proximaParcela;
    } catch (err) {
        logger.error('Não foi possível gerar boleto de saúde.', { error: err });

        return false;
    }
}

function getParcelas(codigoBeneficiario, token) {
    return safeRequest({
        url: URL_PGTO + '/saude/parcelas',
        qs: {
            origem: 'CH',
            codigoBeneficiario: codigoBeneficiario,
        },
        headers: {
            access_token: token,
            client_id: auth.AUTH_HEADERS.client_id,
            'Content-Type': 'application/json',
        },
        json: true,
    }).toPromise();
}

async function getBoletoIndividualIPTE(codigoBeneficiario, parcela, token) {
    const fmtData = 'DDMMYYYY';
    const qs = {
        tipoRetorno: '3',
        origem: 'CH',
        numeroParcela: parcela.numeroParcela,
        tipoRequisicao: 'IND',
        codigoBeneficiario: codigoBeneficiario,
    };
    // Formata a data de vencto da API
    const vencto = moment(parcela.dataVencimento, fmtData);
    // Mais velho que 60 dias não pode
    if (vencto.isBefore(moment().subtract(60, 'days'))) {
        throw new Error('Boleto muito antigo!');
    }
    // Se vencido...
    if (!vencto.isValid() || vencto.isBefore()) {
        // Pede pra API vencimento amanhã
        qs.dataVencimento = moment().add(1, 'day').format(fmtData);
        qs.boletoVencido = true;
    }
    return safeRequest({
        uri: URL_PGTO + '/saude/boletos/segunda-via/individual',
        qs,
        headers: {
            access_token: token,
            client_id: auth.AUTH_HEADERS.client_id,
            'Content-Type': 'application/json',
        },
        json: true,
    }).pipe(map(x => {
        x.dataVencimento = qs.dataVencimento;
        return x;
    })).toPromise();
}


/**
 * Lista peguntas de identificação positiva
 *
 * @param {string} codigoBeneficiario
 * 
 * @returns {Promise<any[]>} Promessa com perguntas
 */
async function getPerguntas(codigoBeneficiario) {
    const access_token = (await auth.tokenBeneficiario()).access_token;
    return safeRequest({
        url: API_SAUDE_BENEFICIARIO + '/beneficiarios/owner/perguntas',
        headers: {
            access_token,
            client_id: auth.AUTH_HEADERS.client_id,
            ci: codigoBeneficiario,
            'Content-Type': 'application/json',
            'Accept': 'application/json',
        },
        json: true,
    }).toPromise();
}


async function checkRespostas(codigoBeneficiario, bodyRespostas) {
    const access_token = (await auth.tokenBeneficiario()).access_token;
    return safeRequest({
        method: 'PATCH',
        url: API_SAUDE_BENEFICIARIO + '/beneficiarios/owner/perguntas/respostas',
        headers: {
            access_token,
            client_id: auth.AUTH_HEADERS.client_id,
            ci: codigoBeneficiario,
            'Content-Type': 'application/json',
            'Accept': 'application/json',
        },
        body: bodyRespostas,
        json: true,
    }).pipe(
        map(x => x.valido === 'true'),
    ).toPromise();
}

async function checkStatusReembolso(dataInicial, dataFinal, codBeneficiario) {
    const access_token = (await auth.tokenReembolso()).access_token;

    return safeRequest({
        method: 'GET',
        url: API_REEMBOLSO + '/acompanhamentos/sr',
        headers: {
            access_token,
            client_id: auth.AUTH_HEADERS.client_id,
            'Content-Type': 'application/json',
        },
        qs: {
            dataInicial,
            dataFinal,
            codBeneficiario,
        },
        json: true,
    }).toPromise();
}
async function getCodigoServico(codigoTuss, codigoTipoPrevia) {
    const access_token = (await auth.tokenPrevia()).access_token;
    return safeRequest({
        method: 'GET',
        url: API_SAUDE_BENEFICIARIO_PREVIA + '/reembolsos/previas',
        headers: {
            access_token,
            client_id: auth.AUTH_HEADERS_BENEFICIARIO_PREVIA.client_id,
            'Content-Type': 'application/json',
        },
        qs: {
            codigoTipoPrevia,
            busca: codigoTuss,
        },
        json: true,
    }).toPromise();
}

async function getPreviaParametros(codigoServico) {
    const access_token = (await auth.tokenPrevia()).access_token;

    return safeRequest({
        method: 'GET',
        url: API_SAUDE_BENEFICIARIO_PREVIA + '/reembolsos/previas/' + codigoServico + '/parametros',
        headers: {
            access_token,
            client_id: auth.AUTH_HEADERS_BENEFICIARIO_PREVIA.client_id,
            'Content-Type': 'application/json',
        },
        json: true,
    }).toPromise();
}

async function getPreviaReembolso(codigoBeneficiario, body) {
    const access_token = (await auth.tokenPrevia()).access_token;

    return safeRequest({
        method: 'POST',
        url: API_SAUDE_BENEFICIARIO_PREVIA + '/beneficiarios/' + codigoBeneficiario + '/reembolsos/previas',
        headers: {
            access_token,
            client_id: auth.AUTH_HEADERS_BENEFICIARIO_PREVIA.client_id,
            codigoorigemtransacao: 1,
            'Content-type': 'application/json',
        },
        body: body,
        json: true,
    }).toPromise();

}

module.exports.checkStatusReembolso = checkStatusReembolso;
module.exports.getBoletoIndividual = getBoletoIndividual;
module.exports.buildProdutoSaude = buildProdutoSaude;
module.exports.getPerguntas = getPerguntas;
module.exports.checkRespostas = checkRespostas;
module.exports.getPreviaReembolso = getPreviaReembolso;
module.exports.getPreviaParametros = getPreviaParametros;
module.exports.getCodigoServico = getCodigoServico;