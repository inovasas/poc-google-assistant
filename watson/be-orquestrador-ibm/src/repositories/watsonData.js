//@ts-check
'use strict';

const promiseRetry = require('promise-retry');
const redis = require('../helpers/connectRedis').client;
const chatRepository = require('../repositories/chat');
const promiseRetryOptions = {
    retries: 3,
};

/**
 *
 * @param {Array<any>} tokenList
 * @param {string} conversation_id
 * @returns {Promise<any>}
 */
module.exports.filterTokenList = async (tokenList, conversation_id) => {
    if (tokenList.length === 1) {
        return tokenList[0];
    }
    if (!conversation_id) {
        /* na primeira conversa, envia mensagem para workspace aleatório
        (caso exista mais de um com o mesmo token cadastrado) */
        return tokenList[Math.floor(Math.random() * tokenList.length)];
    }
    try {
        const redisWksId = await promiseRetry((retry) => {
            // busca conversation_id no redis
            return chatRepository.findConversationIdInRedis(conversation_id)
                .catch(retry);
        }, promiseRetryOptions);

        const wcsCredentials = tokenList.find(x => x.workspace_id === redisWksId);

        if (wcsCredentials) {
            return wcsCredentials;
        }
    } catch (err) {
        return {
            error: err,
            msg: 'Redis connection failed.',
        };
    }

    try {
        const conversationId = await promiseRetry(
            (retry) => chatRepository.findConversationIdInDb(conversation_id).catch(retry),
            // busca conversation_id no banco, caso não exista no redis
            promiseRetryOptions);
        if (conversationId.length === 0) {
            throw { msg: 'conversation_id não encontrado.' };
        }
        return tokenList.find(x => x.workspace_id === conversationId[0].workspace_id);
    } catch (err) {
        throw {
            error: err,
            msg: 'Mongo connection failed.',
        };
    }
};

/**
 *
 * @param {string} token
 * @returns {Promise<any[]>}
 */
module.exports.loadCredentials = async (token) => {

    let resultTokenList;

    // busca token no redis
    try {
        resultTokenList = JSON.parse(await promiseRetry((retry) => {
            return chatRepository.findTokenInRedis(token)
                .catch(retry);
        }, promiseRetryOptions));
    } catch (err) {
        console.error('Redis connection failed!');
    }

    // busca token no banco, caso não exista no redis
    if (!resultTokenList) {
        try {
            resultTokenList = await promiseRetry((retry) => {
                return chatRepository.findTokenInDb(token).catch(retry);
            }, promiseRetryOptions);
            redis.set(token, JSON.stringify(resultTokenList), 'EX', 300);
        } catch (err) {
            throw {
                error: err,
                msg: 'MongoDB connection failed.',
            };
        }
    }
    return resultTokenList;
};
