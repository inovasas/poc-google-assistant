//@ts-check
'use strict';

const fs = require('fs');


/**
 * 
 * @returns {String}
 */
module.exports.getMongoPasswd = () => {

    try {
        if (process.env.DB_MONGO_PASS) {
            return fs
                .readFileSync(process.env.DB_MONGO_PASS)
                .toString()
                .split(/\n/)[0]
                .trim()
                .toString();
        }
    } catch (e) {
        console.log(e);
    }

    let mongoPass = process.env.MONGODB_SECRET_PATH || '';
    const secretMongoPath = `${mongoPass.replace(/\/*$/, '')}/password`;
    if (process.env.MONGODB_SECRET_PATH && fs.existsSync(secretMongoPath)) {
        mongoPass = (fs.readFileSync(secretMongoPath).toString() || '').trim();
    }
    console.log(`${process.env.MONGODB} ===> password: ${mongoPass}`);
    return mongoPass;
};

/**
 * 
 * @returns {String}
 */
module.exports.getMongoUser = () => {

    try {
        if (process.env.DB_MONGO_USER) {
            return fs
                .readFileSync(process.env.DB_MONGO_USER)
                .toString()
                .split(/\n/)[0]
                .trim()
                .toString();
        }
    } catch (e) {
        console.log(e);
    }

    let mongoUser = process.env.MONGODB_SECRET_PATH || '';
    const secretMongoPath = `${mongoUser.replace(/\/*$/, '')}/username`;
    if (process.env.MONGODB_SECRET_PATH && fs.existsSync(secretMongoPath)) {
        mongoUser = (fs.readFileSync(secretMongoPath).toString() || '').trim();
    }
    console.log(`${process.env.MONGODB} ===> username: ${mongoUser}`);
    return mongoUser;
};