//@ts-check
'use strict';

const nlu = require('./NLUInstance');
const chatRepository = require('../repositories/chat');
const { mapObj } = require('./formMapper');
const { mapObjFollowize } = require('./formMapperFollowize');

const { sendForm, sendLeadFollowize } =
    require('../repositories/formCorretor');
const prestadores = require('../repositories/prestadores');
const { getServicosMedicos } = require('../repositories/salesforceHealth');
const { atualizaCaso } = require('../repositories/salesforce');
const logger = require('../helpers/logger');
const _ = require('lodash');
const { sendHumanChat } = require('../repositories/wavyWhatsapp');

/**
 *
 * @param {String} endereco
 */
async function getLatLong(endereco) {
    const resp = await prestadores.getLatLong(endereco);

    return resp;
}

/**
 *
 * @param {String} text
 */
function analyzeText(text) {
    return new Promise((resolve, reject) => {
        nlu.analyze(text, (err, resp) => {
            if (err) {
                reject(err);
            } else {
                resolve(resp);
            }
        });
    });
}

/**
 * Mapa com todas as transformações de contexto e output.
 *
 * @type {{[nome: string]: (watsonData: any) => any}}
 */
const preWatsonReducers = {
    async valida_nome(watsonData) {
        const nome = watsonData.input;
        if (nome.split(' ').length > 1) {
            try {
                const nluResponse = await analyzeText(nome);
                const resultNLU = nluResponse.entities[0];

                if (nluResponse.entities.length > 0 && resultNLU.type === 'Person') {
                    watsonData.context.nome = resultNLU.text;

                    delete watsonData.context.valida_nome;
                } else {
                    watsonData.context.nome_invalido = true;
                }
            } catch (err) {
                watsonData.context.nome_invalido = true;
                logger.error('NLU Error. Check NLU limit.');
            }
        } else {
            delete watsonData.context.valida_nome;
            watsonData.context.nome = nome;
        }
    },
    async valida_endereco(watsonData) {
        const context = watsonData.context;
        const endereco = watsonData.input;
        let result;
        try {
            const objEndereco = await getLatLong(endereco);
            result = objEndereco.results[0];
            context.endereco = result.formatted_address.replace(/[()]/g, ' ');
            context.long = result.geometry.location.lng;
            context.lat = result.geometry.location.lat;
        } catch (err) {
            throw new Error('Infelizmente, nosso serviço está indisponível no momento. ' +
                'Por favor, tente novamente mais tarde.');
        }

        const components = {};
        result.address_components.forEach(x =>
            x.types.forEach(t => components[t] = x.long_name));
        context.shortAddress = [
            components.route,
            components.sublocality_level_1,
        ].filter(x => x).join(', bairro ') || components.locality;
        context.shortAddress = context.shortAddress.replace(/[()]/g, ' ');

        delete context.valida_endereco;
    },
};

/**
 * Mapa com todas as possíveis transformações de contexto e output. executadas depois do 
 * retorno da resposta do watson
 *
 * @type {{[nome: string]: (watsonData: any, watsonResponse: any, val: any) => any}}
 */
const posWatsonReducers = {
    feedback(watsonData, watsonResponse) {
        chatRepository.saveFeedback(watsonResponse, watsonData.workspace_id, watsonData.oid);
    },
    async captura_sugestao(watsonData, watsonResponse) {
        const suggestions = await chatRepository.findSuggestionInDb(
            watsonResponse.context.captura_sugestao);
        const captureValue = watsonResponse.context.captura_sugestao;
        watsonResponse.context.sugestao = suggestions[0].elementos;
        watsonResponse.context[captureValue] = [];
        watsonResponse.context['texto_' + captureValue] = suggestions[0].elementos.map(x =>
            x.texto.toLowerCase());
        suggestions[0].elementos.forEach(e => {
            watsonResponse.context[captureValue].push({
                texto: e.texto.toLowerCase(),
                sempreMostrar: e.sempreMostrar,
            });
        });
        delete watsonResponse.context.captura_sugestao;
    },
    async envia_corretor(watsonData, watsonResponse) {
        logger.info('------ envia corretor ------', { watsonData });
        delete watsonResponse.context.envia_corretor;

        // sendForm(mapObj(watsonResponse.context),
        //     watsonResponse.context.conversation_id,
        // ).then(() =>
        //     logger.info('Lead de' + watsonResponse.context.email + 'enviado com sucesso'),
        // );

        try {
            const contextObj = await mapObj(watsonResponse.context);

            const result = await sendForm(contextObj, watsonResponse.context.conversation_id);

            logger.debug('result lead send form', { result });

            logger.info('Lead de ' + watsonResponse.context.email + ' enviado com sucesso');
        } catch (err) {
            logger.error('erro no envio de Lead para SF', { err });
        }

        sendLeadFollowize(mapObjFollowize(watsonResponse.context),
            watsonResponse.context.conversation_id,
        ).then(() =>
            logger.info('Followize - ', watsonResponse.context.email, 'enviado com sucesso'),
        );
    },
    async busca_servicos(watsonData, watsonResponse) {
        logger.debug('busca servicos...', { watsonResponse });
        const servicos = await getServicosMedicos(watsonResponse.context);

        watsonResponse.context.lista_servicos = servicos;
    },
    async transferir_atendimento_wavy(watsonData, watsonResponse) {
        try {
            logger.debug('send human chat');
            const result = await sendHumanChat(watsonResponse.context);
            watsonResponse.context.transferido_wavy = true;
            logger.info('Transferido para atendimento humano Wavy', { result });
        } catch (err) {
            watsonResponse.context.erro_transferencia_humano = true;
            logger.error('Erro ao tentar transferência para atendimento humano Wavy', { err });
        }
    },
    status_lista_cuidado(watsonData, watsonResponse) {
        try {
            atualizaCaso(watsonResponse.context.conversation_id,
                watsonResponse.context.status_lista_cuidado);
        } catch (err) {
            logger.error('Erro ao tentar atualizar caso no SalesForce' +
                `(${watsonResponse.context.status_lista_cuidado})`, { err });
        }
    },
    async testFlag(watsonData, watsonResponse) {
        logger.debug('teste flag...', { watsonResponse });
    },
};

posWatsonReducers.justificativa_feedback = posWatsonReducers.feedback;

/**
 *
 * @param {any} watsonData
 * @returns {Promise<any>}
 */
module.exports.preHandleContext = async (watsonData) => {
    const chaves = Object.keys(watsonData.output || {})
        .concat(Object.keys(watsonData.context || {}));
    for (const chave of chaves) {
        if (chave in preWatsonReducers) {
            await preWatsonReducers[chave](watsonData);
        }
    }
    return watsonData.context;
};

/**
 *
 * @param {any} watsonData
 * @param {any} watsonResponse
 * @returns {Promise<any>}
 * 
 */
module.exports.posHandleContext = async (watsonData, watsonResponse) => {
    const { context, output } = watsonResponse;
    const chaves = _.chain(context || {})
        .clone()
        .assign(output || {})
        .pickBy()
        .keys()
        .value();
    for (const chave of chaves) {
        if (chave in posWatsonReducers) {
            const valor = context[chave] || output[chave];
            await posWatsonReducers[chave](watsonData, watsonResponse, valor);
        }
    }
    return watsonResponse.context;
};

module.exports.posWatsonReducers = posWatsonReducers;
module.exports.preWatsonReducers = preWatsonReducers;
