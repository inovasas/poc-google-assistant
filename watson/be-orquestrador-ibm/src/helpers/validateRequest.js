//@ts-check
'use strict';

/**
 * 
 * @param {string[]} fields
 * @param {Object} req
 */
function validateFieldsInBody(fields, req) {
    /**
     * @type {string[]}
     */
    const errorMsg = [];
    fields.forEach((f) => {
        if (!req.body.hasOwnProperty(f)) {
            errorMsg.push(f);
        }
    });
    if (errorMsg.length > 0) {
        return `Missing field(s): ${errorMsg.join(', ')}`;
    }
    return '';
}

/**
 * 
 * @param {string[]} fields
 * @param {Object} req
 */
function validateFieldsInBodyValues(fields, req) {
    /**
     * @type {string[]}
     */
    const errorMsg = [];
    fields.forEach((f) => {
        if (req.body.values && !req.body.values.hasOwnProperty(f)) {
            errorMsg.push(f);
        }
    });
    if (errorMsg.length > 0) {
        return `Missing field(s): ${errorMsg.join(', ')}`;
    }
    return '';
}

/**
 * 
 * @param {string[]} fields
 * @param {Object} req
 */
function validateFieldsInQuery(fields, req) {
    /**
     * @type {string[]}
     */
    const errorMsg = [];
    fields.forEach((f) => {
        if (!req.query.hasOwnProperty(f)) {
            errorMsg.push(f);
        }
    });
    if (errorMsg.length > 0) {
        return `Missing fields: ${errorMsg.join(', ')}`;
    }
    return '';
}

/**
 * 
 * @param {string[]} fields
 * @param {Object} req
 */
function validateFieldsInParam(fields, req) {
    /**
     * @type {string[]}
     */
    const errorMsg = [];
    fields.forEach((f) => {
        if (!req.params.hasOwnProperty(f)) {
            errorMsg.push(f);
        }
    });
    if (errorMsg.length > 0) {
        return `Missing fields: ${errorMsg.join(', ')}`;
    }
    return '';
}


module.exports.validateFieldsInBody = validateFieldsInBody;
module.exports.validateFieldsInBodyValues = validateFieldsInBodyValues;
module.exports.validateFieldsInQuery = validateFieldsInQuery;
module.exports.validateFieldsInParam = validateFieldsInParam;