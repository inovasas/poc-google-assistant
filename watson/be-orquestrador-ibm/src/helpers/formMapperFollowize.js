'use strict';

const moment = require('moment');
const { capitalize } = require('./utils');

/** @type {{[val: string]: (key: any) => any}} */
const mapper = {
    'nome': async (nome) => ({
        'name': nome,
    }),
    'email': async (email) => ({
        'email': email,
    }),
    'mobile': async (mobile) => ({
        'phone': mobile,
    }),
    'localidade': async (localidade) => ({
        'zipCode': localidade.cep,
        'state': localidade.uf,
    }),
    'para_quem': async (paraQuem) => ({
        'locationTitle': paraQuem === 'para_mim' ? 'Para Mim' : (
            paraQuem === 'empresa' ? 'Para Empresa' : ''),
    }),
    'cpf': async (cpf) => ({
        'registrationNumber': cpf,
    }),
    'cnpj': async (cnpj) => ({
        'companyRegistrationNumber': cnpj,
    }),
    'assunto': async (assunto) => ({
        'productTitle': capitalize(assunto).replace(/Saude/, 'Saúde'),
    }),
};

const watsonBlackList = ['conversation_id', 'system', 'timezone', 'nome_invalido',
    'texto_frota', 'texto_profissoes', 'quantidade_pessoas', 'texto_atividades',
    'texto_atividades', 'marca', 'texto_marca', 'seguradora', 'texto_seguradora'];

/**
 *
 * @param {Object} ctx
 * @return {Promise<any>} obj 
 */
async function mapObjFollowize(ctx) {
    const resObj = {};
    for (const key of Object.keys(ctx)) {
        if (watsonBlackList.includes(key)) {
            continue;
        }
        const funcMapeador = mapper[key];
        let newValue = {};

        if (funcMapeador) {
            newValue = await funcMapeador(ctx[key]);
        } else {
            newValue[key] = ctx[key];
        }

        Object.assign(resObj, newValue);
    }

    resObj.clientKey = 'b4715c36eb8901dbfffb8b262af1f4bd';
    resObj.brandTitle = 'SulAmérica';
    resObj.conversionGoal = 'Chat Tá com Tudo';
    resObj.environment = process.env.FOLLOWIZE_ENV;

    if (resObj.locationTitle === 'Para Empresa' && resObj.productTitle === 'Auto') {
        resObj.message = `${ctx.texto_atividades} - ` +
            `${ctx.texto_frota} x ${ctx.quantidade_veiculos}`;
    }

    if (resObj.locationTitle === 'Para Mim' && resObj.productTitle === 'Auto') {
        const nascimento = ctx.nascimento.length !== 8 ? ctx.nascimento :
            moment(ctx.nascimento, 'DDMMYYYY').format('DD/MM/YYYY');

        const ano_veiculo = ctx.ano_veiculo || 0;
        const texto_seguradoras = ctx.texto_seguradoras || 'N/A';
        const tempo_segurado = ctx.tempo_segurado || 0;
        const acionou_seguradora = ctx.acionou_seguradora || 'N/A';
        const vezes_acionado = ctx.vezes_acionado || 0;

        resObj.message = `Cliente: Nascimento em ${nascimento}, sexo ${ctx.sexo} \n` +
            `Carro: Zero KM: ${ctx.zero_km}, ano ${ano_veiculo}, ` +
            `marca ${ctx.texto_marcas},modelo ${ctx.modelo_veiculo} \n` +
            `Possui seguro: ${ctx.possui_seguro} - ` +
            `Seguradora atual: ${texto_seguradoras} - Segurado há ${tempo_segurado} anos  - ` +
            `Acionou seguradora: ${acionou_seguradora} - ${vezes_acionado} vezes; \n` +
            `Principal condutor: ${ctx.principal_condutor} - ` +
            `CEP veículo: ${ctx.cep_veiculo} - ` +
            `Jovem irá dirigir: ${ctx.jovem_condutor}.`;
    }

    if (resObj.locationTitle === 'Para Empresa' && resObj.productTitle === 'Saúde') {
        resObj.message = `${ctx.texto_atividades} x ${ctx.quantidade_pessoas} Vidas`;
    }

    if (resObj.locationTitle === 'Para Mim' && resObj.productTitle === 'Saúde') {
        resObj.message = `${ctx.texto_profissoes} x ${ctx.quantidade_pessoas} Vidas`;
    }

    return resObj;
}

module.exports.mapObjFollowize = mapObjFollowize;