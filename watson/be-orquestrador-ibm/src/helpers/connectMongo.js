//@ts-check
'use strict';

const mongoose = require('mongoose');
const logger = require('./logger');
const fs = require('fs');
const path = require('path');

function connectMongo() {
    return new Promise((resolve, reject) => {
        const MONGODB = {};
        MONGODB.uri = process.env.MONGO_URI;

        const cert = `./certificates/${process.env.NODE_ENV}_mongodb.cert`;

        const certFileBuf = fs.readFileSync(path.join(process.cwd(), cert));

        /** @type{import('mongoose').ConnectionOptions}*/
        MONGODB.options = {
            // useNewUrlParser: true,
            // useCreateIndex: true,
            // useFindAndModify: true,
            user: process.env.MONGODB_USER,
            pass: process.env.MONGODB_ADM_PASS,
            auth: {
                authdb: 'admin',
            },
            replicaSet: { sslCA: certFileBuf },

            poolSize: 100,
            socketTimeoutMS: 360000,

        };

        logger.debug('Iniciando conexao com o mongo');

        // When successfully connected
        mongoose.connection.on('connected', function () {
            logger.info('Conectado com o banco de dados');
        });

        // If the connection throws an error
        mongoose.connection.on('error', function (err) {
            logger.error('Erro na conexao com o banco de dados', { error: err });
        });

        // When the connection is disconnected
        mongoose.connection.on('disconnected', function () {
            logger.warn('Conexão com o mongo caiu.');
        });

        mongoose.connect(
            MONGODB.uri,
            MONGODB.options,
            err => {
                if (err) {
                    reject(err);
                } else {
                    resolve();
                }
            });
    });
}

module.exports.init = async () => {
    await connectMongo();
};
