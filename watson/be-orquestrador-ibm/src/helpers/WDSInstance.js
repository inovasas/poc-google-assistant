//@ts-check
'use strict';

const { bindCallback, of } = require('rxjs');
const { retry, map, catchError, timeout } = require('rxjs/operators');
const WDSv1 = require('watson-developer-cloud/discovery/v1');
const sanitize = require('sanitize-html');
const commonTags = require('common-tags');

const wdsConnection = new WDSv1({
    iam_apikey: process.env.DISCOVERY_API_KEY,
    version: '2018-12-03',
});
/**
 * @type {(obj: any) => import('rxjs').Observable<any>}
 */
const wdsQuery = bindCallback(wdsConnection.query.bind(wdsConnection));
const SANITIZE_CONFIG = { allowedTags: ['p', 'ul', 'li'], allowedAttributes: {} };
const SANITIZE_CONFIG_RESTRICT = { allowedTags: [], allowedAttributes: {} };

/**
 * O métdo que executa a chamada da API do discovery e retorna o primeiro elemento
 * 
 * @param {string} collection_id O ID da coleção
 * @param {string} environment_id O ID do environment
 * @param {string} natural_language_query A string a ser mandada ao WDS
 * @returns {Promise<any>} Uma promessa com o resultado do texto da primeira
 * passagem retornada, pode ser uma string vazia
 */
function sendWDSquery(collection_id, environment_id, natural_language_query) {
    return wdsQuery({
        collection_id,
        environment_id,
        passages: true,
        count: 3,
        natural_language_query,
    }).pipe(
        map(wdsResp => {
            wdsResp = wdsResp[1];
            return wdsResp.results.map(x => {
                x.passages = [];
                wdsResp.passages.filter(passage => passage.document_id === x.id)
                    .forEach(passage => x.passages.push(passage));


                x.passages.forEach(passage => {
                    passage.passage_text = sanitize(passage.passage_text,
                        SANITIZE_CONFIG_RESTRICT);
                    passage.passage_text = commonTags.oneLineTrim(passage.passage_text);

                    if (passage.passage_text.startsWith(x.extracted_metadata.title)) {
                        passage.passage_text = passage.passage_text.slice(
                            x.extracted_metadata.title.length);
                    }
                });

                if (x.text.startsWith(x.extracted_metadata.title)) {
                    x.text = x.text.slice(x.extracted_metadata.title.length);
                }

                x.html = sanitize(x.html, SANITIZE_CONFIG);
                x.html = commonTags.oneLineTrim(x.html);

                if (x.html.startsWith(x.extracted_metadata.title)) {
                    x.html = x.html.slice(x.extracted_metadata.title.length);
                }

                if (x.passages.length === 0) {
                    x.passages.push({
                        'passage_text': sanitize(x.html.substring(0, 200),
                            SANITIZE_CONFIG_RESTRICT),
                    });
                }

                x.text = x.html; // iguala os dois campos temporariamente

                return x;
            });
        }),
        timeout(2000),
        retry(2), // Vez inicial + 2 tentativas = 3
        catchError(err => {
            console.error('Erro durante a chamada do WDS!!!\n', err);
            return of([]);
        }),
    ).toPromise();
}

module.exports = { sendWDSquery };
