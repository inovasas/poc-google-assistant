'use strict';

const autoApis = require('../repositories/autoApis');
const validateContex = require('./validateContext');

Object.assign(validateContex.posWatsonReducers, {
    async apolices_auto(watsonData, watsonResponse) {
        if (watsonResponse.context.apolices_auto !== 'missing') {
            return;
        }
        watsonResponse.context.apolices_auto = await autoApis
            .getApolices(watsonResponse.context.cpf) || [];
    },
});
