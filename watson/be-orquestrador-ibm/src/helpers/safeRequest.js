'use strict';

const request = require('request-promise-native');
const rxjs = require('rxjs');
const logger = require('./logger');
const {
    retryWhen, delay, take, map, tap, concatMap, last,
} = require('rxjs/operators');

/**
 * Função de helper para request que faz request com retry por rxjs e retorna um
 * observable para tratamento
 * 
 * @param {import('request').Options} options
 * 
 * @returns {import('rxjs').Observable<any>}
 */
module.exports.safeRequest = function (options, retryAttempts = 2) {
    return rxjs.from(request({
        ...options,
        rejectUnauthorized: false,
        simple: true,
        resolveWithFullResponse: true,
    })).pipe(
        retryWhen(errors => {
            const filter = errors.pipe(
                take(retryAttempts),
                concatMap((err, i) => (i + 1) < retryAttempts && (err.statusCode == null ||
                    err.statusCode < 200 || err.statusCode >= 500) ?
                    rxjs.of(err) : rxjs.throwError(err)),
                delay(1500),
                tap((error) => logger.warn('Safe request retry!', {
                    error, requestError: error,
                })),
            );
            return rxjs.concat(filter, filter.pipe(
                last(),
                map((err) => rxjs.throwError(err)),
            ));
        }),
        map(res => res.body),
    );
};
