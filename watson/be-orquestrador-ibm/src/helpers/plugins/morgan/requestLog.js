'use strict';

const mongoose = require('mongoose');
const morgan = require('morgan');
const stream = require('stream');
const carrier = require('carrier');
const rs = require('../../readSecret');

/**
 * RequestLog object
 * @param  {string} format - represents morgan formatting, check their github, 
 * default value is 'combined'.
 * @param  {object} options - represents morgan options, check their github, default value 
 * is empty object {}.
 */
function RequestLog(format, options = {}) {
    // Filter the arguments
    var args = Array.prototype.slice.call(arguments);

    if (args.length === 1 && typeof format === 'object') {
        throw new Error('Format parameter should be a string. Default parameter is ' +
            '\'combined\'.');
    }

    if (args.length > 1 && typeof options !== 'object') {
        throw new Error('Options parameter needs to be an object. You can specify empty ' +
            'object like {}.');
    }

    options = options || {};
    format = format || 'combined';

    // Create connection to MongoDb
    const pathMongo = process.env.MONGODB || '';
    const MONGODB = {
        uri: pathMongo
            .replace('USER', rs.getMongoUser())
            .replace('PASSWD', rs.getMongoPasswd()),
    };
    mongoose.connect(MONGODB.uri);

    // Create stream for morgan to write
    var st = new stream.PassThrough();
    // Create stream to read from
    var lineStream = carrier.carry(st);
    lineStream.on('line', onLine);

    // Morgan options stream
    options.stream = stream;

    // Create mongoose model
    const collection = 'requestLogs';
    var RequestLog = mongoose.model('RequestLog',
        new mongoose.Schema({
            date: {
                type: Date,
                default: Date.now,
            },
            log: String,
        }), collection);

    function onLine(line = '') {
        var logModel = new RequestLog();
        logModel.log = line;

        logModel.save((err) => {
            if (err) {
                throw err; 
            }
        });
    }
    return morgan(format, options);
}



module.exports = RequestLog;