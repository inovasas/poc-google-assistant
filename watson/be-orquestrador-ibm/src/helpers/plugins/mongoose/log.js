//@ts-check
'use strict';

// const LogModel = require('../../../models/v1/logs');

/* 

let preSave = async function (next) {
    var doc = this;
    // doc._seq = ((await doc.constructor.count()) + 1);

    const objLog = {
        action: 'insert',
        // key: (doc.constructor.modelName),
        key: doc.collection.name,
        fromValue: {},
        toValue: doc
    };

    new LogModel(objLog).save()
        .then((data) => {
            next();
        })
        .catch((err) => {
            next(err);
        });
}

let preUpdate = function (next, done) {
    var self = this;

    const objLog = {
        action: 'update',
        key: self.model.modelName,
        fromValue: {
            conditions: self._conditions
        },
        toValue: {
            update: self._update
        }
    };

    new LogModel(objLog).save()
        .then((data) => {
            next();
        })
        .catch((err) => {
            next(err);
        });
}

let preRemove = function (err, doc) {
    var self = this;
    console.log({
        self,
        err,
        doc
    });
}

 */

/**
 * 
 * @param {Schema} schema
 * @returns {Schema}
 */
module.exports = (schema) => {
    // schema.add({
    //     _seq: {
    //         type: Number,
    //         default: 0
    //     }
    // });

    // schema.pre('save', preSave);
    // schema.pre('remove', preRemove);
    // schema.pre('update', preUpdate);

    return schema;
};