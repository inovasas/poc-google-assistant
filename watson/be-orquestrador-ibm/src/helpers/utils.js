//@ts-check
'use strict';

const fs = require('fs');
const path = require('path');
const util = require('util');
const redis = require('./connectRedis').client;
const httpStatus = require('http-status-codes');
const _ = require('lodash');
const fp = require('lodash/fp');

/** @type {(key: string) => Promise<string>} */
const redisGetPromise = util.promisify(redis.get.bind(redis));

/**
 * 
 * @param {string} dir 
 * @param {string[]} filelist 
 * @returns {string[]}
 */
const readRecursiveDirectory = (dir, filelist = ['']) => {
    const pathDir = path.join(process.cwd(), 'src', dir);
    const files = fs.readdirSync(pathDir);
    filelist = filelist.length ? filelist : [''];
    files.forEach((file) => {
        if (fs.statSync(path.join(pathDir, file)).isDirectory()) {
            filelist = readRecursiveDirectory(path.join(dir, file), filelist);
        } else {
            filelist.push(path.join(dir, file));
        }
    });
    return filelist;
};

/**
 * 
 * @param {string} dir 
 * @param {string[]} filelist
 * @returns {string[]}
 */
const readRecursiveFile = (dir, filelist = ['']) => {
    try {
        const pathDir = path.join(process.cwd(), 'src', dir);
        const files = fs.readdirSync(pathDir);
        filelist = filelist.length ? filelist : [''];
        files.forEach((file) => {
            if (fs.statSync(path.join(pathDir, file)).isDirectory()) {
                filelist = readRecursiveFile(path.join(dir, file), filelist);
            } else {
                filelist.push(file);
            }
        });
    } catch (e) {
        console.log(e);
    }
    return filelist;
};

/**
 * 
 * @param {Object} obj
 * @returns {boolean}
 */
function isEmptyObject(obj) {
    return Object.getOwnPropertyNames(obj).length === 0;
}

/**
 * 
 * @param {Object} user
 * @returns {Object}
 */
function parseUser(user) {
    if (!user || typeof (user) !== 'object') {
        return {
            name: 'N/A',
            enrollment: 'N/A',
            site: 'N/A',
        };
    }
    return {
        name: user.name,
        enrollment: user.enrollment,
        site: user.site,
    };
}

/**
 * 
 * @param {number} w
 * @param {number} y
 */
function getDateOfISOWeek(w, y) {
    var simple = new Date(y, 0, 1 + (w - 1) * 7);
    var dow = simple.getDay();
    var ISOweekStart = simple;
    if (dow <= 4) {
        ISOweekStart.setDate(simple.getDate() - simple.getDay() + 1);
    } else {
        ISOweekStart.setDate(simple.getDate() + 8 - simple.getDay());
    }
    return ISOweekStart;
}

/**
 * Trata a resposta dos links para substuir os links com o guarda de links.
 *
 * @param {URL} reqUrl URL do request
 * @param {any} wdsResp Resposta do watson.
 */
function applyLinkGuard(reqUrl, wdsResp) {
    reqUrl.search = '';
    reqUrl.pathname = '/api/v1/linkGuard';
    /** @type {string[]} */
    const formattedOutput = [];
    /** Iterates over all messages of the output.
     * @type {string}
     */
    for (let msg of wdsResp.output.text) {
        // const linkRegex = /https?:\/\/[^" ]*/gi;
        const linkRegex = /(\shref\s*=\s*["']?)(https?:\/\/[^"'>\s]*)/gi;
        /** @type {RegExpExecArray} */
        let nextLink;
        while ((nextLink = linkRegex.exec(msg)) != null) {
            const origLink = nextLink[2];
            // Usuario mesmo do log e é o que vai ser mandado ao chatbase!
            reqUrl.search = `?target=${encodeURIComponent(origLink)}` +
                `&cid=${encodeURIComponent(wdsResp.context.conversation_id)}`;
            if (wdsResp.context.usuario) {
                reqUrl.search += `&user=${encodeURIComponent(wdsResp.context.usuario)}`;
            }
            msg = msg.substring(0, nextLink.index + nextLink[1].length) + reqUrl.href +
                msg.substring(nextLink.index + nextLink[0].length);
        }
        formattedOutput.push(msg);
    }
    wdsResp.output.text = formattedOutput;
}

function normalizeInput(input) {
    if (!input) {
        return '';
    }
    if (typeof input === 'string') {
        return input;
    }
    if (!_.isEmpty(input) && typeof input.text !== 'string') {
        throw {
            code: httpStatus.BAD_REQUEST,
            msg: 'Propriedade `input` deve ser uma string ou um objeto com a ' +
                'propiedade text (string)',
        };
    }
    return input.text || '';
}

/** @type {((string) => string)} */
module.exports.capitalize = fp.compose(_.startCase, _.lowerCase);
module.exports.readRecursiveFile = readRecursiveFile;
module.exports.readRecursiveDirectory = readRecursiveDirectory;
module.exports.isEmptyObject = isEmptyObject;
module.exports.parseUser = parseUser;
module.exports.getDateOfISOWeek = getDateOfISOWeek;
module.exports.applyLinkGuard = applyLinkGuard;
module.exports.redisGetPromise = redisGetPromise;
module.exports.normalizeInput = normalizeInput;
