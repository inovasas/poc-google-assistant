'use strict';

const yup = require('yup');

module.exports = yup.date().transform(function (value) {
    if (this.isType(value)) {
        return value;
    }
    if (Number.isInteger(value)) {
        return new Date(value);
    }
    return value;
});
