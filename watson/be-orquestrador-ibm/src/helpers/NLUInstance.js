//@ts-check
'use strict';

const NLU = require('watson-developer-cloud/natural-language-understanding/v1');

/**
 *
 * @param {String} text
 * @param {Function} cb
 * @param {boolean} cb
 */
async function analyze(text, cb) {
    try {
        const txt = await analyzeText(text);
        return cb(null, txt);
    } catch (err) {
        return cb(err, null);
    }
}

/**
 *
 * @param {String} text
 * @returns {Promise}
 */
async function analyzeText(text) {
    const nlu = new NLU({
        version: process.env.NLU_API_VERSION,
        iam_apikey: process.env.NLU_API_KEY,
        url: process.env.NLU_URL,
    });

    const parameters = {
        'text': text,
        'language': 'pt',
        'features': {
            'entities': {
                'limit': 2,
            },
        },
    };

    return new Promise(async (resolve, reject) => {
        nlu.analyze(parameters, (err, resp) => {
            if (err) {
                console.error('message');
                console.error(JSON.stringify(err));
                return reject(err);
            } else {
                return resolve(resp);
            }
        });
    });
}

module.exports.analyze = analyze;