//@ts-check
'use strict';

const AssistantV1 = require('watson-developer-cloud/assistant/v1');
const { preHandleContext, posHandleContext } = require('../helpers/validateContext');
const redis = require('../helpers/connectRedis').client;

/** @param {Object} bd */
async function sendMessage(bd) {
    try {
        bd.context = await preHandleContext(bd);

        const respWatson = await message(bd);

        respWatson.context = await posHandleContext(bd, respWatson);

        redis.set(respWatson.context.conversation_id, bd.workspace_id, 'EX', 5 * 60);

        return respWatson;
    } catch (err) {
        throw err;
    }
}

/**
 *
 * @param {Object} body
 */
async function message(body) {

    const {
        workspace_id,
        input,
        context,
        username,
        password,
    } = body;

    const msg = await sendWatsonMessage(workspace_id, input, context, username, password);
    return msg;
}

/**
 *
 * @param {string} workspace_id
 * @param {string} input
 * @param {Object} context
 * @param {string} username
 * @param {string} password
 * @returns {Promise}
 */
const sendWatsonMessage = (workspace_id, input, context, username, password) => {
    context = context || {};
    const wa = new AssistantV1({
        username: username || '',
        password: password || '',
        version: process.env.CONVERSATION_API_VERSION || '2019-02-28',
    });


    return new Promise((resolve, reject) => {
        wa.message({
            workspace_id,
            input: {
                text: input.replace(/\r|\n|\r|\t/g, ''),
            },
            context,
            nodes_visited_details: true,
            alternate_intents: true,
        }, (err, resp) => {
            if (err) {
                console.error('message');
                console.error(JSON.stringify(err));
                return reject(err);
            } else {
                return resolve(resp);
            }
        });

    });
};


module.exports.sendMessage = sendMessage;
