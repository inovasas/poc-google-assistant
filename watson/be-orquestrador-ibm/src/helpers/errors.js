'use strict';

const statusCodes = require('http-status-codes');

class ErroOrquestrador extends Error {
    /**
     * 
     * @param {string} message mensagem de Erro
     * @param {number} status Codigo de erro HTTP
     */
    constructor(message, status) {
        super(message);
        this.name = this.constructor.name;
        Error.captureStackTrace(this, this.constructor);
        this.status = status || 500;
    }
}

class ErroAutenticacao extends ErroOrquestrador {
    /**
     * Constrói um novo objeto de erro representando um erro de/durante a autenticação
     *
     * @param {string} api Especifica em qual das API's ocorreu o erro de autenticação
     */
    constructor(api) {
        super('Erro de autenticação na API:' + api, statusCodes.UNAUTHORIZED);
    }
}

class ProdutoInativo extends ErroOrquestrador {
    constructor() {
        super('Produto inativo', 500);
    }
}

module.exports = { ErroOrquestrador, ProdutoInativo, ErroAutenticacao };
