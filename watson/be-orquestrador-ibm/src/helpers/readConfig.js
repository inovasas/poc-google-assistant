//@ts-check
'use strict';

const path = require('path');

module.exports.readEnvFile = () => {
    try {

        // disable local file env configuration
        if (process.env.DISABLE_FILE_CONF) {
            return;
        }

        let pwd = process.cwd();
        pwd += '/src/dotenv/';
        const fileConf = process.env.FILE_CONF;
        const nodeEnv = process.env.NODE_ENV;
        let fileName = '_local.env';
        let file = '';
        if (typeof fileConf !== 'string' ||
            fileConf === '') {
            file = pwd + fileName;
            if (typeof nodeEnv === 'string' &&
                nodeEnv !== '') {
                switch (nodeEnv) {
                    case 'production':
                        fileName = '_prd.env';
                        break;
                    case 'statement':
                        fileName = '_homol.env';
                        break;
                    case 'development':
                        fileName = '_dev.env';
                        break;
                    default:
                }
                file = pwd + fileName;
            }
        } else {
            file = fileConf;
        }

        console.debug(`file ==> ${file}`);
        console.debug(`nodeEnv ==> ${nodeEnv}`);

        require('dotenv').config({
            path: path.resolve(file),
        });
    } catch (e) {
        console.info(e);
    }
};