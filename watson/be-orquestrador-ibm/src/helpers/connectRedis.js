'use strict';

const { URL } = require('url');
const utils = require('util');
const redis = require('redis');
const logger = require('./logger');

if (typeof process.env.REDIS !== 'string' || process.env.REDIS === '') {
    logger.error('URL DO REDIS NAO ENCONTRADA!!!');
    process.exit(1);
}

const opts = {};

if (process.env.NODE_ENV !== 'local') {
    opts.tls = {
        servername: new URL(process.env.REDIS).hostname,
    };
}

const client = redis.createClient(process.env.REDIS, opts);

client.on('connect', () => {
    logger.info('Conectado com sucesso ao servidor redis!');
});
client.on('error', (err) => {
    logger.error('Algo deu errado com a conexão, reconectando ==> ', err);
});

/**
 * 
 * @param {string} key
 * @param {Object} value
 * @param {number} seconds
 */
const set = (key, value, seconds) => {
    client.set(key, value, 'EX', seconds);
};

const rediseval = utils.promisify(client.eval.bind(client));

/**
 * @param {string} key
 * @param {any} val
 * @param {number} ttl
 * 
 * @returns {Promise<boolean>}
 */
module.exports.update = function update(key, val, ttl = 0) {
    // Código LUA para fazer uma transaction de update no redis
    return rediseval(`
        local ttl = redis.call('TTL', KEYS[1])
        if ttl > 0 then 
            return redis.call('SETEX', KEYS[1], ttl, ARGV[1])
        elseif tonumber(ARGV[2]) > 0 then
            return redis.call('SETEX', KEYS[1], ARGV[2], ARGV[1])
        end
        return false
    `.trim(), 1, key, val, ttl).catch((error) => {
        logger.error('Erro ao atualizar chave no redis', { error });
    });
};
module.exports.client = client;
module.exports.get = utils.promisify(client.get.bind(client));
module.exports.set = set;