'use strict';


let errors = [];

module.exports.isRequired = async (value, message) => {
    if (!value || value.length <= 0) {
        errors.push({
            message: message,
        });
    }
};

module.exports.hasMinLen = async (value, min, message) => {
    if (!value || value.length < min) {
        errors.push({
            message: message,
        });
    }
};

module.exports.hasMaxLen = async (value, max, message) => {
    if (!value || value.length > max) {
        errors.push({
            message: message,
        });
    }
};

module.exports.isFixedLen = async (value, len, message) => {
    if (!value || value.length !== len) {
        errors.push({
            message: message,
        });
    }
};

module.exports.isEmail = (value, message) => {
    const reg = new RegExp(/\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*/);
    if (!reg.test(value)) {
        errors.push({
            message: message,
        });
    }
};



module.exports.errors = errors;

module.exports.clear = async () => {
    errors = [];
};

module.exports.isValid = async () => {
    return errors.length === 0;
};