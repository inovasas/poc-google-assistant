'use strict';

const _ = require('lodash');
const fp = require('lodash/fp');

/** @type{(string) => string} */
const capitalize = fp.compose(_.startCase, _.lowerCase);

/**
 * Trata resultado de especialidades
 * 
 * @param {any[]} especialidades Lista de especialidades
 * @returns {any[]} Lista formatada
 */
function specialistsToVoice(especialidades) {
    especialidades.forEach(x => {
        const telefoneA = _.get(x.telefones.find(x => x.tipo === 'A'), 'telefone', '');
        x.telefoneVoz = telefoneA.replace(/[(). -]/g, '').split('')
            .map((x, i) => {
                if (i % 2 === 0 && i > 0) {
                    return '<break time="150ms"/>' + x;
                }
                return x;
            }).join('');
        /** @type{string[]} */
        let endereco = x.endereco.endereco.split(' ');
        // FIXME: Fazer uma solução de depara mais dinâmica e configurável em runtime
        switch (endereco[0].toLowerCase()) {
            case 'r':
                endereco[0] = 'Rua';
                break;
            case 'av':
                endereco[0] = 'Avenida';
                break;
            case 'pca':
                endereco[0] = 'Praça';
                break;
            case 'al':
                endereco[0] = 'Alameda';
                break;
            case 'lgo':
                endereco[0] = 'Largo';
                break;
        }
        endereco = _.dropWhile(endereco, (x, i) =>
            _.toLower(endereco[i + 1]) === _.toLower(x));
        if (endereco.length >= 3) {
            endereco[endereco.length - 2] = endereco[endereco.length - 2] + ',';
        }
        x.logradouro = capitalize(endereco.join(' ')) + ', ' + x.endereco.numeroEndereco;
    });
    return especialidades;
}

module.exports = { specialistsToVoice };
