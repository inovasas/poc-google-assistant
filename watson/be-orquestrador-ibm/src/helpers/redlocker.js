'use strict';

const Redis = require('ioredis');
const Redlock = require('redlock');
const logger = require('./logger');

const client = new Redis(process.env.REDIS, {
    tls: process.env.NODE_ENV !== 'local' ? {
        servername: new URL(process.env.REDIS).hostname,
    } : null,
});

client.on('error', (error) => {
    logger.error('Erro na conexão do redlock!!!', { error });
});

client.on('ready', () => {
    logger.debug('Redlock connected');
});

const redlock = new Redlock([client], { retryCount: 0 });

module.exports = redlock;
