'use strict';

const LogDNAWinston = require('logdna-winston');
const { createLogger, format, transports } = require('winston');
const _ = require('lodash');
const util = require('util');

const long = format((info) => {
    const others = _.omit(info, ['message', 'level', 'timestamp', Symbol.for('message'),
        Symbol.for('level')]);
    if (info.timestamp) {
        const extras = _.map(others, (val, key) => '\n' + key + ': ' + util.inspect(val))
            .join('');
        // @ts-ignore
        info[Symbol.for('message')] =
            `[${info.timestamp}] ${info.level}: ${info.message} ${extras}`;
    }
    return info;
});

const logger = createLogger({
    level: 'info',
    format: format.json(),
    transports: [
        new LogDNAWinston({
            key: process.env.LOGDNA_KEY,
            app: 'orquestrador_oficina_google',
            env: process.env.NODE_ENV || 'local',
            logdna_url: 'https://logs.us-south.logging.cloud.ibm.com/logs/ingest',
        }),
    ],
});

if (process.env.NODE_ENV !== 'production') {
    logger.add(new transports.Console({
        level: 'debug',
        format: format.combine(
            format.timestamp(),
            format.colorize(),
            long(),
        ),
    }));
} else {
    // FIXME: remover quando kibana for depreciado.
    logger.add(new transports.Console({
        format: format.simple(),
    }));
}

module.exports = logger;
