'use strict';

const logger = require('../helpers/logger');

/** @type {{[val: string]: (key: any) => any}} */
const mapper = {
    'nome': async (nome) => ({
        'first_name': nome,
    }),
    'cpf': async (cpf) => ({
        '00N3100000GMpnV': cpf,
    }),
    'cnpj': async (cnpj) => ({
        '00N3100000GMpnT': cnpj,
    }),
    'para_quem': async (paraQuem) => ({
        '00N3100000GvfGn': paraQuem === 'para_mim' ? 1 : '',
        '00N3100000GvfGp': paraQuem === 'empresa' ? 1 : '',
    }),
    'assunto': async (assunto) => ({
        '00N3100000GvfGc': assunto === 'auto' ? 1 : '',
        '00N3100000GvfGf': assunto === 'saude' ? 1 : '',
        '00N3100000GvfGd': assunto === 'odonto' ? 1 : '',
    }),
    'texto_profissoes': async (profissao) => ({
        '00N3100000GvfGq': profissao,
    }),
    'quantidade_pessoas': async (quantidadePessoas) => ({
        '00N3100000GvfGt': quantidadePessoas,
    }),
    'texto_frota': async (frota) => ({
        '00N3100000GvfWJ': frota,
    }),
    'quantidade_veiculos': async (quantidadeVeiculos) => ({
        '00N3100000GvfGu': quantidadeVeiculos,
    }),
    'texto_atividades': async (atividadeEmpresa) => ({
        '00N3100000GvfGr': atividadeEmpresa,
    }),
    'localidade': async (localidade) => ({
        '00N3100000GvfGx': localidade.uf,
        '00N3100000GvfGv': localidade.regiao,
    }),
};

const watsonBlackList = ['conversation_id', 'system', 'marca', 'texto_marca', 'seguradora',
    'texto_seguradora'];

/**
 *
 * @param {Object} context
 * @return {Promise<any>} obj 
 */
async function mapObj(context) {
    const resultObj = {};
    for (const key of Object.keys(context)) {
        if (watsonBlackList.includes(key)) {
            continue;
        }
        const funcMapeador = mapper[key];
        let newValue = {};

        if (funcMapeador) {
            newValue = await funcMapeador(context[key]);
        } else {
            newValue[key] = context[key];
        }
        Object.assign(resultObj, newValue);
    }

    resultObj.oid = process.env.ID_SALESFORCE;

    resultObj['00N3100000GvfGZ'] = 'TA_COM_TUDO';

    resultObj.debug = 1;
    resultObj.debugEmail = 'fernando.sales@sulamerica.com.br';

    logger.info('----- result obj ----- ', { resultObj });

    return resultObj;
}

module.exports.mapObj = mapObj;