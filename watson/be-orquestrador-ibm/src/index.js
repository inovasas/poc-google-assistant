//@ts-check
'use strict';

const express = require('express');
const winston = require('./helpers/logger');
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');
const cors = require('cors');
const da = require('./helpers/connectMongo');
const middleware = require('./middlewares/externalTokenHeader');
const utils = require('./helpers/utils');
const expressLog = require('express-winston');
const compression = require('compression');
const rateLimit = require('express-rate-limit');
const httpStatus = require('http-status-codes');
const timeout = require('connect-timeout');
const helmet = require('helmet');
const moment = require('moment');
require('moment/locale/pt-br');
moment.locale('pt-BR');

const app = express();

app.use(expressLog.logger({
    winstonInstance: winston,
    colorize: process.env.NODE_ENV === 'local' || !process.env.NODE_ENV,
    meta: true,
    // @ts-ignore
    statusLevels: true,
}));

if (process.platform !== 'win32') {
    process.stderr.pipe(winston);
}

const ignoreFavicon = (req, res, next) => {
    if (req.originalUrl === '/favicon.ico') {
        res.status(204).json({
            nope: true,
        });
    } else {
        next();
    }
};

// gzip
const shouldCompress = (req, res) => {
    if (req.headers['x-no-compression']) {
        // don't compress responses with this request header
        return false;
    }
    // fallback to standard filter function
    return compression.filter(req, res);
};

// helmet
app.use(helmet());

app.enable('trust proxy');
// @ts-ignore
app.use(rateLimit({
    windowMs: 1000,
    max: 5000,
}));

app.use(compression({
    filter: shouldCompress,
}));

winston.debug(`process.env.NODE_ENV: ${process.env.NODE_ENV}`);

app.use(bodyParser.json({
    limit: '50mb',
}));
app.use(bodyParser.urlencoded({
    parameterLimit: 100000,
    limit: '50mb',
    extended: false,
}));
// app.use(getHostOrigin);
app.use(cookieParser());

app.use(cors());
app.use(ignoreFavicon);
app.use(timeout('20s'));
// app.use(haltOnTimedout);
da.init(); //('models/v1');

// -- INICIO --
//Mapeia rotas automatico dentro da pasta routes
const fileRoutes = utils.readRecursiveDirectory('routes').filter(item => {
    return item !== '';
});
app.use(/^\/api\/v1\/.+$/, middleware.readTokenMetadata);

fileRoutes.forEach(file => {
    const rf = require('./' + file.replace('.js', ''));
    const fn = file
        .replace('routes', '')
        .split('\\')
        .join('/')
        .replace('.js', '');
    app.use(fn, rf);
    winston.debug('Route ' + fn + ' --> ok!');
});
// -- FIM --

// app.use('/api/_bkp_/', express.static(__dirname + '/__bkp$'));
// app.use('/api/_log_', express.static(__dirname + '/__log$'));

app.get('/api', (req, res) => {
    res.status(200).json({
        title: 'WCS Orquestrador - SulAmerica',
        version: '0.0.1',
        path: '/api/v1',
    });
});

// app.use(catch404);

/** @param {any} _ */
// eslint-disable-next-line no-unused-vars
app.use((err, req, res, next) => { // jshint ignore:line
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};
    if (err.code === 'ETIMEDOUT') {
        winston.error('Erro de timeout! Já respondendo o request com a mensagem de timeout.');
        err.status = httpStatus.GATEWAY_TIMEOUT;
        err.message = 'Timeout na conexão com os serivços.';
    } else {
        winston.error('Exeção sincrona não tratada!:\n', err);
    }

    if (err.message === 'message') {
        winston.info(err);
    }

    res.status(err.status || 500).send({
        status: err.status,
        message: err.message,
        // objErr: err
    });
});

module.exports = app;
