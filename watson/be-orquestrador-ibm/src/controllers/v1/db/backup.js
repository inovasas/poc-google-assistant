//@ts-check
'use strict';

const httpStatus = require('http-status-codes');
const fs = require('fs');
const vr = require('../../../helpers/validateRequest');
const utils = require('../../../helpers/utils');
const archiver = require('archiver');
const mongoose = require('mongoose');
const path = require('path');
const _ = require('lodash');


const lengthFile = 50000;

/**
 *
 * @param {string} str
 * @param {number} length
 */
const chunkString = (str, length) => {
    return str.match(new RegExp(`.{1,${length}}`, 'gi'));
};


/**
 *
 * @param {Object} req
 * @param {Object} res
 */
module.exports.do = (req, res) => {
    const errors = vr.validateFieldsInQuery(['key'], req);
    if (errors) {
        return res.status(httpStatus.INTERNAL_SERVER_ERROR).json({
            success: false,
            error: {
                msg: errors,
            },
        });
    }
    const newDate = new Date();
    const dd = _.padStart(newDate.getDate().toString(), 2, '0');
    const mm = _.padStart((newDate.getMonth() + 1).toString(), 2, '0');
    const yyyy = newDate.getFullYear().toString();

    if (req.query.key && req.query.key === `podecopiar${newDate.getDate().toString()}`) {
        try {
            const schema = req.query.schema || '';
            /** @type{any} */
            const connection = mongoose.connection || {};
            const backup = {};
            const arrNotActivated = ['Experiment', 'Importation', 'Log', 'RegularEvaluation',
                'ServiceNumber'];
            let models = [];
            if (schema === '') {
                models = Object.keys(connection.models);
            } else {
                models = [schema];
            }

            const filePath = path.join(__dirname, '../../../__bkp$');
            const arrFiles = [];
            console.log(`filePath: ${filePath}`);

            const promises = models.map((item) => {
                return new Promise(async (resolve, reject) => {
                    try {
                        if (!backup.hasOwnProperty(item)) {
                            if (_.filter(arrNotActivated, (o) => {
                                return item === o;
                            }).length) {
                                backup[item] = await connection.models[item].find({});
                            } else {
                                backup[item] = await connection.models[item].find({
                                    activated: true,
                                });
                            }


                            const strSchema = JSON.stringify(backup[item]);
                            const files = chunkString(strSchema, lengthFile);
                            for (let i = 0; i < files.length; i++) {
                                const file = files[i];

                                const fileName = `${yyyy}${mm}${dd}_${item}_${i}.txt`;

                                if (fs.existsSync(`${filePath}/${fileName}`)) {
                                    fs.unlinkSync(`${filePath}/${fileName}`);
                                }
                                arrFiles.push(`${filePath}/${fileName}`);
                                fs.writeFileSync(`${filePath}/${fileName}`, file);
                                console.info(`File ${fileName} was created.`);
                            }

                            console.info(`The ${item} collection was copied.`);
                        }
                        resolve();
                    } catch (err) {
                        reject(err);
                    }
                });
            });

            Promise.all(promises).then(() => {
                const output = fs.createWriteStream(`${filePath}/${yyyy}${mm}${dd}_txt.zip`);
                const archive = archiver('zip', {
                    zlib: {
                        level: 9,
                    },
                });
                archive.pipe(output);
                archive.glob(`${filePath}/*.txt`);
                archive.finalize();
                output.on('close', () => {
                    console.log(archive.pointer() + ' total bytes');
                    for (let i = 0; i < arrFiles.length; i++) {
                        const file = arrFiles[i];
                        fs.unlinkSync(file);
                    }
                    console.log('Backup finalizado com sucesso');
                });
            }).catch((error) => {
                console.log('erro ao realiar backup: ' + error);
            });

            return res.status(httpStatus.OK).json({
                success: true,
                dd,
                file: `/api/_bkp_/${yyyy}${mm}${dd}_bkp.zip`,
            });
        } catch (err) {
            return res.status(httpStatus.INTERNAL_SERVER_ERROR).json({
                success: false,
                error: err,
                dd,
            });
        }
    } else {
        return res.status(httpStatus.INTERNAL_SERVER_ERROR).json({
            success: false,
            dd,
        });
    }
};

/**
 *
 * @param {Object} req
 * @param {Object} res
 */
module.exports.clearOldBkp = async (req, res) => {
    const errors = vr.validateFieldsInQuery(['key'], req);
    if (errors) {
        return res.status(httpStatus.INTERNAL_SERVER_ERROR).json({
            success: false,
            error: {
                msg: errors,
            },
        });
    }
    const newDate = new Date();
    const dd = _.padStart(newDate.getDate().toString(), 2, '0');
    const mm = _.padStart((newDate.getMonth() + 1).toString(), 2, '0');
    const yyyy = newDate.getFullYear().toString();

    if (req.query.key && req.query.key === `podecopiar${newDate.getDate().toString()}`) {
        try {
            const filePath = path.join(__dirname, '../../../__bkp$');
            const fileName = `${yyyy}${mm}${dd}_bkp.zip`;
            const files = utils
                .readRecursiveDirectory('__bkp$')
                .filter(item => {
                    return item !== `__bkp$/${fileName}` && item !== '__bkp$/.gitkeep' &&
                        item !== '__bkp$/keep.git' && item !== '';
                });

            for (let i = 0; i < files.length; i++) {
                const file = files[i];
                fs.unlinkSync(`${filePath}/${file.replace('__bkp$/', '')}`);
            }


            return res.status(httpStatus.OK).json({
                success: true,
                dd,
            });
        } catch (err) {
            return res.status(httpStatus.INTERNAL_SERVER_ERROR).json({
                success: false,
                error: err,
                dd,
            });
        }
    } else {
        return res.status(httpStatus.INTERNAL_SERVER_ERROR).json({
            success: false,
            dd,
        });
    }
};