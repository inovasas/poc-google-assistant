//@ts-check
'use strict';

// const workspaceRepositorio = require('../../../repositories/workspace');
// const answerRepositorio = require('../../../repositories/answer');

module.exports.fixDb = async () => {

    // const day = new Date().getDate();
    // const {
    //     key,
    // } = req.query;

    // if (key && key === `podearrumar${day}`) {

    //     try {
    //         await workspaceRepositorio.find({
    //             activated: true
    //         }, {
    //             _id: 1
    //         }).then(async (result) => {
    //             for (let i = 0; i < result.length; i++) {
    //                 const element = result[i];
    //                 await corrigeWks(element._id);
    //             }
    //         });
    //         return res.status(httpStatus.OK).json({
    //             success: true,
    //             day
    //         });
    //     } catch (err) {
    //         console.log(`Catch Error ${err}`);
    //         return res.status(httpStatus.INTERNAL_SERVER_ERROR).json({
    //             success: false,
    //             error: err,
    //             day
    //         });
    //     }

    // } else {
    //     return res.status(httpStatus.INTERNAL_SERVER_ERROR).json({
    //         success: false,
    //         day
    //     });
    // }
};

// function corrigeWks(wksId) {
//     return new Promise(async (resolve, reject) => {
//         const defaultUser = {
//             name: 'Not Found',
//             enrollment: 'N/A',
//             site: 'Not Found'
//         };

//         await workspaceRepositorio.find({
//             _id: wksId,
//             activated: true
//         }).then((result) => {
//             result.forEach(async (wks) => {
//                 wks.intents.forEach(i => {
//                     if (!i.createdAt) {
//                         i.createdAt = Date.now();
//                     }
//                     if (!i.updatedAt) {
//                         i.createdAt = Date.now();
//                     }
//                     if (!i.createdBy) {
//                         i.createdBy = defaultUser;
//                     }
//                     if (!i.updatedBy) {
//                         i.updatedBy = defaultUser;
//                     }
//                     i.examples.forEach(e => {
//                         if (!e.createdAt) {
//                             e.createdAt = Date.now();
//                         }
//                         if (!e.updatedAt) {
//                             e.createdAt = Date.now();
//                         }
//                         if (!e.createdBy) {
//                             e.createdBy = defaultUser;
//                         }
//                         if (!e.updatedBy) {
//                             e.updatedBy = defaultUser;
//                         }
//                     });
//                 });
//                 wks.entities.forEach(e => {
//                     if (!e.createdAt) {
//                         e.createdAt = Date.now();
//                     }
//                     if (!e.updatedAt) {
//                         e.createdAt = Date.now();
//                     }
//                     if (!e.createdBy) {
//                         e.createdBy = defaultUser;
//                     }
//                     if (!e.updatedBy) {
//                         e.updatedBy = defaultUser;
//                     }
//                     e.values.forEach(ev => {
//                         if (!ev.createdAt) {
//                             e.createdAt = Date.now();
//                         }
//                         if (!ev.updatedAt) {
//                             e.createdAt = Date.now();
//                         }
//                         if (!ev.createdBy) {
//                             ev.createdBy = defaultUser;
//                         }
//                         if (!ev.updatedBy) {
//                             ev.updatedBy = defaultUser;
//                         }
//                     });
//                 });
//                 wks.dialog_nodes.forEach(d => {
//                     if (!d.createdAt) {
//                         d.createdAt = Date.now();
//                     }
//                     if (!d.updatedAt) {
//                         d.createdAt = Date.now();
//                     }
//                     if (!d.createdBy) {
//                         d.createdBy = defaultUser;
//                     }
//                     if (!d.updatedBy) {
//                         d.updatedBy = defaultUser;
//                     }
//                     if (d.output && !d.output.text) {
//                         d.output.text = [];
//                     }
//                 });

//                 wks.save();
//                 await answerRepositorio.find({
//                     workspace: wksId
//                 }).then((result) => {
//                     result.forEach(a => {
//                         if (!a.createdAt) {
//                             a.createdAt = Date.now();
//                         }
//                         if (!a.updatedAt) {
//                             a.createdAt = Date.now();
//                         }
//                         if (!a.createdBy) {
//                             a.createdBy = defaultUser;
//                         }
//                         if (!a.updatedBy) {
//                             a.updatedBy = defaultUser;
//                         }
//                         if (!a.defaultAnswer) {
//                             a.defaultAnswer = false;
//                         }
//                         a.save();
//                     });

//                 });

//                 console.log(`Fixed ${wks.name}`);
//                 return resolve(true);
//             });
//         });
//     });
// }