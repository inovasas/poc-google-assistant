//@ts-check
'use strict';

const httpStatus = require('http-status-codes');
const fs = require('fs');
const vr = require('../../../helpers/validateRequest');
const utils = require('../../../helpers/utils');
const mongoose = require('mongoose');
const path = require('path');
const _ = require('lodash');

/**
 *
 * @param {Object} req
 * @param {Object} res
 */
module.exports.do = async (req, res) => {
    const errors = vr.validateFieldsInQuery(['key'], req);
    if (errors) {
        return res.status(httpStatus.INTERNAL_SERVER_ERROR).json({
            success: false,
            error: {
                msg: errors,
            },
        });
    }
    const newDate = new Date();
    const dd = _.padStart(newDate.getDate().toString(), 2, '0');
    const mm = _.padStart((newDate.getMonth() + 1).toString(), 2, '0');
    const yyyy = newDate.getFullYear().toString();
    const fileName = `${yyyy}${mm}${dd}.export`;


    if (req.query.key && req.query.key === `podeexportar${newDate.getDate().toString()}`) {
        try {
            const connection = mongoose.connection || {};
            const backup = {};
            // @ts-ignore
            const models = Object.keys(connection.models);
            const filePath = path.join(__dirname, '../../../__bkp$');
            const arrExcludeSchemas = ['CHAT', 'LOG',
                'SERVICENUMBER', 'EXPERIMENT',
                'SESSION', 'IMPORTATION',
                'REGULAREVALUATION', 'INTENT',
                'INTENTEXAMPLE', 'ENTITY',
                'ENTITYVALUE', 'DIALOG',
            ];



            const promises = models.map((item) => {
                return new Promise(async (resolve) => {
                    if (!backup.hasOwnProperty(item.toUpperCase()) &&
                        !_.filter(arrExcludeSchemas, (o) => {
                            return item.toUpperCase() === o;
                        }).length) {
                        // @ts-ignore
                        backup[item.toUpperCase()] = await connection.models[item].find({
                            activated: true,
                        });
                    }
                    resolve();
                });
            });


            Promise.all(promises)
                .then(() => {
                    if (fs.existsSync(`${filePath}/${fileName}`)) {
                        fs.unlinkSync(`${filePath}/${fileName}`);
                    }
                    fs.writeFileSync(`${filePath}/${fileName}`, JSON.stringify(backup));
                    console.log('Export finalizado com sucesso');
                })
                .catch((error) => {
                    console.log('erro ao realiar export: ' + error);
                });


            return res.status(httpStatus.OK).json({
                success: true,
                dd,
                // file: `/api/_bkp_/${yyyy}${mm}${dd}_export.zip`
                file: `/api/_bkp_/${yyyy}${mm}${dd}.export`,
            });
        } catch (err) {
            return res.status(httpStatus.INTERNAL_SERVER_ERROR).json({
                success: false,
                error: err,
                dd,
            });
        }
    } else {
        return res.status(httpStatus.INTERNAL_SERVER_ERROR).json({
            success: false,
            dd,
        });
    }
};

/**
 *
 * @param {Object} req
 * @param {Object} res
 */
module.exports.clearOldExport = async (req, res) => {
    const errors = vr.validateFieldsInQuery(['key'], req);
    if (errors) {
        return res.status(httpStatus.INTERNAL_SERVER_ERROR).json({
            success: false,
            error: {
                msg: errors,
            },
        });
    }
    const newDate = new Date();
    const dd = _.padStart(newDate.getDate().toString(), 2, '0');
    const mm = _.padStart((newDate.getMonth() + 1).toString(), 2, '0');
    const yyyy = newDate.getFullYear().toString();

    if (req.query.key && req.query.key === `podeexportar${newDate.getDate().toString()}`) {
        try {
            const filePath = path.join(__dirname, '../../../__bkp$');
            const fileName = `${yyyy}${mm}${dd}.export`;
            const files = utils
                .readRecursiveDirectory('__bkp$')
                .filter(item => {
                    return item !== `__bkp$/${fileName}` && item !== '__bkp$/.gitkeep' &&
                        item !== '__bkp$/keep.git' && item !== '';
                });

            for (let i = 0; i < files.length; i++) {
                const file = files[i];
                fs.unlinkSync(`${filePath}/${file.replace('__bkp$/', '')}`);
            }


            return res.status(httpStatus.OK).json({
                success: true,
                dd,
            });
        } catch (err) {
            return res.status(httpStatus.INTERNAL_SERVER_ERROR).json({
                success: false,
                error: err,
                dd,
            });
        }
    } else {
        return res.status(httpStatus.INTERNAL_SERVER_ERROR).json({
            success: false,
            dd,
        });
    }
};