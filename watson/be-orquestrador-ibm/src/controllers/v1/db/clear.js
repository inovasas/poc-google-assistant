//@ts-check
'use strict';

const httpStatus = require('http-status-codes');
const vr = require('../../../helpers/validateRequest');
const mongoose = require('mongoose');


/**
 *
 * @param {Object} req
 * @param {Object} res
 */
module.exports.clear = async (req, res) => {
    const errors = vr.validateFieldsInQuery(['key'], req);
    if (errors) {
        return res.status(httpStatus.INTERNAL_SERVER_ERROR).json({
            success: false,
            error: {
                msg: errors,
            },
        });
    }
    const day = new Date().getDate();

    if (req.query.key && req.query.key === `podeapagar${day}`) {
        try {
            const connection = mongoose.connection;
            Object.keys(connection.models).forEach(async collection => {
                await connection.models[collection].remove({});
                console.info(`${collection} was clean`);
            });

            return res.status(httpStatus.OK).json({
                success: true,
                day,
            });
        } catch (err) {
            return res.status(httpStatus.INTERNAL_SERVER_ERROR).json({
                success: false,
                error: err,
                day,
            });
        }
    } else {
        return res.status(httpStatus.INTERNAL_SERVER_ERROR).json({
            success: false,
            day,
        });
    }
};