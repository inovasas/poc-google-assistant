//@ts-check
'use strict';

const httpStatus = require('http-status-codes');
const vr = require('../../../helpers/validateRequest');
const mongoose = require('mongoose');

/**
 *
 * @param {Object} req
 * @param {Object} res
 */
module.exports.get = async (req, res) => {
    const errors = vr.validateFieldsInQuery(['key', 'schema'], req);
    if (errors) {
        return res.status(httpStatus.INTERNAL_SERVER_ERROR).json({
            success: false,
            error: {
                msg: errors,
            },
        });
    }
    const day = new Date().getDate();

    if (req.query.key && req.query.schema && req.query.key === `podemostrar${day}`) {
        try {
            const connection = mongoose.connection;
            const dataFromDb = req.query.id ?
                await connection.models[req.query.schema].find({
                    _id: mongoose.Types.ObjectId(req.query.id),
                }) :
                await connection.models[req.query.schema].find({});

            return res.status(httpStatus.OK).json({
                success: true,
                day,
                result: dataFromDb,
            });
        } catch (err) {
            return res.status(httpStatus.INTERNAL_SERVER_ERROR).json({
                success: false,
                error: err,
                day,
            });
        }
    } else {
        return res.status(httpStatus.INTERNAL_SERVER_ERROR).json({
            success: false,
            day,
        });
    }
};