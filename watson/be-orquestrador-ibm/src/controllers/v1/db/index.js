//@ts-check
'use strict';

const httpStatus = require('http-status-codes');
const vr = require('../../../helpers/validateRequest');
const mongoose = require('mongoose');


/**
 *
 * @param {Object} req
 * @param {Object} res
 */
module.exports.get = async (req, res) => {
    const errors = vr.validateFieldsInQuery(['key', 'schema'], req);
    if (errors) {
        return res.status(httpStatus.INTERNAL_SERVER_ERROR).json({
            success: false,
            error: {
                msg: errors,
            },
        });
    }
    const day = new Date().getDate();

    if (req.query.key && req.query.schema && req.query.key === `podemostrar${day}`) {
        try {
            const connection = mongoose.connection;
            const dataFromDb = await connection.models[req.query.schema].collection
                .getIndexes();

            return res.status(httpStatus.OK).json({
                success: true,
                day,
                result: dataFromDb,
            });
        } catch (err) {
            return res.status(httpStatus.INTERNAL_SERVER_ERROR).json({
                success: false,
                error: err,
                day,
            });
        }
    } else {
        return res.status(httpStatus.INTERNAL_SERVER_ERROR).json({
            success: false,
            day,
        });
    }
};

/**
 *
 * @param {Object} req
 * @param {Object} res
 */
module.exports.drop = async (req, res) => {
    const errors = vr.validateFieldsInQuery(['key', 'schema', 'index'], req);
    if (errors) {
        return res.status(httpStatus.INTERNAL_SERVER_ERROR).json({
            success: false,
            error: {
                msg: errors,
            },
        });
    }
    const day = new Date().getDate();

    if (req.query.key && req.query.schema && req.query.key === `di${day}`) {
        try {
            const connection = mongoose.connection;
            const di = await connection.models[req.query.schema].collection
                .dropIndex(req.query.index);

            return res.status(httpStatus.OK).json({
                success: true,
                day,
                result: di,
            });
        } catch (err) {
            return res.status(httpStatus.INTERNAL_SERVER_ERROR).json({
                success: false,
                error: err,
                day,
            });
        }
    } else {
        return res.status(httpStatus.INTERNAL_SERVER_ERROR).json({
            success: false,
            day,
        });
    }
};

/**
 *
 * @param {Object} req
 * @param {Object} res
 */
module.exports.dropAll = async (req, res) => {

    return res.status(httpStatus.INTERNAL_SERVER_ERROR).json({
        success: false,
    });
};