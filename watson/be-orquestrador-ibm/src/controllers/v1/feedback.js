'use strict';
//@ts-check

const mongoose = require('mongoose');
const status = require('http-status-codes');

/**
 * Recebe um post e salva ele no histórico
 *
 * @param req Objeto request do express
 * @param {import('express').Response} res
 * @returns void
 */
async function saveFeedback(req, res) {
    if (!req.body) {
        res.status(status.BAD_REQUEST).json({
            status: 'error',
            message: 'Request sem corpo',
        });
        return;
    }
    const payload = {
        // Injetado pelo middleware veja 'externalTokenHeader.js'
        workspace_id: req.locals.wdsCredentials.workspace_id,
        conversation_id: req.body.conversation_id,
        success: req.body.info,
        intents: req.body.intencao,
        filtroObj: req.body.filtroObj,
        username: '', // Usava o session na oficina, não existe mais
        userReviewed: '',
    };
    const historicoColl = mongoose.connection.collection('feedback');
    try {
        await historicoColl.insertOne(payload);
    } catch (err) {
        console.error('Ocorreu um erro ao inserir o feedback na base!');
        console.error(err);
        res.status(status.INTERNAL_SERVER_ERROR).json({
            status: 'error',
            message: 'Erro interno.',
        });
    }
    res.json({
        status: 'ok',
        message: 'Feedback salvo com sucesso',
    });
}

module.exports = {
    saveFeedback,
};
