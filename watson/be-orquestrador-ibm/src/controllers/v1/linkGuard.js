//@ts-check
'use strict';

const httpStatus = require('http-status-codes');
const request = require('request-promise-native');
const { UAParser } = require('ua-parser-js');
const { from } = require('rxjs');
const { retry, flatMap, filter } = require('rxjs/operators');
const chatbaseRepo = require('../../repositories/chatbase');

const parser = new UAParser();

/**
 * @param {import('express').Request} req
 * @param {import('express').Response} res
 * @returns {Promise<import('express').Response>}
 */
module.exports.linkGuard = async function (req, res) {
    const { target, user, cid } = req.query;
    if (!target) {
        return res.status(httpStatus.BAD_REQUEST).send({
            status: 'error',
            message: 'Target missing',
        });
    }
    res.redirect(target);

    parser.setUA(req.get('user-agent'));
    from(chatbaseRepo.readChatbaseKey(cid)).pipe(
        filter(x => !!x),
        flatMap((chatbaseKey) => from(request({
            url: 'https://chatbase.com/api/click',
            method: 'POST',
            json: true,
            body: {
                api_key: chatbaseKey,
                url: target,
                platform: parser.getDevice().model ? 'Mobile' : 'Web',
                user_id: user,
            },
        }))),
        retry(3),
    ).subscribe(null, (err) => {
        console.error('Não foi possível enviar link tracking para o chatbase!');
        console.error(err);
    });
};
