'use strict';
//@ts-check

const mongoose = require('mongoose');
const status = require('http-status-codes');
const logger = require('../../helpers/logger');

/**
 * Recebe um post e salva ele no histórico
 *
 * @param req
 * @param res
 * @returns void
 */
async function saveHistorico(req, res) {
    if (!req.body) {
        res.status(status.BAD_REQUEST).json({
            status: 'error',
            message: 'Request sem corpo',
        });
        return;
    }

    const payload = {
        oid: req.locals.wdsCredentials._id.toString(),
        workspace_id: req.locals.wdsCredentials.workspace_id,
        conversation_id: req.body.conversation_id,
        text: req.body.mensagem,
        username: req.body.user,
        filtroObj: req.body.filtroObj,
        intents: req.body.intents || [],
        entities: req.body.entities || [],
        typeAgent: req.body.tipo,
        info: req.body.info,
        createdAt: new Date(),
        updatedAt: new Date(),
    };
    const historicoColl = mongoose.connection.collection('historico');
    try {
        await historicoColl.insertOne(payload);
    } catch (err) {
        logger.error('Ocorreu um erro ao inserir o historico na base!', { err, payload });
        res.status(status.INTERNAL_SERVER_ERROR).json({
            status: 'error',
            message: 'Erro interno.',
        });
    }
    res.json({
        status: 'ok',
        message: 'Historico salvo com sucesso',
    });
}

module.exports = {
    saveHistorico,
};
