'use strict';

const promiseRetryOptions = { retries: 3 };
const promiseRetry = require('promise-retry');
const utils = require('../../../helpers/utils');
const util = require('util');
const httpStatus = require('http-status-codes');
const voiceHelper = require('../../../helpers/voice');
const wa = require('../../../helpers/WCSInstance');
const chatRepository = require('../../../repositories/chat');
const redis = require('../../../helpers/connectRedis').client;
const location = require('../../../repositories/prestadores');
const { loadCredentials, filterTokenList } = require('../../../repositories/watsonData');
const WAIT_TIME = 2000;

const redisSetPromise = util.promisify(redis.set.bind(redis));

async function voice(req, res) {
    /**
     * @type {any}
     */
    let context = {};
    const text = req.body.input;
    const ivrData = req.body.ivrData;
    const uuid = ivrData.uuid;


    console.log('Input CPQD: ' + text);

    // verifica se está buscando resultado assíncronos
    if (await isWaitingApi(uuid)) {
        res.json({
            output: '',
            CallSid: req.body.CallSid,
            customerId: req.body.customerId,
            status: '1',
            ivrData: req.body.ivrData,
            waitTime: WAIT_TIME,
        });
        return;
    }

    // sai do loop de espera, prosseguindo com a conversa
    if (text === '<WAITING>') {
        res.json({
            output: '',
            CallSid: req.body.CallSid,
            customerId: req.body.customerId,
            status: '1',
            ivrData: req.body.ivrData,
        });
        return;
    }

    try {
        const DEFAULT_CONTEXT = `{"usuario": "${ivrData.caller_id_number}", "canal": "voz"}`;
        // busca UUID no Redis
        context = JSON.parse(
            await promiseRetry((retry) => chatRepository.findCallSidInRedis(uuid).catch(retry),
                promiseRetryOptions) || DEFAULT_CONTEXT);
    } catch (err) {
        return res.status(httpStatus.BAD_REQUEST).send({
            error: err,
            msg: 'Redis connection failed.',
        });
    }

    /** @type {any[]} */
    const watsonCredentialsList = await loadCredentials(req.query.externaltoken);
    const watsonCredentials = await filterTokenList(watsonCredentialsList,
        context.conversation_id);

    const request = {
        oid: watsonCredentials._id.toString(),
        input: utils.normalizeInput(text),
        context: context,
        workspace_id: watsonCredentials.workspace_id,
        username: watsonCredentials.username,
        password: watsonCredentials.password,
    };

    const watsonResponse = await wa.sendMessage(request);

    const reqObj = {
        input: {
            text,
        },
        context: {
            conversation_id: watsonResponse.context.conversation_id,
            ...context,
        },
    };

    // grava request no banco
    if (text !== '') {
        chatRepository.logRequestMessage(reqObj, request.workspace_id, request.oid);
    }

    // grava response no banco
    chatRepository.logResponseMessage(watsonResponse, request.workspace_id, request.oid);

    if ('busca_especialidade' in watsonResponse.output) {
        res.json({
            output: watsonResponse.output.text.join('. '),
            CallSid: req.body.CallSid,
            customerId: req.body.customerId,
            status: '1',
            ivrData: req.body.ivrData,
            waitTime: WAIT_TIME,
        });

        const lat = watsonResponse.context.lat;
        const long = watsonResponse.context.long;
        let specialists = [];

        await redisSetPromise('searching_' + uuid, 'yes');
        redis.expire('searching_' + uuid, (WAIT_TIME + 10000) / 1000);

        try {
            /** @type {any[]} */
            specialists = (await location.getSpecialists(
                watsonResponse.context.cod_especialidades, lat, long, '', ''));
            specialists = specialists.slice(0, 3);
        } catch (err) {
            //TODO: adicionar flag no contexto para o caso da chamada não responder 
            //(e no Watson, adicionar mensagem com erro)
            watsonResponse.context.api_error = true;
        }

        watsonResponse.context.lista_endereco = voiceHelper.specialistsToVoice(specialists);
        await redisSetPromise(uuid, JSON.stringify(watsonResponse.context));
        redis.expire(uuid, 3 * 60);
        redis.del('searching_' + uuid);

        return;
    }

    // salva CallSid no Redis
    await redisSetPromise(uuid, JSON.stringify(watsonResponse.context), 'EX', 3 * 60);

    const resp = {
        output: watsonResponse.output.text.join('. '),
        CallSid: req.body.CallSid,
        customerId: req.body.customerId,
        status: 'end_flow' in watsonResponse.output ? '1' : '0',
        ivrData: req.body.ivrData,
        waitTime: null, // null?
    };

    res.json(resp);
}

async function isWaitingApi(uuid) {
    const result = await utils.redisGetPromise('searching_' + uuid);

    if (result) {
        redis.expire('searching_' + uuid, (WAIT_TIME + 10000) / 1000);
    }

    return !!result;
}

module.exports = { voice };