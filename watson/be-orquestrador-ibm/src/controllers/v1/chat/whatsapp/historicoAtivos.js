'use strict';

const WhatsAppAtivos = require('../../../../repositories/historico');
const httpStatus = require('http-status-codes');
const logger = require('../../../../helpers/logger');
const timestamp = require('../../../../helpers/yupTimestamp');
const boom = require('http-errors');
const yup = require('yup');

async function findConversa(req, res) {
    try {
        const result = await WhatsAppAtivos.listById(req.params.case_id);
        res.status(httpStatus.OK).json(result);
    } catch (err) {
        if (err instanceof boom.HttpError && err.expose) {
            res.status(err.statusCode).send({
                status: 'ERR',
                message: err.message,
            });
            return;
        }
        logger.error('Erro ao buscar campanhas por CaseId', err);
        res.status(httpStatus.INTERNAL_SERVER_ERROR).json({
            status: 'error',
            message: 'Erro buscar histórico de ativos',
        });
    }
}

const querySchema = yup.object({
    phoneNumber: yup.string().matches(/^\d{12,13}$/, 'Não é um telefone válido!'),
    startDate: timestamp,
    endDate: timestamp,
}).noUnknown().notOneOf([{}]);

async function searchActives(req, res) {
    try {
        const query = await querySchema.validate(req.query);
        const results = await WhatsAppAtivos.listByPhoneAndPeriod(query);
        results.map(x => {
            x.dadosAtivo = JSON.parse(x.text);
            delete x.text;
        });
        res.status(httpStatus.OK).json({ results });
    } catch (err) {
        logger.error('Erro ao listar por telefone e período', { err });
        res.status(httpStatus.INTERNAL_SERVER_ERROR).json({
            status: 'error',
            message: 'Erro buscar histórico de ativos',
        });
    }
}

module.exports = {
    findConversa,
    searchActives,
};