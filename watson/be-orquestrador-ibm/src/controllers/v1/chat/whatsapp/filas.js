'use strict';

const Bull = require('bull');
const { URL } = require('url');
const logger = require('../../../../helpers/logger');

const CAMPOS_CONTEXTO = ['phoneNumber', 'nome', 'documento'];
// 3 Minutos! -- TODO: Configurável.
const tempoSessaoMin = process.env.TEMPO_SESSAO_MIN;
const TEMPO_SESSAO = Number.parseFloat(tempoSessaoMin) * 60 * 1000;
const CHAVE24HORAS = 'sessaoWhats24hrs';
const SECS24HORAS = 24 * 60 * 60;
const TIMEOUT_MILLIS_WHATSAPP = 60000;

const redisURL = new URL(process.env.REDIS);
const redisConf = {
    host: redisURL.hostname,
    port: Number.parseInt(redisURL.port, 10),
    db: 0,
    password: redisURL.password,
};
if (process.env.NODE_ENV !== 'local') {
    redisConf.tls = {
        servername: redisURL.hostname,
    };
}

const filaInatividade = new Bull('whatsAppSessions', process.env.REDIS, {
    redis: redisConf,
    defaultJobOptions: {
        attempts: 2,
        removeOnComplete: true,
        removeOnFail: true,
        delay: TEMPO_SESSAO,
        backoff: {
            type: 'fixed',
            delay: TEMPO_SESSAO,
        },
    },
});

const mensagens = new Bull('whatsAppMessages', process.env.REDIS, {
    redis: redisConf,
    defaultJobOptions: {
        attempts: 1,
        removeOnComplete: true,
        removeOnFail: true,
    },
});

const salesForce = new Bull('whatsAppSalesForce', process.env.REDIS, {
    redis: redisConf,
    defaultJobOptions: {
        attempts: 5,
        removeOnComplete: true,
        removeOnFail: true,
    },
});

const currentStack = new Error().stack;
async function event(signal) {
    try {
        await filaInatividade.close();
        await salesForce.close();
        await mensagens.close();
        logger.debug('Saida limpa bull.');
    } catch (err) {
        logger.error('Erro no cleanup!!!', { stackTrace: currentStack });
    }
    process.kill(process.pid, signal);
}
process.once('SIGINT', event);
process.once('SIGTERM', event);

salesForce.on('failed', (job, err) => {
    logger.warn('Erro desconhecido na thread do salesforce!', { job, err });
});

module.exports.mensagens = mensagens;
module.exports.filaInatividade = filaInatividade;
module.exports.salesForce = salesForce;
module.exports.TEMPO_SESSAO = TEMPO_SESSAO;
module.exports.CAMPOS_CONTEXTO = CAMPOS_CONTEXTO;
module.exports.CHAVE24HORAS = CHAVE24HORAS;
module.exports.TIMEOUT_MILLIS_WHATSAPP = TIMEOUT_MILLIS_WHATSAPP;
module.exports.SECS24HORAS = SECS24HORAS;
