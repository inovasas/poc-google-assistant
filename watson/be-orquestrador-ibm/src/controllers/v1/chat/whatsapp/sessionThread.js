'use strict';

const logger = require('../../../../helpers/logger');
const { update } = require('../../../../helpers/connectRedis');
const filas = require('./filas');
const WhatsAppUser = require('./whatsappUser');
const wavy = require('../../../../repositories/wavyWhatsapp');
const _ = require('lodash');

/**
 * @typedef WhatsSessionData
 * @property {any} context
 * @property {any} watsonCredentials
 * @property {string} phoneNumber
 * @property {number | string} latestAction
 */

/**
 *
 * @param {import('bull').Job<WhatsSessionData>} job
 * @returns {Promise<any?>} nothing or error
 */
module.exports = async (job) => {
    const data = job.data;

    // @ts-ignore
    const progresso = await job.progress();
    const fecharSessao = 'finalizar_sessao' in data.context;
    const transferidoHumano = data.context.transferido_humano;
    if (progresso !== 50 && !fecharSessao && !transferidoHumano) {
        await wavy.sendWhatsappMsg('Hey, não me deixa aqui sozinho! ' +
            'Fiquei com dúvida na última pergunta... Me ajuda nessa?', data.phoneNumber);
        await job.progress(50);

        await job.releaseLock();
        throw new Error('Primeira tentativa.');
    }
    if (!fecharSessao && !transferidoHumano) {
        await wavy.sendWhatsappMsg(
            'Ops! Nos vemos depois então! Quando precisar de ajuda ' +
            'novamente, é só me chamar! 😉', data.phoneNumber);
    }
    const savedCTX = _.pick(data.context, filas.CAMPOS_CONTEXTO);
    await WhatsAppUser.updateOne(
        { phoneNumber: data.phoneNumber },
        { ...savedCTX },
        { upsert: true },
    ).exec();

    // Atualiza redis:
    const chaveRedis = filas.CHAVE24HORAS + data.phoneNumber;
    // Melhor um false proposital do que undefined.
    savedCTX.autenticado = data.context.autenticado || false;
    const strCtx = JSON.stringify(savedCTX);
    // Atualiza valor da chave ou a cria com 24 horas timeout
    await update(chaveRedis, strCtx, filas.SECS24HORAS);

    logger.debug('Fechando sessão do usuário ' + data.phoneNumber, {
        phoneNumber: data.phoneNumber,
        action: data.latestAction,
        workspace_id: data.watsonCredentials.workspace_id,
    });
    // @ts-ignore
    data.latestAction = Number.parseInt(data.latestAction, 10) || 9999;

    if (!transferidoHumano) {
        await filas.salesForce.add({
            action: data.latestAction,
            context: data.context,
            phoneNumber: data.phoneNumber,
        });
    }
    await job.progress('finalizado');
};
