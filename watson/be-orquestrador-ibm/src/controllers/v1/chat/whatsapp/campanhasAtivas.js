'use strict';
//@ts-check

const WhatsAppCampanhas = require('../../../../models/whatsAppCampanhas');
const httpStatus = require('http-status-codes');
const logger = require('../../../../helpers/logger');
const moment = require('moment');

async function getCampaigns(req, res) {
    let result;

    try {
        result = await WhatsAppCampanhas.find({});
    } catch (err) {
        logger.error('Erro ao buscar campanhas', err);
        res.status(httpStatus.INTERNAL_SERVER_ERROR).json({
            status: 'error',
            message: 'Erro buscar campanhas',
        });
    }

    res.status(200).json({
        status: 'OK',
        message: result,
    });
}

async function getCampaignById(req, res) {
    try {
        const result = await WhatsAppCampanhas.findById(req.params.id);
        res.status(200).json({
            status: 'OK',
            message: result,
        });
    } catch (err) {
        logger.error('Erro ao buscar campanhas', err);
        res.status(httpStatus.INTERNAL_SERVER_ERROR).json({
            status: 'error',
            message: 'Erro buscar campanhas',
        });
    }
}

async function saveCampaign(req, res) {
    const body = req.body;
    let result;

    if (!body) {
        res.status(httpStatus.BAD_REQUEST).json({
            status: 'error',
            message: 'Request sem corpo.',
        });
    }

    // TODO: YUP validation
    const payload = {
        namespace: body.namespace,
        elementName: body.elementName,
        horaMinima: body.horaMinima,
        horaMaxima: body.horaMaxima,
        diasSemana: body.diasSemana,
        feriado: body.feriado,
        prioritario: body.prioritario,
        assistantExternalToken: body.assistantExternalToken,
        ttl: body.ttl,
    };


    // valida formato de horas
    const formatHora = 'HH:mm';
    if (!moment(payload.horaMinima, formatHora).isValid() ||
        !moment(payload.horaMaxima, formatHora).isValid()) {
        res.status(httpStatus.BAD_REQUEST).json({
            status: 'error',
            message: 'Formato de horas inválido',
        });
    }

    try {
        result = await WhatsAppCampanhas.create(payload);
    } catch (err) {
        logger.error('Erro ao inserir nova campanha ativa', err);
        return res.status(httpStatus.INTERNAL_SERVER_ERROR).json({
            status: 'error',
            message: 'Erro ao gravar campanha',
        });
    }

    res.status(httpStatus.OK).json({
        status: 'OK',
        message: 'Campanha salva com sucesso',
        id: result.id,
    });
}

async function updateCampaign(req, res) {
    const body = req.body;

    if (!body) {
        res.status(httpStatus.BAD_REQUEST).json({
            status: 'error',
            message: 'Request sem corpo.',
        });
    }

    try {
        await WhatsAppCampanhas.findByIdAndUpdate(
            req.params.id,
            { $set: body },
        );

    } catch (err) {
        logger.error('Erro ao editar campanha', err);
        res.status(httpStatus.INTERNAL_SERVER_ERROR).json({
            status: 'error',
            message: 'Erro ao editar campanha',
        });
    }

    res.status(httpStatus.OK).json({
        status: 'OK',
        message: 'Campanha alterada com sucesso',
    });
}

async function deleteCampaign(req, res) {
    const body = req.body;

    if (!body) {
        res.status(httpStatus.BAD_REQUEST).json({
            status: 'error',
            message: 'Request sem corpo.',
        });
    }

    try {
        await WhatsAppCampanhas.findByIdAndDelete(req.params.id);
    } catch (err) {
        logger.error('Erro ao remover campanha ativa', err);
        res.status(httpStatus.INTERNAL_SERVER_ERROR).json({
            status: 'error',
            message: 'Erro ao remover campanha',
        });
    }

    res.status(httpStatus.OK).json({
        status: 'OK',
        message: 'Campanha removida com sucesso',
    });
}

module.exports = {
    getCampaigns,
    getCampaignById,
    saveCampaign,
    updateCampaign,
    deleteCampaign,
};