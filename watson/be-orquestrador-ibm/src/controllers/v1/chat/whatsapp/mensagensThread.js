'use strict';

const { loadCredentials } = require('../../../../repositories/watsonData');
const chatRepository = require('../../../../repositories/chat');
const chatbase = require('../../../../repositories/chatbase');
const { get, client: redis } = require('../../../../helpers/connectRedis');
const wa = require('../../../../helpers/WCSInstance');
const filas = require('./filas');
const WhatsappUser = require('./whatsappUser');
const logger = require('../../../../helpers/logger');
const _ = require('lodash');
const producerMoWhatsapp = require('../../../../repositories/ampqProducerMoWhatsappjs');
const publishWhatsResp = require('../../../../repositories/ampqProducerWhatsappResponse.js');

const { from } = require('rxjs');
const { retry } = require('rxjs/operators');

/**
 * @typedef WhatsAppLocation
 * @property {string} name
 * @property {string} address
 * @property {string} geoPoint
 */

/**
 * @typedef WhatsAppTextMsg
 * @property {string} type
 * @property {string} messageText
 * @property {WhatsAppLocation?} location
 */

/**
 * @typedef WhatsSessionData
 * @property {any} context
 * @property {any} watsonCredentials
 * @property {string} phoneNumber
 * @property {number} latestAction
 */

/**
 * @typedef WhatsAppMsg
 * @property {string} id
 * @property {string} source
 * @property {string} origin
 * @property {any} userProfile // TODO
 * @property {string} correlationId
 * @property {string} campaignId
 * @property {string} campaignAlias
 * @property {number?} second_time
 * @property {WhatsAppTextMsg} message
 *
 */

/**
 * Verifica se x é uma array e se tem elementos
 * @param {any} x valor a ser checado
 * @returns {boolean} É uma array com elementos?
 */
function usefulArray(x) {
    return _.isArray(x) && !_.isEmpty(x);
}

async function newHSMSession(msg) {
    const key = 'whatsappAtivo:' + msg.source;
    const sessionString = await get(key);
    if (!sessionString) {
        return null;
    }
    const session = JSON.parse(sessionString);
    const { wksConfig: wdsCredential, context } = session;
    logger.debug(`Abrindo sessão de resposta a HSM pro número ${msg.source}`, {
        workspace_id: wdsCredential.workspace_id,
    });
    redis.del(key);
    return {
        context: context || {},
        watsonCredentials: wdsCredential,
    };
}

async function getRandomWkspc() {
    const credentials = await loadCredentials('2467bdf0-9c0c-11e9-a1c2-19c83f192285');
    return credentials[Math.floor(Math.random() * credentials.length)];
}

/**
 * Constroi todo payload de uma sessão, dados de autenticação watson
 * 
 * @param {WhatsAppMsg} msg mensagem inicial
 */
async function passiveSessionContext(msg) {

    // Recupera o cache de 24horas do redis
    const ctx = JSON.parse(await get(filas.CHAVE24HORAS + msg.source) || 'null') ||
        await WhatsappUser.findOne({ phoneNumber: msg.source }).lean().exec();
    logger.debug('Carregando dados de whatsapp para o número ' + msg.source);
    return ctx || {};
}

async function buildJobData(msg) {
    const ctxDefault = { phoneNumber: msg.source, canal: 'whatsApp' };

    /** @type {any} */
    const HSMData = await newHSMSession(msg) || {};
    const context = Object.assign(ctxDefault, await passiveSessionContext(msg),
        HSMData.context);

    return {
        context,
        phoneNumber: msg.source,
        latestAction: 9999,
        watsonCredentials: HSMData.watsonCredentials || await getRandomWkspc(),
    };
}

/**
 * 
 * @param {any} whatsMsg
 * @returns {Promise<any?>} nothing or error
 */
module.exports = async function (whatsMsg) {
    const { location } = whatsMsg.message;
    if (whatsMsg.message.type !== 'TEXT' && !location) {
        // TODO: Menssagem configurável!
        const unsupported = 'Desculpe, por enquanto só estou preparado para responder texto!';
        publishWhatsResp.publish({
            message: {
                response_type: 'text',
                text: unsupported,
            },
            'destinations': whatsMsg.source,
        });
        return;
    }
    let sessionJob = await filas.filaInatividade.getJob(whatsMsg.source);
    /** @type {WhatsSessionData} */
    let sessionData;
    let goAgain = false;
    try {
        if (sessionJob == null || _.isEmpty(sessionJob.data)) {
            sessionData = await buildJobData(whatsMsg);
            logger.debug(`Sessão aberta pro número ${whatsMsg.source}.`, { sessionData });
            sessionJob = await filas.filaInatividade.add(sessionData, {
                jobId: whatsMsg.source,
                attempts: 2,
                removeOnComplete: true,
                removeOnFail: true,
                delay: filas.TEMPO_SESSAO,
                backoff: {
                    type: 'fixed',
                    delay: filas.TEMPO_SESSAO,
                },
            });
        } else {
            // @ts-ignore
            await sessionJob.moveToDelayed(Date.now() + filas.TEMPO_SESSAO, true);
            sessionData = sessionJob.data;
        }
        await sessionJob.takeLock();
        const { context, watsonCredentials } = sessionData;

        if (location) {
            whatsMsg.message.messageText = location.geoPoint;
            const [latitude, longitude] = location.geoPoint.split(',');
            context.latitude = latitude;
            context.longitude = longitude;
        }
        context.usuario = sessionData.phoneNumber;

        const wcsReq = {
            oid: watsonCredentials._id.toString(),
            input: whatsMsg.message.messageText,
            context,
            workspace_id: watsonCredentials.workspace_id,
            username: watsonCredentials.username,
            password: watsonCredentials.password,
        };

        const wcsMsg = await from(wa.sendMessage(wcsReq)).pipe(retry(2)).toPromise();
        if (usefulArray(wcsMsg.output.log_messages)) {
            wcsMsg.output.log_messages.forEach((msg) => {
                logger.warn('Watson error log message.', { msg });
            });
        }

        // adiciona conversation_id na primeira mensagem do usuário
        if (!context.conversation_id) {
            context.conversation_id = wcsMsg.context.conversation_id;
        }

        // grava request no banco
        if (wcsReq.input !== '' && !whatsMsg.second_time) {
            chatRepository.logRequestMessage({
                input: {
                    text: wcsReq.input,
                },
                context,
            }, wcsReq.workspace_id, wcsReq.oid);
        }

        // grava response no banco
        const saved = await chatRepository.logResponseMessage(wcsMsg, wcsReq.workspace_id,
            wcsReq.oid);
        chatbase.logToChatbase(wcsReq, wcsMsg, watsonCredentials);

        if (whatsMsg.second_time > 5 && 'go_again' in wcsMsg.output) {
            logger.error('Loop no chat, várias mensagens caem no mesmo nó.', {
                workspace_id: watsonCredentials.workspace_id,
                context: wcsMsg.context,
            });
        } else if ('go_again' in wcsMsg.output) {
            whatsMsg.message.messageText = '';
            whatsMsg.second_time = (whatsMsg.second_time || 0) + 1;
            goAgain = true;
        }

        for (const generic of wcsMsg.output.generic) {
            publishWhatsResp.publish({
                'message': generic,
                'destinations': whatsMsg.source,
                'historico_id': saved.insertedId.toHexString(),
                'msg_delay': wcsMsg.output.msg_delay,
            });
        }

        sessionData.latestAction = wcsMsg.output.action_number || sessionData.latestAction;
        sessionData.context = wcsMsg.context;
    } catch (error) {
        logger.error('Erro desconhecido na thread do whatsapp', {
            error, sessionData, sessionJob,
        });
        const MSG_PADRAO = 'Desculpe, ocorreu um erro em nosso sistema. ' +
            'Por favor, tente novamente mais tarde';
        const txtMsg = (error && error.msg) || MSG_PADRAO;

        publishWhatsResp.publish({
            message: {
                response_type: 'text',
                text: txtMsg,
            },
            destinations: whatsMsg.source,
        });
    } finally {
        await sessionJob.update(sessionData);
        await sessionJob.progress(0);
    }

    try {
        await sessionJob.releaseLock();
        // Whats App Mobile Originated
        const key = `wamo:${whatsMsg.source}`;
        redis.del(key, async () => {
            if ('finalizar_sessao' in sessionData.context) {
                await sessionJob.promote();
            } else if (goAgain) {
                producerMoWhatsapp.publish(whatsMsg);
            }
        });
    } catch (error) {
        logger.warn('Falha ao liberar trava do job sessão whatsapp',
            { error, sessionData, sessionJob });
    }
};
