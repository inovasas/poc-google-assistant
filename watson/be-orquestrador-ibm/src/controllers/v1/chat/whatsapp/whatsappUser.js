'use strict';

const mongoose = require('mongoose');

const whatsappUserSchema = new mongoose.Schema({
    phoneNumber: {
        type: String,
        index: true,
        unique: true,
    },
    nome: String,
    documento: String,
});
const WhatsappUser = mongoose.model('WhatsappUser', whatsappUserSchema);

module.exports = WhatsappUser;
