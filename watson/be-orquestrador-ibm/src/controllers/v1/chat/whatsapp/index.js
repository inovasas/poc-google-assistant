'use strict';

const filas = require('./filas');
const mensagensThread = require('./mensagensThread');
const sessionThread = require('./sessionThread');
const salesForceThread = require('./salesForceThread');
const actions = require('../../../../repositories/actionHandlers');
const validateContext = require('../../../../helpers/validateContext');
require('../../../../repositories/ampqConsumeMoWhatsapp');

validateContext.posWatsonReducers.action_number = actions;

// Para inicializar as transformações de contexto
require('../../../../repositories/mdmApis');
require('../../../../repositories/identificacaoPositiva');

filas.mensagens.process(10, mensagensThread);
filas.filaInatividade.process(10, sessionThread);
filas.salesForce.process(10, salesForceThread);
