'use strict';

// const filas = require('./filas');
const mongoose = require('mongoose');
const logger = require('../../../../helpers/logger');
const { atualizaCaso, gerarManifestacao } = require('../../../../repositories/salesforce');
const chat = require('../../../../repositories/chat');
const { from } = require('rxjs');
const { retry } = require('rxjs/operators');
const _ = require('lodash');

/**
 * @typedef WhatsSessionData
 * @property {number} action
 * @property {any} context
 * @property {string} phoneNumber
 */


/**
 * Busca payload de determinada action
 *
 * @param {Number} action
 * @param {String} planoBU
 * @returns {Promise<any>} actionPayload
 */
async function getActionPayload(action, planoBU) {
    const actionPayload = await mongoose.connection.db.collection('actionPayload')
        .findOne({ 'action': action, 'planoBU': planoBU });
    return actionPayload;
}

/**
 *
 * @param {import('bull').Job<WhatsSessionData>} job
 * @returns {Promise<any?>} nothing or error
 */
module.exports = async function (job) {
    const salesForceJob = job.data;
    const { context: ctx } = salesForceJob;

    logger.debug('salesforceThread...', { salesForceJob });

    if ('cuidado_coordenado' in ctx) {
        logger.debug('Atualização de caso SalesForce...', { ctx });
        atualizaCaso(ctx.conversation_id, 'Interação Parcial');
        return;
    } else if ('cuidado_coordenado_finalizado' in ctx) {
        return;
    }

    const actionPayload = await from(getActionPayload(salesForceJob.action,
        _.get(salesForceJob, ['context', 'produtos_mdm', ctx.produto_selecionado, 'planoBU']),
    )).pipe(retry(2)).toPromise();

    if (_.isEmpty(actionPayload)) {
        logger.warn(`Action ${salesForceJob.action} não foi encontrada!`, { salesForceJob });
        return;
    }

    const manifestacaoObj = {
        nomeDaPessoaC: ctx.nome,
        categoriaC: actionPayload.categoria,
        type: actionPayload.type,
        tipoC: actionPayload.tipo,
        subject: actionPayload.subject,
        description: (await chat.getAllHistory(ctx.conversation_id)),
        status: actionPayload.status,
        motivoStatus: actionPayload.motivoStatus,
        origin: actionPayload.origin,
        formaDeContatoC: actionPayload.contato,
        IdSessionC: ctx.conversation_id,
        phoneNumber: salesForceJob.phoneNumber,
    };

    const userId = _.get(salesForceJob, ['context', 'produto_atual', 'carteirinha',
        'codigo-beneficiario']) || ctx.documento || null;

    try {
        const manifestacao = await gerarManifestacao(
            actionPayload.recordTypeName,
            actionPayload.subtipo,
            actionPayload.areaNegocio,
            userId,
            manifestacaoObj,
        );
        logger.info(`Manifestação criada para o numero ${ctx.phoneNumber}`, {
            ..._.pick(actionPayload, ['recordTypeName', 'subtipo', 'areaNegocio']),
            action: salesForceJob.action,
            manifestacao,
            cpf: ctx.documento,
        });
    } catch (err) {
        console.error(err);
    }
};
