'use strict';

const httpStatus = require('http-status-codes');
const chatRepository = require('../../../repositories/chat');
const wa = require('../../../helpers/WCSInstance');
const wds = require('../../../helpers/WDSInstance');
const utils = require('../../../helpers/utils');
const chatbase = require('../../../repositories/chatbase');
const { from } = require('rxjs');
const { retry } = require('rxjs/operators');
const logger = require('../../../helpers/logger');

const URL = require('url').URL;

/**
 *
 * @param req objeto de request do express
 * @param {import('express').Response} res
 */
async function conversation(req, res) {
    try {
        // Injetado pelo middleware veja 'externalTokenHeader.js'
        const obj = req.locals.wdsCredentials;

        const bd = {
            oid: obj._id.toString(),
            input: utils.normalizeInput(req.body.input),
            context: req.body.context,
            workspace_id: obj.workspace_id,
            username: obj.username,
            password: obj.password,
        };

        let message;
        try {
            message = await from(wa.sendMessage(bd)).pipe(retry(2)).toPromise();
        } catch (err) {
            return res.status(httpStatus.INTERNAL_SERVER_ERROR).send({
                error: err,
                msg: 'Watson connection failed.',
            });
        }

        // FIXME: POC ONLY
        if ('discovery' in message.output) {
            try {
                const result = (await wds.sendWDSquery(
                    obj.discovery_collection_id,
                    obj.discovery_environment_id,
                    bd.input));

                bd.context.wds_success = result.length > 0;
                bd.context.wds_success = result[0].result_metadata.confidence >
                    (obj.discovery_threshold / 100);

                // FIXME: two watson round trips
                message = await from(wa.sendMessage(bd)).pipe(retry(2)).toPromise();
                message.output.wds_response = result;

                delete message.context.wds_success;
            } catch (err) {
                message.context.wds_success = false;
                logger.error('Erro na chamada para Watson Discovery!');
            }
        }
        // FIXME: END OF POC CODE =====================================

        // grava response no banco
        if (obj.save_history) {
            const oid = req.locals.wdsCredentials._id.toString();

            chatRepository.logRequestMessage(req.body, bd.workspace_id, oid);
            chatRepository.logResponseMessage(message, bd.workspace_id, oid);
        }


        try {
            utils.applyLinkGuard(new URL(req.protocol + '://' + req.get('host')), message);
        } catch (err) {
            logger.warn('falha url', req.protocol + '://' + req.get('host'));
        }

        res.status(httpStatus.OK).send(message);

        chatbase.logToChatbase(bd, message, obj).catch((error) =>
            logger.error('Erro ao enviar mensagem pro chatbase', { error }));
    } catch (error) {
        logger.error('Unhandled Exception on Conversation Controller', { error });
        return res.status(error.code || 500).send(error.msg || '');
    }
}

module.exports = { conversation };
