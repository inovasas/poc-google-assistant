/**
 * Constrói um intent para pedir uma localização
 *
 * @param {string} permissionContext Para que o que serve a permissão
 * @param {string} requestPrompt A justificativa da permissão
 */
function buildLocationIntent(permissionContext, requestPrompt) {
    richResponse.items.push(
      
        {
          "simpleResponse": {
            "textToSpeech": "Here's an example of a browsing carousel.",
            "displayText": "Here's a simple response. Which response would you like to see next?"
          }
        },
        
      );
    return {
        "intent": "actions.intent.OPTION",
        "inputValueData": {
          "@type": "type.googleapis.com/google.actions.v2.OptionValueSpec",
          "listSelect": {
            "title": "List Title",
            "items": [
              {
                "optionInfo": {
                  "key": "SELECTION_KEY_ONE",
                  "synonyms": [
                    "synonym 1",
                    "synonym 2",
                    "synonym 3"
                  ]
                },
                "description": "This is a description of a list item.",
                "image": {
                  "url": "https://storage.googleapis.com/actionsresources/logo_assistant_2x_64dp.png",
                  "accessibilityText": "Image alternate text"
                },
                "title": "Title of First List Item"
              },
              {
                "optionInfo": {
                  "key": "SELECTION_KEY_GOOGLE_HOME",
                  "synonyms": [
                    "Google Home Assistant",
                    "Assistant on the Google Home"
                  ]
                },
                "description": "Google Home is a voice-activated speaker powered by the Google Assistant.",
                "image": {
                  "url": "https://storage.googleapis.com/actionsresources/logo_assistant_2x_64dp.png",
                  "accessibilityText": "Google Home"
                },
                "title": "Google Home"
              },
              {
                "optionInfo": {
                  "key": "SELECTION_KEY_GOOGLE_PIXEL",
                  "synonyms": [
                    "Google Pixel XL",
                    "Pixel",
                    "Pixel XL"
                  ]
                },
                "description": "Pixel. Phone by Google.",
                "image": {
                  "url": "https://storage.googleapis.com/actionsresources/logo_assistant_2x_64dp.png",
                  "accessibilityText": "Google Pixel"
                },
                "title": "Google Pixel"
              }
            ]
          }
        }
      };
}

/** @type {((string) => string)} */
const capitalize = fp.compose(_.startCase, _.lowerCase);

function buildCarrousel(watsonResponse, richResponse) {
    richResponse.items.push(
      
      {
        "simpleResponse": {
          "textToSpeech": "Here's an example of a browsing carousel.",
          "displayText": "Here's a simple response. Which response would you like to see next?"
        }
      },
      
    );
}