//@ts-check
'use strict';

const promiseRetry = require('promise-retry');
const url = require('url');
const util = require('util');
const wa = require('../../../helpers/WCSInstance');
const redis = require('../../../helpers/connectRedis').client;
const utils = require('../../../helpers/utils');
const httpStatus = require('http-status-codes');
const twillio = require('twilio');
const VoiceResponse = twillio.twiml.VoiceResponse;
const sms = twillio(process.env.TWILIO_SMS_ACOUNT_SID, process.env.TWILIO_SMS_TOKEN);
const tel = twillio(process.env.TWILIO_CALL_ACOUNT_SID, process.env.TWILIO_CALL_TOKEN);
const chatRepository = require('../../../repositories/chat');
const { loadCredentials, filterTokenList } = require('../../../repositories/watsonData');
const location = require('../../../repositories/prestadores');
const promiseRetryOptions = { retries: 3 };
const googleAssistant = require('./googleAssistant');
const conversation = require('./conversation');
const voice = require('./voice');
const voiceHelper = require('../../../helpers/voice');
const _ = require('lodash');

require('./whatsapp');

// FIXME: Twillio code from here on out

/**
 * 
 * @param {any} req
 * @param {any} res
 */
module.exports.twilio = async (req, res) => {
    const twiml = new VoiceResponse();

    /**
     * @type {any}
     */
    let context = {};
    let text = '';
    const callSid = req.body.CallSid;
    try {
        // busca CallSid no Redis
        context = JSON.parse(
            await promiseRetry(
                (retry) => chatRepository.findCallSidInRedis(callSid).catch(retry),
                promiseRetryOptions) || '{ "channel": "telephone" }');
    } catch (err) {
        twiml.say('Nosso serviço encontra-se indisponivel no momento, pedimos ' +
            'desculpa pela incoveniencia.');
        twiml.hangup();
        res.type('text/xml').send(twiml.toString());
        return res.status(httpStatus.BAD_REQUEST).send({
            error: err,
            msg: 'Redis connection failed.',
        });
    }

    /** @type {any[]} */
    const watsonCredentialsList = await loadCredentials(req.query.externaltoken);
    const watsonCredentials = await filterTokenList(watsonCredentialsList,
        context.conversation_id);

    if (req.body.CallStatus === 'ringing') {
        context = {
            usuario: req.body.Caller,
        };
    } else {
        text = req.body.SpeechResult ||
            await util.promisify(redis.get.bind(redis))('msg' + callSid);
    }

    const bd = {
        oid: watsonCredentials._id.toString(),
        input: text,
        context: context,
        workspace_id: watsonCredentials.workspace_id,
        username: watsonCredentials.username,
        password: watsonCredentials.password,
    };

    let watsonResponse;
    try {
        watsonResponse = await wa.sendMessage(bd);
    } catch (err) {
        twiml.say('Nosso serviço encontra-se indisponível no momento, pedimos ' +
            'desculpa pela incoveniência.');
        twiml.hangup();
        res.type('text/xml').send(twiml.toString());
        console.error('Ocorreu um erro no send message do twilio, alguma integração' +
            ' ou a conexão com o watson está fora do ar!', err);
        return;
    }

    if ('busca_especialidade' in watsonResponse.output) {
        const respProcurando = new VoiceResponse();
        respProcurando.say(watsonResponse.output.text.join('. '));
        respProcurando.pause({
            length: 15,
        });
        respProcurando.say('Infelizmente, nosso sistema está fora do ar, e não ' +
            'consegui acessar a lista de especialidades. A SulAmérica agradece ' +
            'a sua ligação.');
        res.type('text/xml').send(respProcurando.toString());

        const fullUrl = url.parse(req.protocol + '://' + req.get('host') + req.originalUrl);

        const lat = watsonResponse.context.lat;
        const long = watsonResponse.context.long;
        const timeStart = new Date().getTime();
        let specialists;

        try {
            /** @type {any[]} */
            specialists = (await location.getSpecialists(
                watsonResponse.context.cod_especialidades, lat, long, '', ''));
            specialists = specialists.slice(0, 3);
        } catch (err) {
            fullUrl.pathname += '/error';
        }

        watsonResponse.context.lista_endereco = voiceHelper.specialistsToVoice(specialists);
        redis.set(callSid, JSON.stringify(watsonResponse.context), 'EX', 180);
        redis.set('msg' + callSid, text, 'EX', 5);
        const delta = new Date().getTime() - timeStart;
        setTimeout(async () => await tel.calls(callSid).update({
            method: 'POST',
            url: fullUrl.href,
        }), 4000 - delta); // 4000 é o tempo pra falar a frase de espera
        return;
    }

    const reqObj = {
        input: {
            text,
        },
        context: {
            conversation_id: watsonResponse.context.conversation_id,
            ...context,
        },
    };
    // grava request no banco
    if (!utils.isEmptyObject(context) && req.body.CallStatus !== 'ringing') {
        chatRepository.logRequestMessage(reqObj, bd.workspace_id, bd.oid);
    }

    // grava response no banco
    chatRepository.logResponseMessage(watsonResponse, bd.workspace_id, bd.oid);

    // envia sms
    if ('envia_sms' in watsonResponse.output) {
        delete context.envia_sms;
        let lista = '';
        context.lista_endereco.forEach((e, i) => {
            const telefone = _.get(e.telefones.find(x => x.tipo === 'A'), 'telefone', '');
            lista += `${i + 1}. ${e.nomeFantasia} - ${e.logradouro}, ${telefone}; \n`;
        });
        try {
            await sms.messages.create({
                from: process.env.TWILIO_SMS_FROM,
                to: req.body.Caller,
                body: `Olá! Aqui estão os resultados da busca de ${context.especialidades} ` +
                    `próximos a você: \n ${lista}`,
            });
            twiml.say('Pronto. Já te mandei um SMS com tudo o que achei.');
        } catch (err) {
            // FIXME: checar por exeções esperadas, ex. telefone fixo
        }
    }

    if ('envia_sms_prev' in watsonResponse.output) {
        delete context.envia_sms;

        try {
            const ctx = watsonResponse.context;

            await sms.messages.create({
                from: process.env.TWILIO_SMS_FROM,
                to: req.body.Caller,
                body: `Olá! Você tem ${ctx.parcelas_em_aberto.length} parcelas em aberto.` +
                    `A primeira vence em ${ctx.parcelas_em_aberto[0].vencto_parcela} no ` +
                    `valor de ${ctx.parcelas_em_aberto[0].valor_parcela}`,
            });
        } catch (err) {
            console.error('Erro SMS', err);
        }
    }

    if ('end_flow' in watsonResponse.output) {
        const respFinal = new VoiceResponse();
        respFinal.say(watsonResponse.output.text.join('. '));
        respFinal.hangup();
        res.type('text/xml').send(respFinal.toString());
        return;
    }

    // salva CallSid no Redis
    redis.set(callSid, JSON.stringify(watsonResponse.context));
    redis.expire(callSid, 180);
    let gather;
    if ('sem_interrupcao' in watsonResponse.output) {
        twiml.say('---REPLACE---');
        gather = twiml.gather({
            input: 'speech',
            language: 'pt-BR',
            speechTimeout: 'auto',
        });
    } else if ('pede_cpf_dtmf' in watsonResponse.output) {
        gather = twiml.gather({
            input: 'dtmf',
            timeout: 8,
            numDigits: 11,
        });
        gather.say('---REPLACE---');
    } else {
        gather = twiml.gather({
            input: 'speech',
            language: 'pt-BR',
            speechTimeout: 'auto',
        });
        gather.say('---REPLACE---');
    }
    Array.from({ length: 5 }, () => {
        gather.pause({ length: 6 });
        gather.say({
            voice: 'Polly.Vitoria',
            language: 'pt-BR',
        }, 'Ainda não consegui te ouvir. Você pode repetir sua resposta por favor?');
    });
    gather.pause({ length: 3 });
    res.type('text/xml').send(twiml.toString().replace('---REPLACE---', `
        <amazon:auto-breaths>
            ${watsonResponse.output.text.join('. ')}
        </amazon:auto-breaths>`));
};

module.exports.conversation = conversation.conversation;
module.exports.voice = voice.voice;
module.exports.google = [googleAssistant.loadParams, googleAssistant.google];
