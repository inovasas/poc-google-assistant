'use strict';

// @ts-ignore
const sanitize = require('sanitize-html');
// @ts-ignore
const _ = require('lodash');
// @ts-ignore
const fp = require('lodash/fp');
// @ts-ignore
const geolib = require('geolib');
const {
    loadCredentials,
    filterTokenList,
// @ts-ignore
} = require('../../../repositories/watsonData');
// @ts-ignore
const wa = require('../../../helpers/WCSInstance');
// @ts-ignore
const location = require('../../../repositories/prestadores');
// @ts-ignore
const voice = require('../../../helpers/voice');
const chatRepository = require('../../../repositories/chat');
// @ts-ignore
const manifestacao = require('../../../repositories/salesforce');

// @ts-ignore
const SANITIZE_CONFIG = { allowedTags: [], allowedAttributes: {} };
const DEFAULT_CONTEXT = '{ "origem": "google", "filtro_produto": "SAUDE" }';

// @ts-ignore
async function loadParams(req, res, next) {
    const body = req.body;
    const context = JSON.parse(body.conversation.conversationToken || DEFAULT_CONTEXT);
    req.locals.context = context;

    const watsonCredentialsList = await loadCredentials(req.query.externaltoken);
    const watsonCredentials = await filterTokenList(watsonCredentialsList,
        context.conversation_id || '');
    req.locals.watsonCredentials = watsonCredentials;
    next();
}


/**
 * Constrói um intent (Login de Segurado)
 *
 * @param {string} permissionContext Para que o que serve a permissão
 * @param {string} requestPrompt A justificativa da permissão
 */
function buildLoginSegurado(permissionContext, requestPrompt, richResponse) {
  richResponse.items.push(
      
    {
      simpleResponse: {

        "textToSpeech": 'Vamos lá! Sobre que assunto iremos tratar?,?,?,?,?,?, Aqui podemos falar sobre Rede referenciada, autorização prévia, assuntos financeiros como boletos ou reembolso, atendimento médico, carteirinha digital, carta de permanência',
        displayText: "Vamos lá! Sobre que assunto iremos tratar? ",
        
      }
    });
  return {
    "intent": "actions.intent.OPTION",
    "inputValueData": {
      "@type": "type.googleapis.com/google.actions.v2.OptionValueSpec",
      "listSelect": {
        "title": "",
        "items": [
          {
            "optionInfo": {
              "key": "Rede referenciada",
              "synonyms": [
                "buscar rede"

              ]
            },
            "description": "Faça uma busca na nossa rede referenciada.",
            "image": {
              "url": "https://i.ibb.co/X4Y9rz4/Componente-2-1-2x.png",
              "accessibilityText": "atendimento"
            },
            "title": "Rede Referenciada"
          },
          {
            "optionInfo": {
              "key": "Autorização prévia"
            },
            "description": "Consulte agora! sua Autorização Prévia",
            "image": {
              "url": "https://i.ibb.co/RjmZRcg/vpp.png",
              "accessibilityText": "atendimento"
            },
            "title": "Autorização Prévia"
          },
          {
            "optionInfo": {
              "key": "financeiro",
              "synonyms": [
                "financeiro"

              ]
            },
            "description": "Reembolso, Imposto de Renda,  2ª via de boleto, reajustes, beneficios e descontos",
            "image": {
              "url": "https://i.ibb.co/cNjWFQw/assuntos-financeiros.png",
              "accessibilityText": "atendimento"
            },
            "title": "Assuntos Financeiro"
          },
          {
            "optionInfo": {
              "key": "Atendimento médico",
              "synonyms": [
                "Login"
              ]
            },
            "description": "Saúde na tela, Médico em Casa, Receita Digital e Preparo para Exames",
            "image": {
              "url": "https://i.ibb.co/2tp3ms1/atendimento-medico.png",
              "accessibilityText": "Login"
            },
            "title": "Atendimento Médico"
          },
          {
            "optionInfo": {
              "key": "Carteirinha Digital",
              "synonyms": [
                "Carteirinha"
              ]
            },
            "description": "Acesse sua Carteirinha Digital",
            "image": {
              "url": "https://i.ibb.co/rHf0RD6/Componente-1-1-2x.png",
              "accessibilityText": ""
            },
            "title": "Carteirinha Digital"
          },
          {
            "optionInfo": {
              "key": "Carta de Permanência",
            "synonyms": [
              "Carta"

            ]
          },
          "description": "Solicite sua carta de Permanência",
          "image": {
            "url": "https://i.ibb.co/02VWP7B/cartadepermanencia.png",
            "accessibilityText": "atendimento"
          },
          "title": "Carta de Permanência"
          },
          
          ]
        }
      }
    };
}

/**
 * Constrói um intent (Opções reembolso)
 */
function buildRembolsoOpcoes(permissionContext, requestPrompt, richResponse) {
  richResponse.items.push(
      
    {
      simpleResponse: {

        "textToSpeech": "O que você precisa saber sobre reembolso?,\nVou te ajudar com alguns, exemplos, olha aí,\n Como solicitar reembolso?,\n Consultar um pedido,\n Prévia de reembolso,\n Como reapresentar um reembolso?,\n Qual o valor do meu reembolso?,\n Qual o prazo para pagamento?,\n Documentos de reembolso\n optionInformações sobre fraudes,\n Definição sobre o que é reembolso,",
        "displayText": "O que você precisa saber sobre reembolso?"
        
      }
    });
  return {
    "intent": "actions.intent.OPTION",
    "inputValueData": {
      "@type": "type.googleapis.com/google.actions.v2.OptionValueSpec",
      "listSelect": {
        "title": "",
        "items": [
          {
            "optionInfo": {
              "key": "Como solicitar reembolso?"
            },
            "title": "Como solicitar reembolso?",
            "image": {}
          },
          {
            "optionInfo": {
              "key": "Consultar um pedido"
            },
            "title": "Consultar um pedido",
            "image": {}
          },
          {
            "optionInfo": {
              "key": "Prévia de reembolso"
            },
            "title": "Prévia de reembolso",
            "image": {}
          },
          {
            "optionInfo": {
              "key": "Como reapresentar um reembolso?"
            },
            "title": "Como reapresentar um reembolso?",
            "image": {}
          },
          {
            "optionInfo": {
              "key": "Qual o valor do meu reembolso?"
            },
            "title": "Qual o valor do meu reembolso?",
            "image": {}
          },
          {
            "optionInfo": {
              "key": "Qual o prazo para pagamento?"
            },
            "title": "Qual o prazo para pagamento?",
            "image": {}
          },
          {
            "optionInfo": {
              "key": "Documentos de reembolso"
            },
            "title": "Documentos de reembolso",
            "image": {}
          },
          {
            "optionInfo": {
              "key": "Informações sobre fraudes"
            },
            "title": "Informações sobre fraudes",
            "image": {}
          },
          {
            "optionInfo": {
              "key": "Definição sobre o que é reembolso"
            },
            "title": "Definição sobre o que é reembolso",
            "image": {}
          }
          ,
          
          ]
        }
      }
    };
}
/**
 * Constrói um intent (Altorização_prévia)
 *
 * @param {string} permissionContext Para que o que serve a permissão
 * @param {string} requestPrompt A justificativa da permissão
 */
function buildAutorizacaoPrevia(permissionContext, requestPrompt, richResponse) {
  richResponse.items.push(
      
    {
      simpleResponse: {

        "textToSpeech": "O que você precisa saber sobre Autorização Prévia?, \n\n Como solicitar autorização Prévia?,\nQual é o prazo de análise?,\nAcompanhar uma autorização.,\nImprimir uma segunda via de autorização.,",
        "displayText": "O que você precisa saber sobre Autorização Prévia?"
        
      }
    });
  return {
    "intent": "actions.intent.OPTION",
    "inputValueData": {
      "@type": "type.googleapis.com/google.actions.v2.OptionValueSpec",
      "listSelect": {
        "title": "",
        "items": [
          {
            "optionInfo": {
              "key": "Como solicitar autorização Prévia"
            },
            "title": "Como solicitar autorização Prévia?",
            "image": {}
          },
          {
            "optionInfo": {
              "key": "Qual é o prazo de análise"
            },
            "title": "Qual é o prazo de análise?",
            "image": {}
          },
          {
            "optionInfo": {
              "key": "Acompanhar uma autorização"
            },
            "title": "Acompanhar uma autorização",
            "image": {}
          },
          {
            "optionInfo": {
              "key": "Imprimir uma 2 via de autorização"
            },
            "title": "Imprimir uma 2ª via de autorização",
            "image": {}
          }
          
          ]
        }
      }
    };
}

/**
 * Constrói um intent (Atendimento_médico)
 *
 * @param {string} permissionContext Para que o que serve a permissão
 * @param {string} requestPrompt A justificativa da permissão
 */
function buildAtendimentoMedico(permissionContext, requestPrompt, richResponse) {
  richResponse.items.push(
      
    {
      simpleResponse: {

        "textToSpeech": 'Sobre que assunto você precisa falar? Atendimento 24 horas, saúde na tela, médico em casa, receita digital ou preparo para exames,?,?,?,?,?,?,',
        displayText: "Sobre que assunto você precisa falar?",
        
      }
    });
  return {
    "intent": "actions.intent.OPTION",
    "inputValueData": {
      "@type": "type.googleapis.com/google.actions.v2.OptionValueSpec",
      "listSelect": {
        "title": "",
        "items": [
          {
            "optionInfo": {
              "key": "Atendimento 24 horas",
              "synonyms": [
                "Atendimento de urgencia"

              ]
            },
            "description": "Fale com um profissional da sáude via telefone a qualquer hora.",
            "image": {
              "url": "https://i.ibb.co/MP0Djny/atendimento-24h.png",
              "accessibilityText": "atendimento"
            },
            "title": "Orientação médica telefônica"
          },
          {
            "optionInfo": {
              "key": "Saúde na tela",
              "synonyms": [
                "médico na tela"

              ]
            },
            "description": "Agora você pode realizar consultas com médicos de diversas especialidades ou com um plantonista aí mesmo no seu celular, computador ou tablet.",
            "image": {
              "url": "https://i.ibb.co/tXMWk04/saude-na-tela.png",
              "accessibilityText": "atendimento"
            },
            "title": "Saúde na Tela"
          },
          {
            "optionInfo": {
              "key": "Médico em casa",
              "synonyms": [
                "Atendimento domiciliar"

              ]
            },
            "description": "Médico em Casa, sem filas, sem burocracia e com todo o conforto e segurança do seu lar. São dois atendimentos por ano para cada segurado de até 12 anos e acima de 65 anos.",
            "image": {
              "url": "https://i.ibb.co/9rmvd4y/icone-medicoemcasa.png",
              "accessibilityText": "Médico em Casa"
            },
            "title": "Médico em Casa"
          },
          {
            "optionInfo": {
              "key": "Receita Digital",
              "synonyms": [
                "Receita Digital"
              ]
            },
            "description": "O atendimento médico por vídeo também permite que você receba prescrição de medicamento e guia de exames.",
            "image": {
              "url": "https://i.ibb.co/qJjh403/receita-digital.png",
              "accessibilityText": "Login"
            },
            "title": "Receita Digital"
          },
          {
            "optionInfo": {
              "key": "Preparo para exames",
              "synonyms": [
                "Preparo para exames"
              ]
            },
            "description": "Informações a respeito de preparo para exames.",
            "image": {
              "url": "https://i.ibb.co/njPrDSj/preparo.png",
              "accessibilityText": ""
            },
            "title": "Preparo para Exames"
          }
          
          ]
        }
      }
    };
}

/**
 * Constrói um intent (menu assuntos financeiros)
 *
 * @param {string} permissionContext Para que o que serve a permissão
 * @param {string} requestPrompt A justificativa da permissão
 */
function buildAssuntosFinanceiros(permissionContext, requestPrompt, richResponse) {
  richResponse.items.push(
      
    {
      simpleResponse: {

        "textToSpeech": 'Aqui você pode tratar assuntos financeiros como: Reembolso, imposto de renda, segunda via de boleto, reajuste ou beneficios e descontos.',
        displayText: "Aqui você pode tratar assuntos financeiros como:",
        
      }
    });
  return {
    "intent": "actions.intent.OPTION",
    "inputValueData": {
      "@type": "type.googleapis.com/google.actions.v2.OptionValueSpec",
      "listSelect": {
        "title": "",
        "items": [
          {
            "optionInfo": {
              "key": "Reembolso",
              "synonyms": [
                "rembolsar"

              ]
            },
            "description": "Solicitar reembolso, Consultar pedido, prévia de reembolso e dúvidas.",
            "image": {
              "url": "https://i.ibb.co/p02t6Zw/icone-reembolso.png",
              "accessibilityText": "atendimento"
            },
            "title": "Reembolso"
          },
          
          {
            "optionInfo": {
              "key": "imposto de renda",
              "synonyms": [
                "Leão",
                "iRPF"
              ]
            },
            "description": "Demonstrativo de Imposto de Renda",
            "image": {
              "url": "https://i.ibb.co/2NcxXhZ/icone-impostorenda.png",
              "accessibilityText": ""
            },
            "title": "Imposto de renda"
          },
          {
            "optionInfo": {
              "key": "Segunda via de boleto",
              "synonyms": [
                "2 via de boleto"
              ]
            },
            "description": "Solicite a segunda via de boleto",
            "image": {
              "url": "https://i.ibb.co/SyYyJcX/icone-2aviaboleto.png",
              "accessibilityText": "Login"
            },
            "title": "2ª via de boleto"
          },
          {
            "optionInfo": {
              "key": "Reajuste",
              "synonyms": [
                "Reajuste"
                
              ]
            },
            "description": "Informações sobre reajuste",
            "image": {
              "url": "https://i.ibb.co/smDn8NZ/reajuste.png",
              "accessibilityText": ""
            },
            "title": "Reajuste"
          },
          {
            "optionInfo": {
              "key": "Beneficios e descontos",
              "synonyms": [
                "beneficios",
                "descontos"
              ]
            },
            "description": "Conheça seus Benefícios e descontos.",
            "image": {
              "url": "https://i.ibb.co/n0pCXDY/beneficios.png",
              "accessibilityText": ""
            },
            "title": "Benéficios e Descontos."
          },
          
          ]
        }
      }
    };
}
/**
 * Constrói um intent (menu inicial)
 *
 * @param {string} permissionContext Para que o que serve a permissão
 * @param {string} requestPrompt A justificativa da permissão
 */
function buildMenuInicial(permissionContext, requestPrompt, richResponse) {
  richResponse.items.push(
      
    {
      simpleResponse: {

        "textToSpeech": 'Olá, eu sou a érica a assistente virtual do SulAmérica e posso te ajudar com o seguintes assuntos: Canais de atendimento, ou para assunto referentes ao seu plano acesse Login do segurado.,\n,\n,\n,\n, ah! eu também posso esclarecer suas dúvidas e te dar informações',
        displayText: "Olá, eu sou a Érica a assistente virtual do SulAmérica e posso te ajudar com o seguintes assuntos:",
        
      }
    });
  return {
    "intent": "actions.intent.OPTION",
    "inputValueData": {
      "@type": "type.googleapis.com/google.actions.v2.OptionValueSpec",
      "listSelect": {
        "title": "",
        "items": [
          {
            "optionInfo": {
              "key": "atendimento",
              "synonyms": [
                "atendimento"

              ]
            },
            "description": "Fale conosco.",
            "image": {
              "url": "https://i.ibb.co/jD6423c/telefone2.png",
              "accessibilityText": "atendimento"
            },
            "title": "Canais de atendimento"
          },
          {
            "optionInfo": {
              "key": "Login",
              "synonyms": [
                "Login"
              ]
            },
            "description": "Acesse a informações do seu plano.",
            "image": {
              "url": "https://i.ibb.co/m04gvm5/cadeado.png",
              "accessibilityText": "Login"
            },
            "title": "Login de Segurado"
          },
          {
            "optionInfo": {
              "key": "Dúvidas",
              "synonyms": [
                "tirar suas dúvidas",
                "informações"
              ]
            },
            "description": "Tire suas Dúvidas.",
            "image": {
              "url": "https://i.ibb.co/gr5rGxr/icone-duvidas.png",
              "accessibilityText": ""
            },
            "title": "Dúvidas"
          }
          ]
        }
      }
    };
}
//Central de atendimento - Canais de atendimento (padrão google)
/** @type {((string) => string)} */
// @ts-ignore
const capitalize = fp.compose(_.startCase, _.lowerCase);
// @ts-ignore
function buildCentralDeAtendimento(watsonResponse, richResponse) {
    richResponse.items.push(
      
      {
        "simpleResponse": {
          "textToSpeech": "Muito bem, se você deseja atendimento pode acessar os seguintes canais. Central de relacionamento capitais e regiões metropolitanas, Central de relacionamento demais localidades, SáQUI ou atendimento via whatsapp",
          "displayText": "Muito bem, se você deseja atendimento pode acessar os seguintes canais."
        }
      },
      {
        "carouselBrowse": {
          "items": [
            {
              "description": "Capitais e regiões metropolitanas (capitais e grandes cidades do interior)",
              "footer": "",
              "image": {
                "accessibilityText": "Image alternate text",
                "url": "https://i.ibb.co/QcWK9ts/icone-omt.png"
              },
              "openUrlAction": {
                "url": "https://tinyurl.com/CRmetropolitana"
              },
              "title": "Central de Relacionamento"
            },
            {
              "description": "Demais localidades",
              "footer": "",
              "image": {
                "accessibilityText": "Image alternate text",
                "url": "https://i.ibb.co/yWbhV75/icone-omt2.png"
              },
              "openUrlAction": {
                "url": "https://tinyurl.com/CRdemais"
              },
              "title": "Central de Relacionamento"
            },
            {
              "description": "Reclamações, Cancelamentos e Informações Institucionais",
              "footer": "",
              "image": {
                "accessibilityText": "Image alternate text",
                "url": "https://i.ibb.co/dt6WSCX/sac.png"
              },
              "openUrlAction": {
                "url": "https://tinyurl.com/SAC-SAS"
              },
              "title": "SAC"
            },
            {
              "description": "Atendimento via WhatsApp",
              "footer": "",
              "image": {
                "accessibilityText": "Image alternate text",
                "url": "https://i.ibb.co/k0GB6FZ/whats.png"
              },
              "openUrlAction": {
                "url": "https://api.whatsapp.com/send?phone=551130049723&text=Quero%20falar%20com%20a%20SulAm%C3%A9rica"
              },
              "title": "WhatsApp"
            }
          ]
        }
        
      }
    );
}
// constroi acompanhar vpp
function buildacompanharvpp(watsonResponse, richResponse) {
  richResponse.items.push(
    
    {
      "simpleResponse": {
        "textToSpeech": "Quer acompanhar a sua Validação prévia de procedimento? Vem comigo!   \n\nEm nosso site você faz assim, faça o login e siga os menus abaixo, \n• Validação Prévia de Procedimentos,\n• Consultar,\n• Selecione a autorização,\n\nNo nosso App siga esse passos,\n\n• Clique no menu principal,\n• Autorização de Procedimentos,\n• Selecione o segurado,\n• Selecione a autorização para visualizar,",
        "displayText": "Quer acompanhar a sua Validação prévia de procedimento? Vem comigo!   \n\nNo site faça o login e siga os menus abaixo ⬇️\n\n• Validação Prévia de Procedimentos\n• Consultar\n• Selecione a autorização\n\nNo App. Siga esses passos aqui  ⬇️\n\n• Clique no menu principal\n• Autorização de Procedimentos \n• Selecione o segurado\n• Selecione a autorização para visualizar"
      }
    },
    {
      "carouselBrowse": {
        "items": [
          {
            "description": "Acesse o nosso site.",
            "footer": "",
            "image": {
              "accessibilityText": "Image alternate text",
              "url": "https://i.ibb.co/jTjx8Pg/site2.png"
            },
            "openUrlAction": {
              "url": "https://saude.sulamericaseguros.com.br/segurado/login/"
            },
            "title": "Site Sulamérica"
          },
          {
            "description": "Acesse o nosso aplicativos",
            "footer": "",
            "image": {
              "accessibilityText": "Image alternate text",
              "url": "https://i.ibb.co/VmbDhbj/icone-saudenatela.png"
            },
            "openUrlAction": {
              "url": "https://saudesas.page.link/app"
            },
            "title": "Aplicativo Saúde"
          },
          {
            "description": "Acesse mais informações sobre autorização prévia",
            "footer": "",
            "image": {
              "accessibilityText": "Image alternate text",
              "url": "https://i.ibb.co/gr5rGxr/icone-duvidas.png"
            },
            "openUrlAction": {
              "url": "http://bit.ly/2YpD6Zk"
            },
            "title": "Mais informações"
          }
        ]
      }
      
    }
  );
}
// constroi segunda via vpp
function buildsegundaviavpp(watsonResponse, richResponse) {
  richResponse.items.push(
    
    {
      "simpleResponse": {
        "textToSpeech": "Se quiser pedir a Segunda Via da VPP . Siga o passos,\n\n\nNo Site, primeiro faça login., Em seguida acesse os seguintes menus,\n\n•Validação Prévia de Procedimentos,\n•Consultar,\n•Selecione a autorização,\n•Se desejar imprimir, clique na opção, Versão para Impressão.\n\nNo Aplicativo\n\n•Autorização de Procedimentos,\n•Selecione o segurado,\n•Selecione a autorização, para visualizar,\n•Se desejar receber por e-mail, clique na opção, Enviar anexos por e-mail.",
              "displayText": "Se quiser pedir a Segunda Via da VPP . Siga o passos 👇 \n\n\nNo Site, primeiro faça login em seguida acesse os seguintes menus⬇️.\n\n•Validação Prévia de Procedimentos\n•Consultar\n•Selecione a autorização\n•Se desejar imprimir, clique na opção: Versão para Impressão.\n\nNo Aplicativo⬇️\n\n•Autorização de Procedimentos\n•Selecione o segurado\n•Selecione a autorização para visualizar\n•Se desejar receber por e-mail, clique na opção: Enviar anexos por e-mail."
      }
    },
    {
      "carouselBrowse": {
        "items": [
          {
            "description": "Acesse o nosso site.",
            "footer": "",
            "image": {
              "accessibilityText": "Image alternate text",
              "url": "https://i.ibb.co/jTjx8Pg/site2.png"
            },
            "openUrlAction": {
              "url": "https://saude.sulamericaseguros.com.br/segurado/login/"
            },
            "title": "Site Sulamérica"
          },
          {
            "description": "Acesse o nosso aplicativos",
            "footer": "",
            "image": {
              "accessibilityText": "Image alternate text",
              "url": "https://i.ibb.co/VmbDhbj/icone-saudenatela.png"
            },
            "openUrlAction": {
              "url": "https://saudesas.page.link/app"
            },
            "title": "Aplicativo Saúde"
          },
          {
            "description": "Acesse mais informações sobre autorização prévia",
            "footer": "",
            "image": {
              "accessibilityText": "Image alternate text",
              "url": "https://i.ibb.co/gr5rGxr/icone-duvidas.png"
            },
            "openUrlAction": {
              "url": "http://bit.ly/2YpD6Zk"
            },
            "title": "Mais informações"
          }
        ]
      }
      
    }
  );
  }
//link Como solicitar reembolso
function buildComosolicitarreembolso(watsonResponse, richResponse) {
  richResponse.items.push(
    
    {
      "simpleResponse": {
        "textToSpeech": "Realizar uma solicitação de reembolso está mais prático. Através dos canais digitais, em poucos cliques, você envia sua documentação e a SulAmérica inicia a análise, em um processo \nrápido e seguro.\nVocê pode acessar o nosso aplicativo e clicar em reembolso, ou acessar o link abaixo e, clicar na guia reembolso.",
        "displayText": "Realizar uma solicitação de reembolso está mais prático. Através dos canais digitais, em poucos cliques, você envia sua documentação e a SulAmérica inicia a análise, em um processo \nrápido e seguro.\nVocê pode acessar o nosso aplicativo e clicar em reembolso ou acessar o link abaixo e clicar na guia reembolso."
      }
    },
    {
      "carouselBrowse": {
        "items": [
          {
            "description": "Você será direcionado ao Aplicativo SulAmérica Saúde",
            "footer": "",
            "image": {
              "accessibilityText": "Image alternate text",
              "url": "https://i.ibb.co/VmbDhbj/icone-saudenatela.png"
            },
            "openUrlAction": {
              "url": "https://saudesas.page.link/app"
            },
            "title": "Acessar o aplicativo."
          },
          {
            "description": "Você será direcionado a área de reembolso no nosso site.",
            "footer": "",
            "image": {
              "accessibilityText": "Image alternate text",
              "url": "https://i.ibb.co/jTjx8Pg/site2.png"
            },
            "openUrlAction": {
              "url": "https://saude.sulamericaseguros.com.br/reembolsosaude/formas-de-solicitar/"
            },
            "title": "Central de Relacionamento"
          }
        ]
      }
      
    }
  );
}
//link app e site
function buildlinkappesite(watsonResponse, richResponse) {
  richResponse.items.push(
    {
      "carouselBrowse": {
        "items": [
          {
            "description": "Você será direcionado ao Aplicativo SulAmérica Saúde",
            "footer": "",
            "image": {
              "accessibilityText": "Image alternate text",
              "url": "https://i.ibb.co/VmbDhbj/icone-saudenatela.png"
            },
            "openUrlAction": {
              "url": "https://saudesas.page.link/app"
            },
            "title": "Acessar o Aplicativo."
          },
          {
            "description": "Você será direcionado a área de reembolso no nosso site.",
            "footer": "",
            "image": {
              "accessibilityText": "Image alternate text",
              "url": "https://i.ibb.co/jTjx8Pg/site2.png"
            },
            "openUrlAction": {
              "url": "https://saude.sulamericaseguros.com.br/segurado/login/"
            },
            "title": "Acessar o Site."
          }
        ]
      }
      
    }
  );
}
//link app medico em casa
function buildmedicoemcasa(watsonResponse, richResponse) {
  richResponse.items.push(
    {
      "carouselBrowse": {
        "items": [
          {
            "description": "Você será direcionado ao Aplicativo SulAmérica Saúde",
            "footer": "",
            "image": {
              "accessibilityText": "Image alternate text",
              "url": "https://i.ibb.co/VmbDhbj/icone-saudenatela.png"
            },
            "openUrlAction": {
              "url": "https://saudesas.page.link/app"
            },
            "title": "Acessar o Aplicativo Saúde."
          },
          {
            "description": "Mais detelhes sobre o médico na tela",
            "footer": "",
            "image": {
              "accessibilityText": "Image alternate text",
              "url": "https://i.ibb.co/gr5rGxr/icone-duvidas.png"
            },
            "openUrlAction": {
              "url": "https://bit.ly/30NB5DA"
            },
            "title": "Mais detalhes"
          },
          {
            "description": "Fale agora com um especialista via telefone",
            "footer": "",
            "image": {
              "accessibilityText": "Image alternate text",
              "url": "https://i.ibb.co/gr5rGxr/icone-duvidas.png"
            },
            "openUrlAction": {
              "url": "https://tinyurl.com/orientacaomedica"
            },
            "title": "Central de atendimento 24 h"
          }
        ]
      }
      
    }
  );
}
//link saude na tela
function buildsaudenatela(watsonResponse, richResponse) {
  richResponse.items.push(
    {
      "carouselBrowse": {
        "items": [
          {
            "description": "Você será direcionado ao Aplicativo SulAmérica Saúde",
            "footer": "",
            "image": {
              "accessibilityText": "Image alternate text",
              "url": "https://i.ibb.co/VmbDhbj/icone-saudenatela.png"
            },
            "openUrlAction": {
              "url": "https://saudesas.page.link/app"
            },
            "title": "Acessar o Aplicativo Saúde."
          },
          {
            "description": "Mais detelhes sobre o saúde na tela",
            "footer": "",
            "image": {
              "accessibilityText": "Image alternate text",
              "url": "https://i.ibb.co/gr5rGxr/icone-duvidas.png"
            },
            "openUrlAction": {
              "url": "https://saudenatela.sulamerica.com.br/"
            },
            "title": "Mais detalhes"
          }
        ]
      }
      
    }
  );
}
//link App
function buildlinkApp(watsonResponse, richResponse) {
  richResponse.items.push(
    
    {
      "basicCard": {
        "image": {
          "url": "https://i.ibb.co/VmbDhbj/icone-saudenatela.png",
          "accessibilityText": "Image alternate text"
        },
        "buttons": [
          {
            "title": "Acesse o nosso Aplicativo",
            "openUrlAction": {
              "url": "https://saudesas.page.link/app"
            }
          }
        ],
        "imageDisplayOptions": "CROPPED"
      }
    }
  );
}

//monta resposta para carta de permencia 
function buildcartadepermanencia(watsonResponse, richResponse) {
  richResponse.items.push(
    {
      "simpleResponse": {
        "textToSpeech": "Para conseguir a carta de permanência, entra lá no nosso site, clicando no botão abaixo , \nfaça o login e siga os menus abaixo \n\n• Clique em Meu Plano\n• Em seguida em \"Carta de Permanência\" \n• Deixe o bloqueador de pop-up de seu navegador de internet  inativo para realizar esta operação, tudo bem? \n\nA carta está em formato PDF e para abri-la, você precisa ter instalado o Adobe Acrobat ou outro leitor de PDF de sua preferência.",
              "displayText": "Para conseguir a carta de permanência, entra lá no nosso site, clicando no botão abaixo, \n\nFaça o login e siga os menus abaixo ⬇️\n\n• Clique em Meu Plano\n• Em seguida em \"Carta de Permanência\" \n• Deixe o bloqueador de pop-up de seu navegador de internet  inativo para realizar esta operação, tudo bem? \n\n📌 A carta está em formato PDF e para abri-la, você precisa ter instalado o Adobe Acrobat ou outro leitor de PDF de sua preferência."
      }
    },
    {
      "basicCard": {
        "image": {
          "url": "https://i.ibb.co/jTjx8Pg/site2.png",
          "accessibilityText": "Image alternate text"
        },
        "buttons": [
          {
            "title": "Portal Sulamérica",
            "openUrlAction": {
              "url": "https://saude.sulamericaseguros.com.br/segurado/login/"
            }
          }
        ],
        "imageDisplayOptions": "CROPPED"
      }
    }
  );
}

//link mais informações solicitar autorização prévia
function buildmaisinfosolicitarvpp(watsonResponse, richResponse) {
  richResponse.items.push(
    
    {
      "basicCard": {
        "image": {
          "url": "https://i.ibb.co/gr5rGxr/icone-duvidas.png",
          "accessibilityText": "Image alternate text"
        },
        "buttons": [
          {
            "title": "Mais informações",
            "openUrlAction": {
              "url": "http://bit.ly/2YpD6Zk"
            }
          }
        ],
        "imageDisplayOptions": "CROPPED"
      }
    }
  );
}
//link Informações receita digital
function buildreceitadigital(watsonResponse, richResponse) {
  richResponse.items.push(
    
    {
      "basicCard": {
        "image": {
          "url": "https://i.ibb.co/gr5rGxr/icone-duvidas.png",
          "accessibilityText": "Image alternate text"
        },
        "buttons": [
          {
            "title": "Mais informações",
            "openUrlAction": {
              "url": "https://sulamerica.com.br/saude/mediconatela/FAQPrescri%C3%A7%C3%A3o.pdf"
            }
          }
        ],
        "imageDisplayOptions": "CROPPED"
      }
    }
  );
}
//orientação médica 24 horas
function buildorientacaomedica(watsonResponse, richResponse) {

  richResponse.items.push(
    {
      "simpleResponse": {
        "textToSpeech": "Uma ligação que leva saúde e tranquilidade até você.\nUma equipe de profissionais de saúde especializados está pronta para tirar suas dúvidas, passar orientações e transmitir muita tranquilidade para você,\nTudo isso é gratuito e ilimitado, disponível 24 horas e 7 dias por semana para você.\n\nQuando você pode ligar?\n\n-Acidentes domésticos e situações de emergência;\n-Exageros no fim de semana; \n-Dúvidas na gestação e cuidados com os filhos.\n-Dúvidas sobre medicamentos, sintomas, mal estar, gripe etc;.",
              "displayText": "Uma ligação que leva saúde e tranquilidade até você.\nUma equipe de profissionais de saúde especializados está pronta para tirar suas dúvidas, passar orientações e transmitir muita tranquilidade para você,\nTudo isso é gratuito e ilimitado, disponível 24 horas e 7 dias por semana para você.\n\nQuando você pode ligar?\n\n-Acidentes domésticos e situações de emergência;\n-Exageros no fim de semana;  \n-Dúvidas na gestação e cuidados com os filhos.\n-Dúvidas sobre medicamentos, sintomas, mal estar, gripe etc;."
      }
    },
    {
      "basicCard": {
        "image": {
          "url": "https://i.ibb.co/gr5rGxr/icone-duvidas.png",
          "accessibilityText": "Image alternate text"
        },
        "buttons": [
          {
            "title": "Orientação Médica 24h",
            "openUrlAction": {
              "url": "https://tinyurl.com/orientacaomedica"
            }
          }
        ],
        "imageDisplayOptions": "CROPPED"
      }
    }
  );
}
//link site
function buildlinksite(watsonResponse, richResponse) {
  richResponse.items.push(
    
    {
      "basicCard": {
        "image": {
          "url": "https://i.ibb.co/jTjx8Pg/site2.png",
          "accessibilityText": "Image alternate text"
        },
        "buttons": [
          {
            "title": "Acesse o nosso Site",
            "openUrlAction": {
              "url": "https://saude.sulamericaseguros.com.br/segurado/login/"
            }
          }
        ],
        "imageDisplayOptions": "CROPPED"
      }
    }
  );
}

//link reajuste gestor
function buildlinkreajustegestor(watsonResponse, richResponse) {
  richResponse.items.push(
    
    {
      "basicCard": {
        "image": {
          "url": "https://i.ibb.co/gr5rGxr/icone-duvidas.png",
          "accessibilityText": "Image alternate text"
        },
        "buttons": [
          {
            "title": "Acesse aqui",
            "openUrlAction": {
              "url": "https://shorturl.at/abdoK"
            }
          }
        ],
        "imageDisplayOptions": "CROPPED"
      }
    }
  );
}
//link whats qualicorp
function buildwhatsqualicorp(watsonResponse, richResponse) {
  richResponse.items.push(
    
    {
      "basicCard": {
        "image": {
          "url": "https://i.ibb.co/gr5rGxr/icone-duvidas.png",
          "accessibilityText": "Image alternate text"
        },
        "buttons": [
          {
            "title": "WhatsApp Qualicorp",
            "openUrlAction": {
              "url": "https://bit.ly/2XRH4WU"
            }
          }
        ],
        "imageDisplayOptions": "CROPPED"
      }
    }
  );
}
//link descontos
function buildlinkdescontos(watsonResponse, richResponse) {
  richResponse.items.push(
    
    {
      "basicCard": {
        "image": {
          "url": "https://i.ibb.co/jTjx8Pg/site2.png",
          "accessibilityText": "Image alternate text"
        },
        "buttons": [
          {
            "title": "Acesse o nosso Site",
            "openUrlAction": {
              "url": "http://sulamericamais.com.br/"
            }
          }
        ],
        "imageDisplayOptions": "CROPPED"
      }
    }
  );
}

//link busca rede referenciada
function buildlinkrede(watsonResponse, richResponse) {
  richResponse.items.push(
    
    {
      "basicCard": {
        "image": {
          "url": "https://i.ibb.co/gr5rGxr/icone-duvidas.png",
          "accessibilityText": "Image alternate text"
        },
        "buttons": [
          {
            "title": "São muitos prestadores.Clique e Confira! ",
            "openUrlAction": {
              "url": "http://bit.ly/2Tk1I0p"
            }
          }
        ],
        "imageDisplayOptions": "CROPPED"
      }
    }
  );
}

//link busca rede referenciada
function buildrederesponse(watsonResponse, richResponse) {
  richResponse.push(
    
    {
      items: watsonResponse.context.map(z => ({
          simpleResponse: {
              ssml: `<speak>${z}</speak>`,
              displayText: z,
          },
      }))
  }
  );
}
/**
 * Constroi o campo expected inputs da resposta do google assistant
 * @param {string} input Request input
 * @param {any} watsonResponse Watson response
 * @returns {Promise<any>} O campo expected inputs
 */
async function buildInputPrompt(input, watsonResponse, googleIntent) {
    const resp = {
        expectUserResponse: !('end_flow' in watsonResponse.output) &&
            googleIntent !== 'actions.intent.CANCEL',
    };
    const richResponse = {
        items: watsonResponse.output.text.map(x => ({
            simpleResponse: {
                ssml: `<speak>${x}</speak>`,
                displayText: x,
            },
        })),
        suggestions: _.flatMap(
            _.filter(watsonResponse.output.generic, x => x.response_type === 'option'),
            resp => _.map(resp.options, opt => ({ title: opt.label }))),
    };
    const inputs = [{
        inputPrompt: { richInitialPrompt: richResponse },
        possibleIntents: [{ intent: 'actions.intent.TEXT' }],
    }];

    if ('menu_inicial' in watsonResponse.output) {
        const inicial = watsonResponse.output.menu_inicial;
        inputs[0].possibleIntents = [
          buildMenuInicial(inicial.justificativa, watsonResponse.output.text[0], richResponse),
        ];
    }
    if ('autorizacao_previa' in watsonResponse.output) {
      const inicial = watsonResponse.output.autorizacao_previa;
      inputs[0].possibleIntents = [
        buildAutorizacaoPrevia(inicial.justificativa, watsonResponse.output.text[0], richResponse),
      ];
  }
    if ('assuntos_financeiros' in watsonResponse.output) {
      const inicial = watsonResponse.output.assuntos_financeiros;
      inputs[0].possibleIntents = [
        buildAssuntosFinanceiros (inicial.justificativa, watsonResponse.output.text[0], richResponse),
      ];
  }
  if ('reembolso_opcoes' in watsonResponse.output) {
    const inicial = watsonResponse.output.reembolso_opcoes;
    inputs[0].possibleIntents = [
      buildRembolsoOpcoes (inicial.justificativa, watsonResponse.output.text[0], richResponse),
    ];
}
  if ('atendimento_medico' in watsonResponse.output) {
    const inicial = watsonResponse.output.atendimento_medico;
    inputs[0].possibleIntents = [
      buildAtendimentoMedico (inicial.justificativa, watsonResponse.output.text[0], richResponse),
    ];
}

    if ('Login_Segurado' in watsonResponse.output) {
      const inicial = watsonResponse.output.Login_Segurado;
      inputs[0].possibleIntents = [
        buildLoginSegurado(inicial.justificativa, watsonResponse.output.text[0], richResponse),
      ];
  }
    if ('geocoding_address' in watsonResponse.output) {
        const latLong = await location.getLatLong(input);
        watsonResponse.context.lat = latLong.results[0].geometry.location.lat;
        watsonResponse.context.long = latLong.results[0].geometry.location.lng;
        const specialists = await location.getSpecialists(
            watsonResponse.context.cod_especialidades,
            latLong.results[0].geometry.location.lat,
            latLong.results[0].geometry.location.lng,
            '', '');
        watsonResponse.context.lista_endereco = voice.specialistsToVoice(specialists);

        // abre manifestacao
        abrirManifestacao(watsonResponse.context);
    }
//abre central de atendimento
    if ((googleIntent === 'actions.intent.PLACE' ||
        'mostrar_atendimento' in watsonResponse.output) &&
        !watsonResponse.context.permissao_negada) {
        buildCentralDeAtendimento(watsonResponse, richResponse);
    }
    //abre como solicitar reembolso
    if ((googleIntent === 'actions.intent.PLACE' ||
    'Como_solicitar_reembolso' in watsonResponse.output) &&
    !watsonResponse.context.permissao_negada) {
    buildComosolicitarreembolso(watsonResponse, richResponse);
}
   //abre link do app
   if ((googleIntent === 'actions.intent.PLACE' ||
   'link_app' in watsonResponse.output) &&
   !watsonResponse.context.permissao_negada) {
   buildlinkApp(watsonResponse, richResponse);
}
   //abre orientação médica 24h
   if ((googleIntent === 'actions.intent.PLACE' ||
   'orientacao_medica' in watsonResponse.output) &&
   !watsonResponse.context.permissao_negada) {
   buildorientacaomedica(watsonResponse, richResponse);
}
  //abre link do app
  if ((googleIntent === 'actions.intent.PLACE' ||
  'carta_permanencia' in watsonResponse.output) &&
  !watsonResponse.context.permissao_negada) {
  buildcartadepermanencia(watsonResponse, richResponse);
}
//abre acompanhar vpp
if ((googleIntent === 'actions.intent.PLACE' ||
'acompanhar_vpp' in watsonResponse.output) &&
!watsonResponse.context.permissao_negada) {
buildacompanharvpp(watsonResponse, richResponse);
}
//abre segunda via vpp
if ((googleIntent === 'actions.intent.PLACE' ||
'segunda_via_vpp' in watsonResponse.output) &&
!watsonResponse.context.permissao_negada) {
buildsegundaviavpp(watsonResponse, richResponse);
}
   //abre link mais informaões solicitar vpp
   if ((googleIntent === 'actions.intent.PLACE' ||
   'mais_info_solicitr_vpp' in watsonResponse.output) &&
   !watsonResponse.context.permissao_negada) {
   buildmaisinfosolicitarvpp(watsonResponse, richResponse);
}
   //abre link do app
   if ((googleIntent === 'actions.intent.PLACE' ||
   'saude_na_tela' in watsonResponse.output) &&
   !watsonResponse.context.permissao_negada) {
   buildsaudenatela(watsonResponse, richResponse);
}
  //abre link do app
  if ((googleIntent === 'actions.intent.PLACE' ||
  'link_site' in watsonResponse.output) &&
  !watsonResponse.context.permissao_negada) {
  buildlinksite(watsonResponse, richResponse);
}
  //abre link reajuste gestor
  if ((googleIntent === 'actions.intent.PLACE' ||
  'link_reajuste_gestor' in watsonResponse.output) &&
  !watsonResponse.context.permissao_negada) {
  buildlinkreajustegestor(watsonResponse, richResponse);
}
  //abre whatsapp qualicorp
  if ((googleIntent === 'actions.intent.PLACE' ||
  'whats_qualicorp' in watsonResponse.output) &&
  !watsonResponse.context.permissao_negada) {
  buildwhatsqualicorp(watsonResponse, richResponse);
}
  //abre link do app
  if ((googleIntent === 'actions.intent.PLACE' ||
  'receita_digital' in watsonResponse.output) &&
  !watsonResponse.context.permissao_negada) {
  buildreceitadigital(watsonResponse, richResponse);
}
  //abre medico em casa
  if ((googleIntent === 'actions.intent.PLACE' ||
  'link_medicoemcasa' in watsonResponse.output) &&
  !watsonResponse.context.permissao_negada) {
  buildmedicoemcasa(watsonResponse, richResponse);
}
  //abre link descontos
  if ((googleIntent === 'actions.intent.PLACE' ||
  'link_desconto' in watsonResponse.output) &&
  !watsonResponse.context.permissao_negada) {
  buildlinkdescontos(watsonResponse, richResponse);
}
  //abre link da rede referenciada
  if ((googleIntent === 'actions.intent.PLACE' ||
  'link_rede' in watsonResponse.output) &&
  !watsonResponse.context.permissao_negada) {
  buildlinkrede(watsonResponse, richResponse);
}
 //abre link da rede referenciada
 if ((googleIntent === 'actions.intent.PLACE' ||
 'rede_response' in watsonResponse.output) &&
 !watsonResponse.context.permissao_negada) {
 buildrederesponse(watsonResponse, richResponse);
}
   //abre link do app e site
   if ((googleIntent === 'actions.intent.PLACE' ||
   'link_app_site' in watsonResponse.output) &&
   !watsonResponse.context.permissao_negada) {
   buildlinkappesite(watsonResponse, richResponse);
}


    if (resp.expectUserResponse) {
        resp.expectedInputs = inputs;
    } else {
        resp.finalResponse = { richResponse };
    }

    resp.conversationToken = JSON.stringify(watsonResponse.context);

    return resp;
}

/**
 * 
 * @param {any} req
 * @param {any} res
 */
async function google(req, res) {
    const body = req.body;

    const context = req.locals.context;
    const watsonCredentials = req.locals.watsonCredentials;
    const googleIntent = body.inputs[0].intent;

    const request = {
        oid: watsonCredentials._id.toString(),
        input: body.inputs[0].rawInputs[0].query,
        context: context,
        workspace_id: watsonCredentials.workspace_id,
        username: watsonCredentials.username,
        password: watsonCredentials.password,
    };

    const text = request.input;

    if (googleIntent === 'actions.intent.CANCEL') {
        request.input = 'Até logo!';
    } else if (googleIntent === 'actions.intent.PLACE') {
        const args = req.body.inputs[0].arguments;

        if (args[0].status && args[0].status.code === 7) {
            request.context.permissao_negada = true;
        } else {
            const { latitude: lat, longitude: long } = args[0].placeValue.coordinates;
            let specialists;
            try {
                /** @type {any[]} */
                specialists = await location.getSpecialists(context.cod_especialidades, lat,
                    long, '', '');
            } catch (err) {
                specialists = [];
            }
            request.context.lista_endereco = voice.specialistsToVoice(specialists);


        }
        request.input = args[1].rawText;
    }

    if (body.conversation.type === 'NEW') {
        request.input = '';
    }

    const watsonResponse = await wa.sendMessage(request);

    const reqObj = {
        input: {
            text,
        },
        context: {
            conversation_id: watsonResponse.context.conversation_id,
            ...context,
        },
    };

    // grava request no banco
    if (request.input !== '') {
        chatRepository.logRequestMessage(reqObj, request.workspace_id, request.oid);
    }

    // grava response no banco
    chatRepository.logResponseMessage(watsonResponse, request.workspace_id, request.oid);

    const resp = await buildInputPrompt(request.input, watsonResponse, googleIntent);

    if ('go_again' in watsonResponse.output && !req.locals.second_time) {
        req.locals.context = watsonResponse.context;
        req.locals.second_time = true;
        google(req, res);
        return;
    }

    if (watsonResponse.output.text.length > 2) {
        console.error('Atenção! Google Assistant não permite respostas com mais de duas ' +
            'mensagens em seguida.');
        console.error('Workspace ID:', req.locals.watsonCredentials.workspace_id);
        console.error(watsonResponse.output.text);
    }

    res.json(resp);
}

/**
 *
 * @param {any} context
 */
async function abrirManifestacao(context) {
    const recordTypeName = 'Relacionamento Cliente';
    const subTipoPesquisaC = 'Indicação de rede';
    const areaNegocio = 'Saúde Individual';
    const carteirinha = context.produto_atual.carteirinha['codigo-beneficiario'];

    const historico = await chatRepository.getAllHistory(context.conversation_id);

    /**
     * @type {any}
     */
    const manifestacaoObj = {
        nomeDaPessoaC: '',
        categoriaC: 'Informação - Saúde Individual',
        type: 'Saúde Individual',
        tipoC: 'Rede Referenciada',
        subject: 'Chat Google Assistant',
        description: historico,
        status: 'concluído',
        origin: 'Chat',
        formaDeContatoC: 'Email',
        IdSessionC: context.conversation_id,
    };


    try {
        manifestacao.gerarManifestacao(recordTypeName, subTipoPesquisaC, areaNegocio,
            carteirinha, manifestacaoObj);
    } catch (err) {
        console.error(err);
    }
}

module.exports = { loadParams, google };